package com.msds.awesometelechat.caches.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;

/**
 * @author Michael Spitsin
 * @since 2015-07-06
 */
public class UriImageWorker extends ImageWorker {

    private static volatile UriImageWorker instance;

    public static void init(Context applicationContext) {
        instance = new UriImageWorker(applicationContext);
    }

    public static UriImageWorker getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Call init(Context) firstly, then use getInstance method");
        }

        return instance;
    }

    protected UriImageWorker(Context context) {
        super(context);
    }

    private Bitmap processBitmap(Uri uri, LoadingImageParams params) {
        Bitmap sourceBitmap = decodeSampledBitmapFromFile(uri.getPath(), params.getReqWidth(), params.getReqHeight(), getImageCache());

        if (params.getActualOrientation() > 0) {
            sourceBitmap = getRotatedBitmap(sourceBitmap, params.getActualOrientation());
        }

        if (params.isCircle()) {
            sourceBitmap = getCroppedBitmap(sourceBitmap);
        }

        return sourceBitmap;
    }

    @Override
    protected Bitmap processBitmap(Object data, Object... params) {
        if (params.length > 0) {
            return processBitmap((Uri) data, (LoadingImageParams) params[0]);
        } else {
            return processBitmap((Uri) data, new LoadingImageParams());
        }
    }

    /**
     * Decode and sample down a bitmap from a file to the requested width and height.
     *
     * @param filename The full path of the file to decode
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @param cache The ImageCache used to find candidate bitmaps for use with inBitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     *         that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight, ImageCache cache) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        // Calculate inSampleSize
        if (reqHeight <= 0 || reqWidth <= 0) {
            options.inSampleSize = 1;
        } else {
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        }

        addInBitmapOptions(options, cache);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    private static Bitmap getRotatedBitmap(Bitmap bitmap, int actualDegrees) {
        return getRotatedBitmap(bitmap, actualDegrees, 0);
    }

    private static Bitmap getRotatedBitmap(Bitmap bitmap, int actualDegrees, int requiredDegrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(actualDegrees - requiredDegrees);

        return Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final float radius = Math.min(bitmap.getHeight(), bitmap.getWidth()) / 2.0f;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, radius, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    private static void addInBitmapOptions(BitmapFactory.Options options, ImageCache cache) {
        // inBitmap only works with mutable bitmaps so force the decoder to
        // return mutable bitmaps.
        options.inMutable = true;

        if (cache != null) {
            // Try and find a bitmap to use for inBitmap
            Bitmap inBitmap = cache.getBitmapFromReusableSet(options);

            if (inBitmap != null) {
                options.inBitmap = inBitmap;
            }
        }
    }

    /**
     * Calculate an inSampleSize for use in a {@link android.graphics.BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link android.graphics.BitmapFactory}. This implementation calculates
     * the closest inSampleSize that is a power of 2 and will result in the final decoded bitmap
     * having a width and height equal to or larger than the requested width and height.
     *
     * @param options An options object with out* params already populated (run through a decode*
     *            method with inJustDecodeBounds==true
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            long totalPixels = width * height / inSampleSize;

            // Anything more than 2x the requested pixels we'll sample down further
            final long totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels > totalReqPixelsCap) {
                inSampleSize *= 2;
                totalPixels /= 2;
            }
        }
        return inSampleSize;
    }

    public static class LoadingImageParams {
        private int mReqWidth;
        private int mReqHeight;
        private boolean isCircle;
        private int mActualOrientation;

        public int getReqWidth() {
            return mReqWidth;
        }

        public LoadingImageParams setReqWidth(int mReqWidth) {
            this.mReqWidth = mReqWidth;
            return this;
        }

        public int getReqHeight() {
            return mReqHeight;
        }

        public LoadingImageParams setReqHeight(int mReqHeight) {
            this.mReqHeight = mReqHeight;
            return this;
        }

        public boolean isCircle() {
            return isCircle;
        }

        public LoadingImageParams setCircle(boolean isCircle) {
            this.isCircle = isCircle;
            return this;
        }

        public int getActualOrientation() {
            return mActualOrientation;
        }

        public LoadingImageParams setActualOrientation(int mActualOrientation) {
            this.mActualOrientation = mActualOrientation;
            return this;
        }
    }
}
