package com.msds.awesometelechat;

import android.support.v7.widget.Toolbar;

import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;

/**
 * @author Michael Spitsin
 * @since 2015-05-11
 */
//TODO descr
public interface DrawerActivityCallBacks {
    void setToolbar(Toolbar toolbar);
    void goToChatFragment(ChatListItem item);
    void clearMessage(long chatId);
    void deleteChat(long chatId);
}
