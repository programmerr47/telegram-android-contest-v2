package com.msds.awesometelechat.chat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.CustomTypefaceSpan;
import com.msds.awesometelechat.util.LocaleUtils;
import com.msds.awesometelechat.util.UserNameFormatter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-23
 */
public final class GroupChatFragment extends ChatFragment {

    private UpdateWatcher<TdApi.UpdateChatTitle> mUpdateChatTitleHandler = new UpdateWatcher<TdApi.UpdateChatTitle>() {
        @Override
        public void onUpdate(TdApi.UpdateChatTitle updateChatTitle) {
            if (getInfo().getChat().id == updateChatTitle.chatId) {
                getToolbar().setTitle(updateChatTitle.title);
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateChatParticipantsCount> mUpdateChatParticipantsCountHandler = new UpdateWatcher<TdApi.UpdateChatParticipantsCount>() {
        @Override
        public void onUpdate(TdApi.UpdateChatParticipantsCount updateChatParticipantsCount) {
            if (getInfo().getChat().id == updateChatParticipantsCount.chatId) {
                TdApi.GroupChatInfo groupChatInfo = (TdApi.GroupChatInfo) getInfo().getChat().type;
                setGroupChatParticipantsInfo(groupChatInfo.groupChat);
            }
        }
    };

    //TODO replace it with default constructor and set arguments method.
    protected GroupChatFragment(TdApi.Chat chat) {
        super(chat);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateChatTitle.class, mUpdateChatTitleHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateChatParticipantsCount.class, mUpdateChatParticipantsCountHandler);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.group_chat_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateChatTitle.class, mUpdateChatTitleHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateChatParticipantsCount.class, mUpdateChatParticipantsCountHandler);
    }

    protected void fillToolbar() {
        TdApi.ChatInfo chatInfo = getInfo().getChat().type;
        Typeface robotoBold = Typeface.createFromAsset(getActivity().getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);
        final TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) chatInfo).groupChat;
        SpannableStringBuilder title = new SpannableStringBuilder(groupChat.title);
        title.setSpan(new CustomTypefaceSpan("", robotoBold), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        getToolbar().setTitle(title);
        setToolbarLogo(groupChat.photoSmall);
        setGroupChatParticipantsInfo(groupChat);
    }

    private void setGroupChatParticipantsInfo(TdApi.GroupChat groupChat) {
        getClient().send(new TdApi.GetGroupChatFull(groupChat.id), new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object instanceof TdApi.GroupChatFull && getActivity() != null) {
                    int onlineCount = 0;
                    int currentTime = (int) (System.currentTimeMillis() / 1000);
                    TdApi.ChatParticipant[] participants = ((TdApi.GroupChatFull) object).participants;
                    for (TdApi.ChatParticipant participant : participants) {
                        TdApi.UserStatus status = participant.user.status;
                        if (status instanceof TdApi.UserStatusOnline && ((TdApi.UserStatusOnline) status).expires > currentTime) {
                            onlineCount++;
                        }
                    }
                    int count = participants.length;
                    if (onlineCount > 1 && count != 0) {
                        getToolbar().setSubtitle(getActivity().getResources().getQuantityString(R.plurals.OFFLINE_ONLINE_MEMBERS_INFO, count, count, onlineCount));
                    } else {
                        getToolbar().setSubtitle(getActivity().getResources().getQuantityString(R.plurals.MEMBERS_INFO, count, count));
                    }
                }
            }
        }));
    }
}
