package com.msds.awesometelechat.chat;

import com.msds.awesometelechat.adapter.item.chat.message.ChatItem;
import com.msds.awesometelechat.collections.MessagesManager;
import com.msds.awesometelechat.collections.ParticipantsManager;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.List;

/**
 * @author Michael Spitsin
 * @since 2015-05-23
 */
public class ChatFragmentInfo {

    private TdApi.Chat mChat;
    private MessagesManager mMessagesManager;
    private ParticipantsManager mParticipantsManager = new ParticipantsManager();

    public ChatFragmentInfo(TdApi.Chat chat, List<ChatItem> messagesContainer) {
        this.mChat = chat;
        this.mMessagesManager = new MessagesManager(messagesContainer);
    }

    public TdApi.Chat getChat() {
        return mChat;
    }

    public MessagesManager getMessagesManager() {
        return mMessagesManager;
    }

    public ParticipantsManager getParticipantsManager() {
        return mParticipantsManager;
    }
}
