package com.msds.awesometelechat.chat;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.msds.awesometelechat.AnimationListener;
import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.DrawerActivityCallBacks;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.ChatAdapter;
import com.msds.awesometelechat.adapter.PhotoThumbAdapter;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;
import com.msds.awesometelechat.adapter.item.chat.message.ChatItem;
import com.msds.awesometelechat.adapter.item.chat.message.ChatItemCreator;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;
import com.msds.awesometelechat.adapter.item.photo.PhotoItem;
import com.msds.awesometelechat.callback.ChatScrollManager;
import com.msds.awesometelechat.callback.RecyclerViewFirstItemFinder;
import com.msds.awesometelechat.callback.impl.MessageFieldTextWatcher;
import com.msds.awesometelechat.callback.impl.ToolbarHideScrollListener;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.concurrent.chattings.AbstractItemsDownloader;
import com.msds.awesometelechat.concurrent.tasks.AsyncTaskWithListener;
import com.msds.awesometelechat.concurrent.tasks.GetGalleryImageUrisTask;
import com.msds.awesometelechat.concurrent.tasks.PhotosSender;
import com.msds.awesometelechat.photos.gallery.GalleryImageProvider;
import com.msds.awesometelechat.photos.gallery.PhotoGalleryActivity;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;
import com.msds.awesometelechat.util.AnimationUtil;
import com.msds.awesometelechat.util.UserInfo;
import com.msds.awesometelechat.util.Util;
import com.msds.awesometelechat.view.EmptyRecyclerView;
import com.msds.awesometelechat.view.YFractionalFrameLayout;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public abstract class ChatFragment extends Fragment implements
        View.OnClickListener,
        PhotoThumbAdapter.OnItemCheckStateListener {

    public static final int REQUEST_TAKE_PHOTO = 1;
    public static final int REQUEST_GET_FROM_GALLERY = 2;

    private DrawerActivityCallBacks mCallBacks;
    private AnimationListener mAnimationListener;

    private EmptyRecyclerView mMessagesRecyclerView;
    private EmptyRecyclerView mRecentPhotosView;
    private Toolbar mToolbar;
    private EditText mMessageField;
    private View mOpenAttachments;
    private View mSend;
    private View mTakePhoto;
    private View mChooseSendFromGallery;
    private TextView mChooseSendFromGalleryLabel;
    private YFractionalFrameLayout mPhotosLayout;
    private View mDarkCoverView;
    private View mPhotosEmptyView;
    private View mPhotosProgressView;
    private View mEmptyMessagesView;
    private View mProgressMessagesView;
    private View mScrollerToBottom;

    private ToolbarHideScrollListener mScrollListener = new ToolbarHideScrollListener(new RecyclerViewFirstItemFinder() {
        @Override
        public int findFirstVisibleItemPosition(RecyclerView recyclerView) {
            return ((LinearLayoutManager) mMessagesRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        }
    });

    private ChatFragmentInfo mInfo;
    private ChatAdapter mChatAdapter;
    private PhotoThumbAdapter mPhotoThumbAdapter;

    private RecyclerItems<PhotoItem> mPhotos;
    private Client mClient;

    //TODO WTF NIGGER!!!!
    private boolean mHasLastReadOutboxMessage;

    private boolean isAttachmentsOpened;
    private boolean isAnimating;
    private boolean isPhotosLoading;

    private Uri mCurrentCapturePhotoUri;

    //Temp value;
    private TdApi.Message mLastMessage;

    private ChatScrollManager mScrollManager = new ChatScrollManager() {
        @Override
        public void scrollToTop() {
            if (mMessagesRecyclerView != null) {
                mMessagesRecyclerView.scrollToPosition(0);
                hideScroller();
            }
        }
    };

    private AbstractItemsDownloader mItemsDownloader = new AbstractItemsDownloader() {
        @Override
        protected void prepareLoading() {
            mChatAdapter.getLazyKeeper().prepare();
        }

        @Override
        protected int doInBackground(TdApi.Messages object, int limit) {
            final TdApi.Message[] messages = object.messages;
            for (final TdApi.Message message : messages) {
                final ChatItem item = ChatItemCreator.INSTANCE.create(message, mChatAdapter);
                if (item != null) {
                    mInfo.getParticipantsManager().setUsernameAndPhotoForItem(item, mChatAdapter, message);
                    mChatAdapter.getLazyKeeper().addNewItem(item);
                }
            }

            return mChatAdapter.getLazyKeeper().getMessagesCount();
        }

        @Override
        protected void onPartIsDownloaded() {
            mChatAdapter.setLoadingState(false);
            mMessagesRecyclerView.setLoadingState(false);
            mChatAdapter.releaseLazyKeeper();
        }
    };

    private AsyncTaskWithListener.OnTaskFinishedListener<List<PhotoItem>> mGetGalleryPhotosTaskListener = new AsyncTaskWithListener.OnTaskFinishedListener<List<PhotoItem>>() {
        @Override
        public void onTaskFinished(Class<? extends AsyncTaskWithListener> completedTask, List<PhotoItem> photoItems) {
            isPhotosLoading = false;
            mRecentPhotosView.setLoadingState(false);
            if (mPhotos != null) {
                mPhotos = new RecyclerItems<>(photoItems);
                mPhotoThumbAdapter.updateItems(mPhotos);
            } else {
                mPhotos = new RecyclerItems<>(photoItems);
                mPhotoThumbAdapter = new PhotoThumbAdapter(mPhotos);
                mPhotoThumbAdapter.setOnItemCheckStateListener(ChatFragment.this);
                mRecentPhotosView.setAdapter(mPhotoThumbAdapter);
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateFile> mPhotoDownloadHandler = new UpdateWatcher<TdApi.UpdateFile>() {
        @Override
        public void onUpdate(TdApi.UpdateFile file) {
            final Set<ChatItem> items = mInfo.getParticipantsManager().getMessagesByAvatar(file.fileId);

            if (items != null) {
                for (final ChatItem item : items) {
                    item.setAvatar(Uri.parse(file.path));

                    if (mChatAdapter != null) {
                        mChatAdapter.notifyItemChanged(item);
                    }
                }
                mInfo.getParticipantsManager().clearAvatarWaitingMessages(file.fileId);
            } else {
                final Set<FileableItem> thumbItems = mInfo.getMessagesManager().getMessagesWithThumbByThumbId().get(file.fileId);
                if (thumbItems != null) {
                    for (FileableItem thumbItem : thumbItems) {
                        thumbItem.setThumbUri(Uri.parse(file.path));

                        if (mChatAdapter != null) {
                            mChatAdapter.notifyItemChanged(thumbItem);
                        }
                    }
                } else {
                    final Set<FileableItem> fileItems = mInfo.getMessagesManager().getFileMessagesByFileId().get(file.fileId);

                    if (fileItems != null) {
                        for (FileableItem fileItem : fileItems) {
                            fileItem.setFileSize(file.size);
                            fileItem.setFilePath(file.path);

                            if (mChatAdapter != null) {
                                mChatAdapter.notifyItemChanged(fileItem);
                            }
                        }
                    }
                }
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateFileProgress> mPhotoDownloadProgressHandler = new UpdateWatcher<TdApi.UpdateFileProgress>() {
        @Override
        public void onUpdate(TdApi.UpdateFileProgress fileProgress) {
            final Set<FileableItem> fileItems = mInfo.getMessagesManager().getFileMessagesByFileId().get(fileProgress.fileId);

            if (fileItems != null) {
                for (FileableItem fileItem : fileItems) {
                    //todo swap param fields when logic or doc will be fixed
                    fileItem.setFileSize(fileProgress.ready);
                    fileItem.setFileDownloadedSize(fileProgress.size);

                    if (mChatAdapter != null) {
                        mChatAdapter.notifyItemChanged(fileItem);
                    }
                }
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateNewMessage> mNewMessageHandler = new UpdateWatcher<TdApi.UpdateNewMessage>() {
        @Override
        public void onUpdate(TdApi.UpdateNewMessage object) {
            TdApi.Message message = object.message;
            if (mInfo.getChat().id == message.chatId) {
                processLastMessage(message, false);
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateChatReadOutbox> mNewChatReadHandler = new UpdateWatcher<TdApi.UpdateChatReadOutbox>() {
        @Override
        public void onUpdate(TdApi.UpdateChatReadOutbox chatReadOutbox) {
            if (chatReadOutbox.chatId == mInfo.getChat().id) {
                for (Iterator<ChatItem> iterator = mInfo.getMessagesManager().getUnreadMessages().iterator(); iterator.hasNext(); ) {
                    ChatItem item = iterator.next();
                    item.setNew(false);
                    notifyElementChanged(item);
                    iterator.remove();
                    if (item.systemInfo.getMessageId() == chatReadOutbox.lastRead) {
                        break;
                    }
                }
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateMessageId> mUpdateMessageIdHandler = new UpdateWatcher<TdApi.UpdateMessageId>() {
        @Override
        public void onUpdate(TdApi.UpdateMessageId messageId) {
            ChatItem item = mInfo.getMessagesManager().getMapMessagesById().get(messageId.oldId);
            if (item != null) {
                item.setSending(false);
                item.setNew(true);
                mInfo.getMessagesManager().getUnreadMessages().add(item);
                mInfo.getMessagesManager().getMapMessagesById().remove(messageId.oldId);
                mInfo.getMessagesManager().getMapMessagesById().put(messageId.newId, item);
                notifyElementChanged(item);
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateMessageDate> mUpdateMessageDateHandler = new UpdateWatcher<TdApi.UpdateMessageDate>() {
        @Override
        public void onUpdate(TdApi.UpdateMessageDate messageDate) {
            ChatItem item = mInfo.getMessagesManager().getMapMessagesById().get(messageDate.messageId);
            if (item != null) {
                item.setDate(new Date(messageDate.newDate * 1000L));
                mChatAdapter.notifyItemChanged(item);
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateMessageContent> mUpdateMessageContentHandler = new UpdateWatcher<TdApi.UpdateMessageContent>() {
        @Override
        public void onUpdate(TdApi.UpdateMessageContent messageContent) {
            ChatItem item = mInfo.getMessagesManager().getMapMessagesById().get(messageContent.messageId);
            if (item != null) {
                //TODO update content
                //notifyElementChanged(item);
            }
        }
    };

    public static ChatFragment newInstance(ChatListItem item) {
        TdApi.ChatInfo chatInfo = item.getChat().type;
        ChatFragment result;

        if (chatInfo.getClass().getName().equals(TdApi.PrivateChatInfo.class.getName()) ||
                chatInfo.getClass().getName().equals(TdApi.UnknownPrivateChatInfo.class.getName())) {
            result = new PrivateChatFragment(item.getChat());
        } else if (chatInfo.getClass().getName().equals(TdApi.GroupChatInfo.class.getName()) ||
                chatInfo.getClass().getName().equals(TdApi.UnknownGroupChatInfo.class.getName())) {
            result = new GroupChatFragment(item.getChat());
        } else {
            throw new IllegalArgumentException("Unhandled type of chat list item. Actual type is: " + item.getChat().type.getClass());
        }

        //TODO remove
        result.setLastMessage(item.getLastMessage() == null ? null : item.getChat().topMessage);

        return result;
    }

    protected ChatFragment(TdApi.Chat chat) {
        this.mInfo = new ChatFragmentInfo(chat, new ArrayList<ChatItem>());
        this.mChatAdapter = new ChatAdapter(mInfo.getMessagesManager(), new ArrayList<AdapterItem>());
        mChatAdapter.setScrollManager(mScrollManager);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        mClient = TG.getClientInstance();

        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateFile.class, mPhotoDownloadHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateFileProgress.class, mPhotoDownloadProgressHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateNewMessage.class, mNewMessageHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateChatReadOutbox.class, mNewChatReadHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateMessageId.class, mUpdateMessageIdHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateMessageDate.class, mUpdateMessageDateHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateMessageContent.class, mUpdateMessageContentHandler);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.single_chat_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mMessagesRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.messages);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mMessageField = (EditText) view.findViewById(R.id.messageField);
        mOpenAttachments = view.findViewById(R.id.openAttachments);
        mSend = view.findViewById(R.id.send);
        mDarkCoverView = view.findViewById(R.id.dark_cover);
        mPhotosLayout = (YFractionalFrameLayout) view.findViewById(R.id.photos_layout);
        mPhotosEmptyView = view.findViewById(R.id.empty_photos_view);
        mPhotosProgressView = view.findViewById(R.id.progress_photos_view);
        mRecentPhotosView = (EmptyRecyclerView) view.findViewById(R.id.recent_gallery_images);
        mTakePhoto = view.findViewById(R.id.action_take_photo);
        mChooseSendFromGallery = view.findViewById(R.id.action_choose_or_send);
        mChooseSendFromGalleryLabel = (TextView) view.findViewById(R.id.choose_or_send_label);
        mEmptyMessagesView = view.findViewById(R.id.empty_messages_view);
        mScrollerToBottom = view.findViewById(R.id.scrollToBottom);
        mProgressMessagesView = view.findViewById(R.id.progress_messages_view);
    }

    //TODO refactor
    protected abstract void fillToolbar();

    //TODO refactor
    protected void setToolbarLogo(TdApi.File file) {
        if (file instanceof TdApi.FileLocal) {
            setToolbarLogo(((TdApi.FileLocal) file).path);
        } else if (file instanceof TdApi.FileEmpty) {
            final int photoId = ((TdApi.FileEmpty) file).id;
            if (photoId != 0) { //todo test
                mClient.send(new TdApi.DownloadFile(photoId), new UIResultHandler(new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object instanceof TdApi.Ok) {
                            AwesomeApplication.getUpdateManager().addWatcher(
                                    TdApi.UpdateFile.class,
                                    new UpdateWatcher<TdApi.UpdateFile>() {
                                        @Override
                                        public void onUpdate(TdApi.UpdateFile file) {
                                            if (file.fileId == photoId) {
                                                AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateFile.class, this);
                                                setToolbarLogo(file.path);
                                            }
                                        }
                                    });
                        }
                    }
                }));
            }
        }
    }

    //TODO replace to toolbar util or something like that
    protected void setToolbarLogo(String path) {
        if (getActivity() != null) {
            RoundedBitmapDrawable photo = RoundedBitmapDrawableFactory.create(getResources(), path);
            photo.setCornerRadius(Math.min(photo.getBitmap().getWidth(), photo.getBitmap().getHeight()) / 2.0f);
            mToolbar.setLogo(photo);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //mMessagesRecyclerView.setItemAnimator(new NoAnimationOnChangeItemAnimatorWrapper(mMessagesRecyclerView.getItemAnimator()));
        mMessagesRecyclerView.getItemAnimator().setSupportsChangeAnimations(false);
        mMessagesRecyclerView.setEmptyView(mEmptyMessagesView);
        mMessagesRecyclerView.setLoadingView(mProgressMessagesView);
        mMessagesRecyclerView.setLoadingState(false);

        mToolbar.setTitleTextAppearance(getActivity(), R.style.ChatTitle);
        mToolbar.setSubtitleTextAppearance(getActivity(), R.style.ChatSubTitle);
        fillToolbar();
        mCallBacks.setToolbar(mToolbar);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mMessagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true));
        mMessagesRecyclerView.setAdapter(mChatAdapter);

        mRecentPhotosView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecentPhotosView.setEmptyView(mPhotosEmptyView);
        mRecentPhotosView.setLoadingView(mPhotosProgressView);

        if (mInfo.getMessagesManager().getMessages().isEmpty()) {
            if (mLastMessage != null && !mItemsDownloader.isItemsDownloadedCompletely()) {
                int visibleItemCount = Util.getMaxVisibleRowsInScreenRes(getActivity(), R.dimen.item_height_small);
                mMessagesRecyclerView.setLoadingState(true);
                mItemsDownloader.download(mInfo.getChat(), 0, (int)(visibleItemCount * 1.5));
            } else if (mLastMessage == null) {
                mItemsDownloader.setItemsDownloadedCompletely();
            }

            if (mLastMessage != null) {
                mHasLastReadOutboxMessage =
                        mLastMessage.fromId == UserInfo.getMe().id
                                && mInfo.getChat().lastReadOutboxMessageId == mInfo.getChat().topMessage.id;
                processLastMessage(mLastMessage, false);
                mLastMessage = null;
            }
        }

        mMessagesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_DRAGGING ||
                        newState == RecyclerView.SCROLL_STATE_IDLE) {
                    LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int firstVisibleItemPos = manager.findFirstCompletelyVisibleItemPosition();

                    if (firstVisibleItemPos == 0) {
                        hideScroller();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollListener.onScrolled(recyclerView, dx, dy);

                if (mItemsDownloader.canDownload()) {
                    //todo will we always be sure that have instance of LinearLayoutManager
                    LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int count = manager.findLastVisibleItemPosition() - manager.findFirstVisibleItemPosition() + 1;
                    if (mChatAdapter.getItemCount() < manager.findLastVisibleItemPosition() + count) {
                        if (!mMessagesRecyclerView.getLoadingState()) {
                            mChatAdapter.setLoadingState(true);
                        }

                        mItemsDownloader.download(mInfo.getChat(), mInfo.getMessagesManager().getMessages().size(), count);
                    }
                }
            }
        });

        mMessageField.addTextChangedListener(new MessageFieldTextWatcher(getActivity(), mSend, mOpenAttachments));
        mSend.setOnClickListener(this);
        mOpenAttachments.setOnClickListener(this);
        mDarkCoverView.setOnClickListener(this);

        if (isAttachmentsOpened) {
            immediatelyOpenAttachments();
        } else {
            immediatelyCloseAttachments();
        }

        if (mPhotos != null) {
            mRecentPhotosView.setAdapter(mPhotoThumbAdapter);
            changeTextOfChooseSendFromGalleryLabel(mPhotoThumbAdapter.getCheckedItemCount());
            mRecentPhotosView.setLoadingState(false);
        } else if (!isPhotosLoading) {
            isPhotosLoading = true;
            mRecentPhotosView.setLoadingState(true);
            mChooseSendFromGalleryLabel.setText(getString(R.string.CHOOSE_FROM_GALLERY));
            GetGalleryImageUrisTask fetchGalleryPhotosTask = new GetGalleryImageUrisTask(getActivity());
            fetchGalleryPhotosTask.setOnTaskFinishedListener(mGetGalleryPhotosTaskListener);
            fetchGalleryPhotosTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            mTakePhoto.setVisibility(View.VISIBLE);
        } else {
            mTakePhoto.setVisibility(View.GONE);
        }

        mTakePhoto.setOnClickListener(this);
        mChooseSendFromGallery.setOnClickListener(this);
        mScrollerToBottom.setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallBacks = (DrawerActivityCallBacks) activity;
            mAnimationListener = (AnimationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AnimationListener");
        }
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        int id = enter ? R.animator.fragment_slide_in : R.animator.fragment_slide_out;
        final Animator anim = AnimatorInflater.loadAnimator(getActivity(), id);
        if (enter) {
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mAnimationListener.onAnimationEnd();
                    mItemsDownloader.setReadyState(true);
                }
            });
        }
        return anim;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMessagesRecyclerView.removeOnScrollListener(mScrollListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateFile.class, mPhotoDownloadHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateFileProgress.class, mPhotoDownloadProgressHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateNewMessage.class, mNewMessageHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateChatReadOutbox.class, mNewChatReadHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateMessageId.class, mUpdateMessageIdHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateMessageDate.class, mUpdateMessageDateHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateMessageContent.class, mUpdateMessageContentHandler);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //TODO think about case when app closes or restarts activity and we need to restore previous chat
        if (requestCode == REQUEST_TAKE_PHOTO &&
                resultCode == Activity.RESULT_OK) {
            onCaptureResult(data);
        } else if (requestCode == REQUEST_GET_FROM_GALLERY &&
                resultCode == Activity.RESULT_OK) {
            Collection<PhotoItem> items = AwesomeApplication.getPhotoThumbsHolder().release();
            sendPhotos(items);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear_history:
                clearHistory();
                return true;
            case R.id.action_leave_group:
                leaveGroup();
                return true;
            case R.id.action_mute:
                mute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.send) {
            TdApi.InputMessageContent content = new TdApi.InputMessageText(mMessageField.getText().toString());
            TdApi.TLFunction sendFunc = new TdApi.SendMessage(mInfo.getChat().id, content);
            mMessageField.setText("");
            mClient.send(sendFunc, new UIResultHandler(new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    if (object instanceof TdApi.Message) {
                        final TdApi.Message message = (TdApi.Message) object;
                        processLastMessage(message, true);
                    }
                }
            }));
        } else if (v.getId() == R.id.openAttachments) {
            switchAttachments();
        } else if (v.getId() == R.id.dark_cover) {
            switchAttachments();
        } else if (v.getId() == R.id.action_choose_or_send) {
            if (mPhotoThumbAdapter != null) {
                if (mPhotoThumbAdapter.getCheckedItemCount() == 0) {
                    AwesomeApplication.getPhotoThumbsHolder().placeNewItems(mPhotos.getOriginalList());
                    Intent intent = new Intent(getActivity(), PhotoGalleryActivity.class);
                    startActivityForResult(intent, REQUEST_GET_FROM_GALLERY);
                } else {
                    sendPhotos(mPhotoThumbAdapter.getCkeckedItems());
                    mPhotoThumbAdapter.uncheckAll();
                    changeTextOfChooseSendFromGalleryLabel(0);
                }
            }
        } else if (v.getId() == R.id.action_take_photo) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File photoFile = getPhotoFile();

                if (photoFile != null) {
                    mCurrentCapturePhotoUri = Uri.fromFile(photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCurrentCapturePhotoUri);
                    Log.v("PHOTO_CAPTURE", "---- " + photoFile);
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }
            }
        } else if (v.getId() == R.id.scrollToBottom) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) mMessagesRecyclerView.getLayoutManager();
            int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

            if (firstVisibleItem > 10) {
                mMessagesRecyclerView.scrollToPosition(0);
            } else {
                mMessagesRecyclerView.smoothScrollToPosition(0);
            }

            hideScroller();
        }
    }

    //TODO refactor
    public void setLastMessage(TdApi.Message lastMessage) {
        this.mLastMessage = lastMessage;
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    //TODO create atachments manager/view and move to there
    public boolean isAttachmentsOpened() {
        return isAttachmentsOpened;
    }

    //TODO create atachments manager/view and move to there
    private void switchAttachments() {
        if (!isAnimating) {
            if (isAttachmentsOpened) {
                closeAttachments();
            } else {
                openAttachments();
            }
        }
    }

    //TODO create atachments manager/view and move to there
    public void closeAttachments() {
        closeAttachments(null);
    }

    public void closeAttachments(Animator.AnimatorListener listener) {
        if (getActivity() != null) {
            Animator closingAnimation = AnimationUtil.getCloseAttachmentsAnimator(getActivity(), mPhotosLayout, mDarkCoverView);
            closingAnimation.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mAnimationListener.onAnimationEnd();
                    mChatAdapter.onAnimationEnd();
                    isAttachmentsOpened = false;
                    isAnimating = false;
                }

                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    mAnimationListener.onAnimationStart();
                    mChatAdapter.onAnimationStart();
                    isAttachmentsOpened = true;
                    isAnimating = true;
                }
            });

            if (listener != null) {
                closingAnimation.addListener(listener);
            }

            closingAnimation.start();
        }
    }

    //TODO create atachments manager/view and move to there
    public void openAttachments() {
        if (getActivity() != null) {
            Animator openingAnimation = AnimationUtil.getOpenAttachmentsAnimator(getActivity(), mPhotosLayout, mDarkCoverView);
            openingAnimation.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    mAnimationListener.onAnimationStart();
                    mChatAdapter.onAnimationStart();
                    isAttachmentsOpened = false;
                    isAnimating = true;
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mAnimationListener.onAnimationEnd();
                    mChatAdapter.onAnimationEnd();
                    isAttachmentsOpened = true;
                    isAnimating = false;
                    forceCloseKeyboardIfNecessary();
                }
            });
        }
    }

    //TODO create atachments manager/view and move to there
    public void immediatelyOpenAttachments() {
        mPhotosLayout.setYFraction(1.0f);
        mDarkCoverView.setVisibility(View.VISIBLE);
        mDarkCoverView.setAlpha(0.5f);
    }

    //TODO create atachments manager/view and move to there
    public void immediatelyCloseAttachments() {
        mPhotosLayout.setYFraction(0.0f);
        mDarkCoverView.setVisibility(View.GONE);
        mDarkCoverView.setAlpha(0.0f);
    }

    public void onCaptureResult(Intent data) {
        if (getActivity() != null) {
            Uri photoUri = mCurrentCapturePhotoUri;
            if (data != null && data.getData() != null) {
                photoUri = data.getData();
            }
            mCurrentCapturePhotoUri = null;

            if (photoUri != null) {
                if (isAttachmentsOpened) {
                    closeAttachments();
                }
                GalleryImageProvider.galleryAddPic(getActivity(), photoUri);
                sendOnePhoto(photoUri);
            }
        }
    }

    @Override
    public void onCheckStateChange(int oldCheckedItemCount, int newCheckedItemCount) {
        changeTextOfChooseSendFromGalleryLabel(newCheckedItemCount);
    }

    protected Client getClient() {
        return mClient;
    }

    protected ChatFragmentInfo getInfo() {
        return mInfo;
    }

    private ActionBar getActionBar() {
        AppCompatActivity parentActivity = (AppCompatActivity) getActivity();
        return parentActivity.getSupportActionBar();
    }

    private void processLastMessage(final TdApi.Message lastMessage, boolean sending) {
        mChatAdapter.processLastMessage(mInfo.getParticipantsManager(), lastMessage, sending);

        if (sending) {
            mMessagesRecyclerView.scrollToPosition(0);
            hideScroller();
        } else {
            LinearLayoutManager recyclerManager = (LinearLayoutManager) mMessagesRecyclerView.getLayoutManager();
            int firstVisiblePosition = recyclerManager.findFirstVisibleItemPosition();
            if (firstVisiblePosition > 1) {
                showScroller();
            } else {
                mMessagesRecyclerView.scrollToPosition(0);
                hideScroller();
            }
        }
    }

    public void notifyElementChanged(ChatItem item) {
        mChatAdapter.notifyItemChanged(item);
    }

    public void forceCloseKeyboardIfNecessary() {
        if (mMessageField.hasFocus() && getActivity() != null) {
            Util.forceClearFocusOnView(getActivity(), mMessageField);
        }
    }

    private void changeTextOfChooseSendFromGalleryLabel(int checkedItemCount) {
        if (getActivity() != null) {
            if (checkedItemCount == 0) {
                mChooseSendFromGalleryLabel.setText(getString(R.string.CHOOSE_FROM_GALLERY));
            } else {
                mChooseSendFromGalleryLabel.setText(String.format(getActivity().getResources().getQuantityString(R.plurals.SEND_AMOUNT_OF_PHOTOS, checkedItemCount), checkedItemCount));
            }
        }
    }

    private File getPhotoFile() {
        File photoFile;
        try {
            photoFile = GalleryImageProvider.createImageFile(getActivity());
        } catch (IOException e) {
            e.printStackTrace();
            photoFile = null;
        }

        return photoFile;
    }

    //TODO
    private void sendPhotos(final Collection<PhotoItem> items) {
        if (items != null) {
            final PhotoItem[] sendingPhotos = items.toArray(new PhotoItem[items.size()]);
            if (isAttachmentsOpened) {
                closeAttachments(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        new PhotosSender(
                                mChatAdapter,
                                mInfo.getChat(),
                                mInfo.getParticipantsManager())
                                .execute(sendingPhotos);
                    }
                });
            }
        } else {
            if (isAttachmentsOpened) {
                closeAttachments();
            }
        }
    }

    //TODO
    private void sendOnePhoto(Uri photoUri) {
        TdApi.InputMessageContent content = new TdApi.InputMessagePhoto(photoUri.getPath());
        TdApi.TLFunction sendFunc = new TdApi.SendMessage(mInfo.getChat().id, content);
        mClient.send(sendFunc, new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object instanceof TdApi.Message) {
                    final TdApi.Message message = (TdApi.Message) object;
                    processLastMessage(message, true);
                }
            }
        }));
    }

    //TODO
    private void clearHistory() {
        new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setTitle(R.string.app_name)
                .setMessage(R.string.CLEAR_HISTORY_CONFIRMATION)
                .setNegativeButton(getString(R.string.CANCEL).toUpperCase(), null)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TdApi.TLFunction clearHistoryFunc = new TdApi.DeleteChatHistory(mInfo.getChat().id);
                        mClient.send(clearHistoryFunc, new UIResultHandler(new Client.ResultHandler() {
                            @Override
                            public void onResult(final TdApi.TLObject object) {
                                if (object instanceof TdApi.Ok && getActivity() != null) {
                                    mInfo.getMessagesManager().clear();
                                    mChatAdapter.notifyDataSetChanged();
                                    mCallBacks.clearMessage(mInfo.getChat().id);
                                }
                            }
                        }));
                    }
                })
                .show();
    }

    //TODO
    private void leaveGroup() {
        new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setTitle(R.string.app_name)
                .setMessage(R.string.LEAVE_CHAT_CONFIRMATION)
                .setNegativeButton(getString(R.string.CANCEL).toUpperCase(), null)
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TdApi.TLFunction deleteSelf = new TdApi.DeleteChatParticipant(mInfo.getChat().id, UserInfo.getMe().id);
                        mClient.send(deleteSelf, new UIResultHandler(new Client.ResultHandler() {
                            @Override
                            public void onResult(final TdApi.TLObject object) {
                                if (object instanceof TdApi.Ok && getActivity() != null) {
                                    mCallBacks.deleteChat(mInfo.getChat().id);
                                    mCallBacks.goToChatFragment(null);
                                }
                            }
                        }));
                    }
                }).show();
    }

    //TODO
    private void mute() {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), "Not supported because of not supporting notifications", Toast.LENGTH_SHORT).show();
        }
    }

    private void showScroller() {
        if (getActivity() != null && mScrollerToBottom.getVisibility() != View.VISIBLE) {
            AnimationUtil.showChatScroller(getActivity(), mScrollerToBottom);
        }
    }

    public void hideScroller() {
        if (getActivity() != null && mScrollerToBottom.getVisibility() == View.VISIBLE) {
            AnimationUtil.hideChatScroller(getActivity(), mScrollerToBottom);
        }
    }
}
