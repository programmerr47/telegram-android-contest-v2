package com.msds.awesometelechat.chat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.CustomTypefaceSpan;
import com.msds.awesometelechat.util.LocaleUtils;
import com.msds.awesometelechat.util.UserNameFormatter;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-25
 */
public class PrivateChatFragment extends ChatFragment {

    private UpdateWatcher<TdApi.UpdateUserStatus> mUpdateUserStatusHandler = new UpdateWatcher<TdApi.UpdateUserStatus>() {
        @Override
        public void onUpdate(TdApi.UpdateUserStatus updateUserStatus) {
            TdApi.User user = ((TdApi.PrivateChatInfo) getInfo().getChat().type).user;
            if (user.id == updateUserStatus.userId) {
                getToolbar().setSubtitle(LocaleUtils.formatUserStatus(updateUserStatus.status));
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateUserPhoto> mUpdateUserPhotoHandler = new UpdateWatcher<TdApi.UpdateUserPhoto>() {
        @Override
        public void onUpdate(TdApi.UpdateUserPhoto updateUserPhoto) {
            TdApi.User user = ((TdApi.PrivateChatInfo) getInfo().getChat().type).user;
            if (user.id == updateUserPhoto.userId) {
                setToolbarLogo(updateUserPhoto.photoSmall);
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateUserName> mUpdateUserNameHandler = new UpdateWatcher<TdApi.UpdateUserName>() {
        @Override
        public void onUpdate(TdApi.UpdateUserName updateUserName) {
            TdApi.User user = ((TdApi.PrivateChatInfo) getInfo().getChat().type).user;
            if (user.id == updateUserName.userId) {
                getToolbar().setTitle((updateUserName.firstName + " " + updateUserName.lastName).trim());
            }
        }
    };

    //TODO replace it with default constructor and set arguments method.
    protected PrivateChatFragment(TdApi.Chat chat) {
        super(chat);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateUserName.class, mUpdateUserNameHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateUserPhoto.class, mUpdateUserPhotoHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateUserStatus.class, mUpdateUserStatusHandler);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.single_chat_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateUserName.class, mUpdateUserNameHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateUserPhoto.class, mUpdateUserPhotoHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateUserStatus.class, mUpdateUserStatusHandler);
    }

    protected void fillToolbar() {
        TdApi.ChatInfo chatInfo = getInfo().getChat().type;
        Typeface robotoRegular = Typeface.createFromAsset(getActivity().getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(getActivity().getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);
        TdApi.User user = ((TdApi.PrivateChatInfo) chatInfo).user;
        String name = UserNameFormatter.fullName(user);
        SpannableStringBuilder title = new SpannableStringBuilder(name);
        title.setSpan(new CustomTypefaceSpan("", robotoBold), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        getToolbar().setTitle(title);
        String status = LocaleUtils.formatUserStatus(user.status);
        SpannableStringBuilder subTitle = new SpannableStringBuilder(status);
        subTitle.setSpan(new CustomTypefaceSpan("", robotoRegular), 0, subTitle.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        getToolbar().setSubtitle(subTitle);
        setToolbarLogo(user.photoSmall);
    }
}
