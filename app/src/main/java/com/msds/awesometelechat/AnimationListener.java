package com.msds.awesometelechat;

/**
 * @author Michael Spitsin
 * @since 2015-05-03
 */
//TODO descr
public interface AnimationListener {
    void onAnimationEnd();
    void onAnimationStart();
}
