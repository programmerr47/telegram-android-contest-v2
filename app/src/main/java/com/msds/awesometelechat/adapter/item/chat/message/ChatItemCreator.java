package com.msds.awesometelechat.adapter.item.chat.message;

import android.support.annotation.NonNull;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.callback.ChatItemNotifier;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-06-27
 */
public enum ChatItemCreator {
    INSTANCE;

    private enum ItemType {
        AUDIO_MESSAGE(TdApi.MessageAudio.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatAudioItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        FILE_MESSAGE(TdApi.MessageDocument.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatFileItem.createFromMessage(AwesomeApplication.getAppContext(), message, notifier);
            }
        },
        PHOTO_MESSAGE(TdApi.MessagePhoto.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatPhotoItem.createFromMessage(AwesomeApplication.getAppContext(), message, notifier);
            }
        },
        SYS_MESSAGE_ADD_PARTICIPANT(TdApi.MessageChatAddParticipant.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatAddParticipantItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        SYS_MESSAGE_CHANGE_PHOTO(TdApi.MessageChatChangePhoto.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                ChatChangePhotoItem resultItem = ChatChangePhotoItem.createFromMessage(AwesomeApplication.getAppContext(), message);

                if (notifier != null) {
                    TdApi.MessageChatChangePhoto content = (TdApi.MessageChatChangePhoto) message.message;
                    ChatCreator.downloadThumbnail(resultItem, content.photo.photos[0].photo, notifier);
                }

                return resultItem;
            }
        },
        SYS_MESSAGE_CHANGE_TITLE(TdApi.MessageChatChangeTitle.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatChangeTitleItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        SYS_MESSAGE_CREATED_GROUP(TdApi.MessageGroupChatCreate.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatCreatedGroupItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        SYS_MESSAGE_REMOVE_PARTICIPANT(TdApi.MessageChatDeleteParticipant.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatRemoveParticipantItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        SYS_MESSAGE_REMOVE_PHOTO(TdApi.MessageChatDeletePhoto.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatRemovePhotoItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        TEXT_MESSAGE(TdApi.MessageText.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatDefaultItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        UNSUPPORTED_MESSAGE(null) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatUnsupportedItem.createFromMessage(AwesomeApplication.getAppContext(), message);
            }
        },
        VIDEO_MESSAGE(TdApi.MessageVideo.class) {
            @Override
            protected ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
                return ChatVideoItem.createFromMessage(AwesomeApplication.getAppContext(), message, notifier);
            }
        };

        private Class targetMessageClass;

        private ItemType(Class targetMessageClass) {
            this.targetMessageClass = targetMessageClass;
        }

        protected abstract ChatItem createItem(@NonNull TdApi.Message message, ChatItemNotifier notifier);

        private static ItemType fromMessageType(TdApi.Message message) {
            Class messageType = message.message.getClass();

            for (ItemType itemType : ItemType.values()) {
                if (itemType.targetMessageClass == messageType) {
                    return itemType;
                }
            }

            return UNSUPPORTED_MESSAGE;
        }
    }

    public ChatItem create(@NonNull TdApi.Message message, ChatItemNotifier notifier) {
        ItemType itemType = ItemType.fromMessageType(message);
        return itemType.createItem(message, notifier);
    }

    public ChatItem create(@NonNull TdApi.Message message) {
        return create(message, null);
    }
}
