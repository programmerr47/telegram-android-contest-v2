package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.ImageableItem;
import com.msds.awesometelechat.callback.ChatItemNotifier;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Random;

/**
 * @author Michael Spitsin
 * @since 2015-02-12
 */
@Deprecated
public final class ChatCreator {

    //TODO replace to FileUtil
    public static int getSizeOfFile(TdApi.File file) {
        if (file instanceof TdApi.FileEmpty) {
            return ((TdApi.FileEmpty) file).size;
        } else if (file instanceof TdApi.FileLocal) {
            return ((TdApi.FileLocal) file).size;
        } else {
            throw new IllegalStateException("Unknown file type: " + file.getClass().getName());
        }
    }

    public static void downloadPhoto(final FileableItem item, final ChatItemNotifier itemNotifier, final TdApi.File photoFile, TdApi.File thumbFile) {
        if (thumbFile instanceof TdApi.FileLocal) {
            item.setThumbUri(Uri.parse(((TdApi.FileLocal) thumbFile).path));
            downloadFile(item, photoFile);
        } else {
            TdApi.FileEmpty fileEmpty = (TdApi.FileEmpty) thumbFile;
            if (fileEmpty.id != 0) {
                Client client = TG.getClientInstance();
                if (client != null) {
                    client.send(new TdApi.DownloadFile(fileEmpty.id), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                            if (object instanceof TdApi.Ok) {
                                downloadFile(item, photoFile);
                            }
                        }
                    });
                }
            } else {
                Log.e("[TD.API]", "Photo id is 0; " + item);
            }
        }
    }

    public static void downloadThumb(final FileableItem item, TdApi.File thumbFile) {
        if (thumbFile instanceof TdApi.FileLocal) {
            TdApi.FileLocal thumbLocal = (TdApi.FileLocal) thumbFile;
            item.setThumbUri(Uri.parse(thumbLocal.path));
        } else {
            TdApi.FileEmpty fileEmpty = (TdApi.FileEmpty) thumbFile;
            if (fileEmpty.id != 0) {
                Client client = TG.getClientInstance();
                if (client != null) {
                    client.send(new TdApi.DownloadFile(fileEmpty.id), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                        }
                    });
                }
            } else {
                Log.e("[TD.API]", "Thumb id is 0; " + item);
            }
        }
    }

    public static void downloadFile(final FileableItem item, TdApi.File file) {
        if (file instanceof TdApi.FileLocal) {
            TdApi.FileLocal fileLocal = (TdApi.FileLocal) file;
            item.setFilePath(fileLocal.path);
            item.setFileSize(fileLocal.size);
        } else {
            TdApi.FileEmpty fileEmpty = (TdApi.FileEmpty) file;
            item.setFileSize(fileEmpty.size);
            if (fileEmpty.id != 0) {
                Client client = TG.getClientInstance();
                if (client != null) {
                    client.send(new TdApi.DownloadFile(fileEmpty.id), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                        }
                    });
                }
            } else {
                Log.e("[TD.API]", "File id is 0; " + item);
            }
        }
    }

    public static void downloadFile(int fileId) {
        Client client = TG.getClientInstance();
        if (client != null) {
            client.send(new TdApi.DownloadFile(fileId), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                }
            });
        }
    }

    public static void cancelDownloadFile(int fileId) {
        Client client = TG.getClientInstance();
        if (client != null) {
            client.send(new TdApi.CancelDownloadFile(fileId), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                }
            });
        }
    }

    public static void downloadThumbnail(final ImageableItem item, TdApi.File photo, final ChatItemNotifier itemNotifier) {
        if (photo instanceof TdApi.FileLocal) {
            item.setUri(Uri.parse(((TdApi.FileLocal) photo).path));
        } else if (photo instanceof TdApi.FileEmpty) {
            final int photoId = ((TdApi.FileEmpty) photo).id;
            if (photoId != 0) {
                Client client = TG.getClientInstance();
                client.send(new TdApi.DownloadFile(photoId), new UIResultHandler(new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object instanceof TdApi.Ok) {
                            AwesomeApplication.getUpdateManager().addWatcher(
                                TdApi.UpdateFile.class,
                                new UpdateWatcher<TdApi.UpdateFile>() {
                                    @Override
                                    public void onUpdate(TdApi.UpdateFile file) {
                                        if (file.fileId == photoId) {
                                            AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateFile.class, this);
                                            item.setUri(Uri.parse(file.path));
                                            itemNotifier.notifyItemChanged(item);
                                        }
                                    }
                                });
                        }
                    }
                }));
            }
        }
    }
}
