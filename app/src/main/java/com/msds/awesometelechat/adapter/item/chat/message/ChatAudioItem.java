package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatAudioItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.TdApi;
import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatAudioItem extends ChatItem {

    public enum AudioStatus {
        NOT_DOWNLOADED(R.drawable.ic_download_blue, 0xfff0f6fa),
        DOWNLOADING(R.drawable.ic_pause_blue, 0xfff0f6fa),
        DOWNLOADED_NOT_PLAYING(R.drawable.ic_play, 0xff68ade1),
        DOWNLOADED_PLAYING(R.drawable.ic_pause, 0xff68ade1);

        private int iconId;

        private Drawable audioBackgroundColorDrawable;

        private AudioStatus(int statusIconResId, int backgroundColor) {
            this.iconId = statusIconResId;
            this.audioBackgroundColorDrawable = new ColorDrawable(backgroundColor);
        }

        public int getStatusIconResId() {
            return iconId;
        }

        public Drawable getAudioBackgroundColorDrawable() {
            return audioBackgroundColorDrawable;
        }
    }

    private float mDownloadingProgress;
    private AudioStatus mAudioStatus;
    private int mPlaybackProgress;
    private int mAudioDuration;

    private String mFormattedAudioDuration;

    public static ChatAudioItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message) {
        TdApi.MessageAudio content = (TdApi.MessageAudio) message.message;
        ChatAudioItem result = new ChatAudioItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.mAudioDuration = content.audio.duration;
        result.mAudioStatus = AudioStatus.NOT_DOWNLOADED;
        result.mFormattedAudioDuration = Util.getAppropriatePlaybackDuration(result.mAudioDuration);
        return result;
    }

    protected ChatAudioItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    public float getDownloadingProgress() {
        return mDownloadingProgress;
    }

    public void setDownloadingProgress(float downloadingProgress) {
        mDownloadingProgress = downloadingProgress;
    }

    public AudioStatus getAudioStatus() {
        return mAudioStatus;
    }

    public void setAudioStatus(AudioStatus audioStatus) {
        mAudioStatus = audioStatus;
    }

    public int getPlaybackProgress() {
        return mPlaybackProgress;
    }

    public void setPlaybackProgress(int playbackProgress) {
        mPlaybackProgress = playbackProgress;
    }

    public int getAudioDuration() {
        return mAudioDuration;
    }

    public void setAudioDuration(int audioDuration) {
        mAudioDuration = audioDuration;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        ChatAudioItemHolder holder = (ChatAudioItemHolder) viewHolder;
        bindBaseView(holder, position);
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_message_audio, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                ChatAudioItemHolder.ResourceParams params = new ChatAudioItemHolder.ResourceParams();
                params.avatarLayoutId = R.id.avatar;
                params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
                params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
                params.newMarkId = R.id.new_mark;
                params.sendingMarkId = R.id.sendingMark;
                params.timestampId = R.id.timestamp;
                params.usernameId = R.id.username;
                params.downloadCancelOpenId = R.id.downloadCancelOpen;
                params.audioDurationId = R.id.audioDuration;
                params.audioPlaybackProgressId = R.id.audioPlaybackProgress;
                params.statusBackgroundId = R.id.statusBackground;
                params.fileStatusImageId = R.id.statusImage;
                params.progressId = R.id.downloadingProgress;

                return new ChatAudioItemHolder(view, params);
            }
        };
    }

    private void bindView(ChatAudioItemHolder holder, int position) {
        //setting audio duration
        holder.getAudioDuration().setText(mFormattedAudioDuration);

        //setting file status
        holder.getFileStatusImage().setImageResource(mAudioStatus.getStatusIconResId());
        holder.getStatusBackground().setImageDrawable(mAudioStatus.getAudioBackgroundColorDrawable());
        switch (mAudioStatus) {
            case NOT_DOWNLOADED:
            case DOWNLOADING:
                holder.getAudioPlaybackProgress().setEnabled(false);
                holder.getAudioPlaybackProgress().setProgress(0);
                break;
            case DOWNLOADED_NOT_PLAYING:
            case DOWNLOADED_PLAYING:
                holder.getAudioPlaybackProgress().setEnabled(true);
                holder.getAudioPlaybackProgress().setProgress(mPlaybackProgress);
                break;
            default:
                break;
        }

        //setting progress
        if (mDownloadingProgress == -1) {
            holder.getDownloadingProgress().setVisibility(View.INVISIBLE);
        } else {
            holder.getDownloadingProgress().setVisibility(View.VISIBLE);
            holder.getDownloadingProgress().setProgressWithAnimation(mDownloadingProgress);
        }
    }
}
