package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatPhotoItemHolder extends ChatItemHolder {

    private ImageView mPhoto;
    private FrameLayout mPhotoContainer;
    private ProgressBar mProgressBar;
    private TextView mProgressLabel;

    public ChatPhotoItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mPhoto = (ImageView) itemView.findViewById(params.photoLayoutId);
        mPhotoContainer = (FrameLayout) itemView.findViewById(params.photoContainerId);
        mProgressBar = (ProgressBar) itemView.findViewById(params.downloadingProgress);
        mProgressLabel = (TextView) itemView.findViewById(params.progressLabel);
    }

    public ImageView getPhoto() {
        return mPhoto;
    }

    public FrameLayout getPhotoContainer() {
        return mPhotoContainer;
    }

    public ProgressBar getDownloadingProgress() {
        return mProgressBar;
    }

    public TextView getProgressLabel() {
        return mProgressLabel;
    }

    public static final class ResourceParams extends ChatItemHolder.ResourceParams {
        public int photoLayoutId;
        public int photoContainerId;
        public int downloadingProgress;
        public int progressLabel;
    }
}
