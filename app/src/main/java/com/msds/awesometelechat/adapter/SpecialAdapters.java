package com.msds.awesometelechat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.drawer.DrawerItem;
import com.msds.awesometelechat.callback.NavigationDrawerListener;

/**
 * Created by Spoke on 11.04.2015.
 */
//todo describe
public class SpecialAdapters {

    public static <Item extends DrawerItem> AbstractMultiTypeRecyclerAdapter<Item> createDrawerAdapter(@NonNull final RecyclerItems<Item> items, final NavigationDrawerListener callbacks) {
        return new AbstractMultiTypeRecyclerAdapter<Item>(items) {
            @Override
            protected void onPostBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
                if (callbacks != null) {
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callbacks.onNavigationDrawerItemSelected(items.get(position), position);
                        }
                    });
                }
            }
        };
    }
}
