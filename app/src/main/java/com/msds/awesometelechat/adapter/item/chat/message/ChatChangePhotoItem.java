package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.IconTitleItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.ImageableItem;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.CustomTypefaceSpan;
import com.msds.awesometelechat.util.UserInfo;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-19
 */
public class ChatChangePhotoItem extends ChatItem implements ImageableItem {

    private int mWidth;
    private int mHeight;
    private Uri mUri;

    private SpannableStringBuilder mStatus;
    private Drawable mPhotoBackgroundDrawable = new ColorDrawable(0xff888888);

    public static ChatChangePhotoItem createFromMessage(@NonNull Context context, TdApi.Message message) {
        TdApi.MessageChatChangePhoto content = (TdApi.MessageChatChangePhoto) message.message;
        ChatChangePhotoItem result = new ChatChangePhotoItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        if (content.photo.photos.length > 0){
            result.mWidth = content.photo.photos[0].width;
            result.mHeight = content.photo.photos[0].height;
        }
        result.generateStatus();
        return result;
    }

    protected ChatChangePhotoItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        IconTitleItemHolder holder = (IconTitleItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat__chaged_photo_status, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                IconTitleItemHolder.ResourceParams params = new IconTitleItemHolder.ResourceParams();
                params.iconLayoutId = R.id.new_group_avatar;
                params.titleLayoutId = R.id.fileInfo;
                return new IconTitleItemHolder(view, params);
            }
        };
    }

    @Override
    public Uri getUri() {
        return mUri;
    }

    @Override
    public void setUri(Uri uri) {
        this.mUri = uri;
    }

    @Override
    protected void onUsernameIsSet() {
        generateStatus();
    }

    private void bindView(IconTitleItemHolder holder, int position) {
        //setting photo
        if (mUri != null) {
            AwesomeApplication.getImageWorker().loadImage(mUri, holder.getIcon());
        } else {
            holder.getIcon().setImageDrawable(mPhotoBackgroundDrawable);
        }

        holder.getTitle().setText(mStatus);
    }

    private void generateStatus() {
        if (systemInfo.getUserId() != UserInfo.getMe().id) {
            mStatus = getMultiFontString();
        } else {
            mStatus = getMultiFontStringSelf();
        }
    }

    private SpannableStringBuilder getMultiFontString() {
        String sourceString = String.format(mContext.getString(R.string.CHANGED_GROUP_PHOTO_INFO), getUsername());

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoBold), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff569ace), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        result.setSpan(new CustomTypefaceSpan("", robotoRegular), getUsername().length(), sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), getUsername().length(), sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }

    private SpannableStringBuilder getMultiFontStringSelf() {
        String sourceString = String.format(mContext.getString(R.string.CHANGED_GROUP_PHOTO_INFO), "You");

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoRegular), 0, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), 0, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }
}
