package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public abstract class ChatItemHolder extends AvatarItemHolder {

    private TextView mUsername;
    private TextView mTimestamp;
    private ImageView mSendingMark;
    private ImageView mNewMark;

    public ChatItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mUsername = (TextView) itemView.findViewById(params.usernameId);
        mTimestamp = (TextView) itemView.findViewById(params.timestampId);
        mSendingMark = (ImageView) itemView.findViewById(params.sendingMarkId);
        mNewMark = (ImageView) itemView.findViewById(params.newMarkId);
    }

    public TextView getUsername() {
        return mUsername;
    }

    public TextView getTimestamp() {
        return mTimestamp;
    }

    public ImageView getSendingMark() {
        return mSendingMark;
    }

    public ImageView getNewMark() {
        return mNewMark;
    }

    public static class ResourceParams extends AvatarItemHolder.ResourceParams {
        public int usernameId;
        public int timestampId;
        public int sendingMarkId;
        public int newMarkId;
    }
}
