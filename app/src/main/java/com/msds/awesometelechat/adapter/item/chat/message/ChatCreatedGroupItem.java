package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.SingleTextItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.CustomTypefaceSpan;
import com.msds.awesometelechat.util.UserInfo;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-18
 */
public class ChatCreatedGroupItem extends ChatItem {

    private SpannableStringBuilder mStatus;

    public static ChatCreatedGroupItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message) {
        ChatCreatedGroupItem result = new ChatCreatedGroupItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.generateStatus();
        return result;
    }

    protected ChatCreatedGroupItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        SingleTextItemHolder holder = (SingleTextItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_status, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new SingleTextItemHolder(view, R.id.fileInfo);
            }
        };
    }

    @Override
    protected void onUsernameIsSet() {
        generateStatus();
    }

    private void bindView(SingleTextItemHolder holder, int position) {
        holder.getTextView().setText(mStatus);
    }

    private void generateStatus() {
        if (systemInfo.getUserId() != UserInfo.getMe().id) {
            mStatus = getMultiFontString();
        } else {
            mStatus = getMultiFontStringSelf();
        }
    }

    private SpannableStringBuilder getMultiFontString() {
        String sourceString = String.format(mContext.getString(R.string.CREATED_GROUP_INFO), getUsername());

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoBold), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff569ace), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        result.setSpan(new CustomTypefaceSpan("", robotoRegular), getUsername().length(), sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), getUsername().length(), sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }

    private SpannableStringBuilder getMultiFontStringSelf() {
        String sourceString = String.format(mContext.getString(R.string.CREATED_GROUP_INFO), "You");

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoRegular), 0, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), 0, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }
}
