package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.msds.awesometelechat.view.CircularProgressBar;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatAudioItemHolder extends ChatItemHolder {

    private View mDownloadCancelOpenView;
    private CircularProgressBar mDownloadingProgress;
    private ImageView mFileStatusImage;
    private SeekBar mAudioPlaybackProgress;
    private TextView mAudioDuration;
    private RoundedImageView mStatusBackground;

    public ChatAudioItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mDownloadCancelOpenView = itemView.findViewById(params.downloadCancelOpenId);
        mDownloadingProgress = (CircularProgressBar) itemView.findViewById(params.progressId);
        mFileStatusImage = (ImageView) itemView.findViewById(params.fileStatusImageId);
        mAudioPlaybackProgress = (SeekBar) itemView.findViewById(params.audioPlaybackProgressId);
        mAudioDuration = (TextView) itemView.findViewById(params.audioDurationId);
        mStatusBackground = (RoundedImageView) itemView.findViewById(params.statusBackgroundId);
    }

    public View getDownloadCancelOpenView() {
        return mDownloadCancelOpenView;
    }

    public CircularProgressBar getDownloadingProgress() {
        return mDownloadingProgress;
    }

    public ImageView getFileStatusImage() {
        return mFileStatusImage;
    }

    public SeekBar getAudioPlaybackProgress() {
        return mAudioPlaybackProgress;
    }

    public TextView getAudioDuration() {
        return mAudioDuration;
    }

    public RoundedImageView getStatusBackground() {
        return mStatusBackground;
    }

    public static final class ResourceParams extends ChatItemHolder.ResourceParams {
        public int downloadCancelOpenId;
        public int progressId;
        public int fileStatusImageId;
        public int audioPlaybackProgressId;
        public int audioDurationId;
        public int statusBackgroundId;
    }
}
