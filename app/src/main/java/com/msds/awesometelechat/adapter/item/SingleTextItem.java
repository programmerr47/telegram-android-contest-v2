package com.msds.awesometelechat.adapter.item;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.SingleTextItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.AdapterItem;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
public abstract class SingleTextItem implements AdapterItem {

    private final Context mContext;
    private final String mText;

    public SingleTextItem(@NonNull Context context, String text) {
        this.mContext = context;
        this.mText = text;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        SingleTextItemHolder holder = (SingleTextItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(getLayoutId(), parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new SingleTextItemHolder(view, getTextViewId());
            }
        };
    }

    private void bindView(SingleTextItemHolder viewHolder, int position) {
        viewHolder.getTextView().setText(mText);
    }

    protected abstract int getLayoutId();

    protected abstract int getTextViewId();
}
