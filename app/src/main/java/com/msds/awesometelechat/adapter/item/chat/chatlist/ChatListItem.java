package com.msds.awesometelechat.adapter.item.chat.chatlist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatListItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.ChatListItemHolderProducer;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.callback.ChatListItemNotifier;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.util.ChatAvatarUtil;
import com.msds.awesometelechat.util.LocaleUtils;
import com.msds.awesometelechat.util.UserInfo;
import com.msds.awesometelechat.util.UserNameFormatter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * Common representation for every chat in chat list page.
 *
 * @author Michael Spitsin
 * @since 2015-04-24
 */
public class ChatListItem implements AdapterItem {

    private final String mAbbreviation;
    private int mAvatarbackgroundColor;

    private long mId;
    protected boolean isDeactivated;

    protected final Context mContext;
    private final String mUsername;
    private final boolean isGroup;

    private CharSequence mLastMessage;
    private Date mTimestamp;
    private int mUnreadIncomingMessagesCount;
    private boolean isSendingMark;
    private boolean isLastOutgoingMessageUnread;
    private boolean isError;

    private Bitmap mAvatarBitmap;
    private Uri mAvatarUri;

    private TdApi.File mPhotoSmall;
    private TdApi.Chat mChat;

    //TODO replace it with listener not hardcode link to fragment
    private ChatListItemNotifier mItemNotifier;

    ChatListItem(@NonNull Context context, @NonNull String username, boolean isGroup) {
        this.mContext = context;
        this.mUsername = username;
        this.isGroup = isGroup;

        this.mAbbreviation = ChatAvatarUtil.getAbbreviationFromString(username);
    }


    public static ChatListItem createFromTdApiChat(@NonNull final Context context, ChatListItemNotifier notifier, final TdApi.Chat chat) {
        ChatListItem item = null;
        TdApi.ChatInfo info = chat.type;
        if (info instanceof TdApi.PrivateChatInfo) {
            TdApi.User user = ((TdApi.PrivateChatInfo) info).user;
            item = new ChatListItem(context, UserNameFormatter.fullName(user), false);
            item.mPhotoSmall = user.photoSmall;
            item.mAvatarbackgroundColor = ChatAvatarUtil.getBackgroundAvatarColor(user.id);
            item.setLastMessage(chat.topMessage, "", null);
        } else if (info instanceof TdApi.GroupChatInfo) {
            TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) info).groupChat;
            item = new ChatListItem(context, groupChat.title, true);
            item.mPhotoSmall = groupChat.photoSmall;
            item.mAvatarbackgroundColor = ChatAvatarUtil.getBackgroundAvatarColor(groupChat.id);
            setLastMessageForGroupChat(item, chat);
        }
        if (item != null) {
            item.mItemNotifier = notifier;
            item.mChat = chat;
            item.mId = chat.id;
            if (item.mPhotoSmall instanceof TdApi.FileLocal) {
                item.setAvatar(Uri.parse(((TdApi.FileLocal) item.mPhotoSmall).path));
            }
            item.setUnreadMessagesCount(chat.unreadCount);
            setIsLastOutgoingMessageUnread(item, chat);
        }
        return item;
    }

    private static void setIsLastOutgoingMessageUnread(final ChatListItem item, final TdApi.Chat chat) {
        Client client = TG.getClientInstance();
        client.send(new TdApi.GetMe(), new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object instanceof TdApi.User) {
                    TdApi.User me = (TdApi.User) object;
                    item.setIsLastOutgoingMessageUnread(
                            chat.topMessage.fromId == me.id
                                    && chat.topMessage.id != chat.lastReadOutboxMessageId
                    );
                    item.mItemNotifier.notifyElementChanged(item);
                }
            }
        }));
    }

    private static void setLastMessageForGroupChat(final ChatListItem item, final TdApi.Chat chat) {
        final Client client = TG.getClientInstance();
        client.send(new TdApi.GetMe(), new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                TdApi.User me = (TdApi.User) object;
                if (chat.topMessage.fromId == me.id) {
                    item.setLastMessage(chat.topMessage, "You: ", me);
                    item.mItemNotifier.notifyElementChanged(item);
                } else {
                    client.send(new TdApi.GetUser(chat.topMessage.fromId), new UIResultHandler(new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            if (object instanceof TdApi.User) {
                                TdApi.User user = (TdApi.User) object;
                                item.setLastMessage(chat.topMessage, UserNameFormatter.fullName(user) + ": ", user);
                                item.mItemNotifier.notifyElementChanged(item);
                            } else if (object instanceof TdApi.Error) {
                                item.setLastMessage(null);
                                item.setLastSystemMessage(null);
                                item.setLastTime(null);
                            }
                        }
                    }));
                }
            }
        }));
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        ChatListItemHolder holder = (ChatListItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new ChatListItemHolderProducer(mContext);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatListItem that = (ChatListItem) o;

        return mId == that.mId;
    }

    @Override
    public int hashCode() {
        return (int) (mId ^ (mId >>> 32));
    }

    @Override
    public String toString() {
        return "ChatListItem{" +
                "mUsername='" + mUsername + '\'' +
                ", isGroup=" + isGroup +
                '}';
    }

    public void clear() {
        mLastMessage = null;
        mTimestamp = null;
        mUnreadIncomingMessagesCount = 0;
        isSendingMark =false;
        isLastOutgoingMessageUnread = false;
        isError = false;
    }

    public ChatListItem setLastMessage(TdApi.Message message, String senderName, TdApi.User sender) {
        isDeactivated = false;
        return ChatListLastMessageVisitor.setlastTextMessage(this, message, sender, senderName);
    }

    public ChatListItem setLastSystemMessage(String lastMessage) {
        isDeactivated = false;
        if (lastMessage != null) {
            Spannable message = new SpannableString(lastMessage);
            int color = mContext.getResources().getColor(R.color.system_message_color);
            message.setSpan(new ForegroundColorSpan(color), 0, lastMessage.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            this.mLastMessage = message;
        } else {
            this.mLastMessage = null;
        }
        return this;
    }

    public ChatListItem setLastSystemMessage(String lastMessage, TdApi.User relatedUser) {
        isDeactivated = false;
        String lastMessageArg = UserNameFormatter.fullName(relatedUser);
        if (relatedUser.id == UserInfo.getMe().id) {
            lastMessageArg = "You";
        }

        return setLastSystemMessage(String.format(lastMessage, lastMessageArg));
    }

    public ChatListItem setLastSystemMessage(int msgId, TdApi.User relatedUser) {
        ChatListItem result = setLastSystemMessage(mContext.getString(msgId), relatedUser);

        if (msgId == R.string.SYSTEM_TEXT_LEFT_GROUP && relatedUser.id == UserInfo.getMe().id) {
            isDeactivated = true;
        } else {
            isDeactivated = false;
        }

        return result;
    }

    public ChatListItem setLastMessage(String lastMessage, String sender) {
        isDeactivated = false;
        Spannable message = new SpannableString(sender + lastMessage);
        if (sender.length() != 0) {
            int color = mContext.getResources().getColor(R.color.system_message_color);
            message.setSpan(new ForegroundColorSpan(color), 0, sender.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        this.mLastMessage = message;
        return this;
    }

    public ChatListItem setLastMessage(String lastMessage) {
        isDeactivated = false;

        this.mLastMessage = lastMessage;
        return this;
    }

    public ChatListItem setLastTime(Date timestamp) {
        this.mTimestamp = timestamp;
        return this;
    }

    public ChatListItem setUnreadMessagesCount(int unreadMessagesCount) {
        this.mUnreadIncomingMessagesCount = unreadMessagesCount;
        return this;
    }

    public int getUnreadMessagesCount() {
        return mUnreadIncomingMessagesCount;
    }

    public ChatListItem setSendingMark(boolean sendingMark) {
        this.isSendingMark = sendingMark;
        return this;
    }

    public ChatListItem setIsLastOutgoingMessageUnread(boolean isLastMessageRead) {
        this.isLastOutgoingMessageUnread = isLastMessageRead;
        return this;
    }

    public boolean isLastOutgoingMessageUnread() {
        return isLastOutgoingMessageUnread;
    }

    public ChatListItem setIsError(boolean isError) {
        this.isError = isError;
        return this;
    }

    public ChatListItem setAvatar(Bitmap avatarBitmap) {
        this.mAvatarBitmap = avatarBitmap;
        return this;
    }

    public ChatListItem setAvatar(Uri avatarUri) {
        this.mAvatarUri = avatarUri;
        return this;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public boolean isDeactivated() {
        return isDeactivated;
    }

    public CharSequence getLastMessage(){
        return mLastMessage;
    }

    public long getId() {
        return mId;
    }

    public TdApi.File getPhotoSmall() {
        return mPhotoSmall;
    }

    public TdApi.Chat getChat() {
        return mChat;
    }

    public Context getContext() {
        return mContext;
    }

    private void bindView(ChatListItemHolder holder, int position) {
        //Setting bg
        if (isDeactivated) {
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.gray_light_very));
        } else {
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.white_pure));
        }

        //Setting avatar
        if (mAvatarBitmap != null || mAvatarUri != null) {
//            holder.getAvatarAbbreviation().setVisibility(View.INVISIBLE);
//            holder.getBackgroundAvatar().setVisibility(View.INVISIBLE);
            holder.getAvatarAbbreviation().setText(mAbbreviation);
            holder.getBackgroundAvatar().setImageDrawable(new ColorDrawable(mAvatarbackgroundColor));

            holder.getAvatar().setVisibility(View.VISIBLE);

            if (mAvatarBitmap != null) {
                holder.getAvatar().setImageBitmap(mAvatarBitmap);
            } else {
                //Todo compute avatar size (width and height) and pass ass params
                AwesomeApplication.getImageWorker().loadImage(
                        mAvatarUri,
                        holder.getAvatar(),
                        new UriImageWorker.LoadingImageParams()
                                .setCircle(true));
            }
        } else {
            holder.getAvatar().setVisibility(View.INVISIBLE);
            holder.getAvatarAbbreviation().setVisibility(View.VISIBLE);
            holder.getBackgroundAvatar().setVisibility(View.VISIBLE);

            holder.getAvatarAbbreviation().setText(mAbbreviation);
            holder.getBackgroundAvatar().setImageDrawable(new ColorDrawable(mAvatarbackgroundColor));
        }

        //Setting marks
        if (isGroup) {
            holder.getGroupMark().setVisibility(View.VISIBLE);
        } else {
            holder.getGroupMark().setVisibility(View.GONE);
        }

        if (isError) {
            holder.getErrorMark().setVisibility(View.VISIBLE);
        } else {
            holder.getErrorMark().setVisibility(View.INVISIBLE);
        }

        if (isLastOutgoingMessageUnread) {
            holder.getUnreadOutgoingMessagesMark().setVisibility(View.VISIBLE);
        } else {
            holder.getUnreadOutgoingMessagesMark().setVisibility(View.GONE);
        }

        if (isSendingMark) {
            holder.getClockMark().setVisibility(View.VISIBLE);
        } else {
            holder.getClockMark().setVisibility(View.GONE);
        }

        //Setting counter
        if (mUnreadIncomingMessagesCount > 0) {
            holder.getUnreadCount().setVisibility(View.VISIBLE);
            holder.getUnreadCount().setText(String.valueOf(mUnreadIncomingMessagesCount));
        } else {
            holder.getUnreadCount().setVisibility(View.INVISIBLE);
        }

        //Setting timestamp
        if (mTimestamp != null) {
            holder.getTimestamp().setVisibility(View.VISIBLE);
            holder.getTimestamp().setText(LocaleUtils.formatDateForChatListElement(mTimestamp));
        } else {
            holder.getTimestamp().setVisibility(View.INVISIBLE);
        }

        //Setting last message text
        if (mLastMessage != null) {
            holder.getText().setVisibility(View.VISIBLE);
            holder.getText().setText(mLastMessage);
        } else {
            holder.getText().setVisibility(View.INVISIBLE);
        }

        //Setting user or group name
        holder.getUsername().setText(mUsername);
    }
}
