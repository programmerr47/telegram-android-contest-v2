package com.msds.awesometelechat.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Michael Spitsin
 * @since 2015-05-11
 */
public abstract class AvatarItemHolder extends RecyclerView.ViewHolder {

    private ImageView mAvatar;
    private ImageView mBackgroundAvatar;
    private TextView mAvatarAbbreviation;

    public AvatarItemHolder(View itemView, ResourceParams params) {
        super(itemView);

        mAvatar = (ImageView) itemView.findViewById(params.avatarLayoutId);
        mBackgroundAvatar = (ImageView) itemView.findViewById(params.backgroundAvatarLayoutId);
        mAvatarAbbreviation = (TextView) itemView.findViewById(params.avatarAbbreviationId);
    }

    public ImageView getAvatar() {
        return mAvatar;
    }

    public ImageView getBackgroundAvatar() {
        return mBackgroundAvatar;
    }

    public TextView getAvatarAbbreviation() {
        return mAvatarAbbreviation;
    }

    public static class ResourceParams {
        public int avatarLayoutId;
        public int backgroundAvatarLayoutId;
        public int avatarAbbreviationId;
    }
}