package com.msds.awesometelechat.adapter.item.drawer;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.DrawerHeaderItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.util.ChatAvatarUtil;
import com.msds.awesometelechat.util.PhoneFormat.PhoneFormat;

/**
 * @author Michael Spitsin
 * @since 2015-05-08
 */
public final class DrawerHeaderItem extends DrawerItem {

    private final String mAbbreviation;
    private final String mUsername;
    private final String mPhoneNumber;
    private final int mAvatarbackgroundColor;

    private Uri mAvatarUri;

    private DrawerHeaderItem(Builder builder) {
        super(builder);

        this.mUsername = builder.username;
        this.mAvatarUri = builder.avatarUri;
        this.mPhoneNumber = builder.phoneNumber;

        this.mAvatarbackgroundColor = ChatAvatarUtil.getBackgroundAvatarColor(builder.userId);
        this.mAbbreviation = ChatAvatarUtil.getAbbreviationFromString(mUsername);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        DrawerHeaderItemHolder holder = (DrawerHeaderItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.drawer_header_item, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                DrawerHeaderItemHolder.ResourceParams params = new DrawerHeaderItemHolder.ResourceParams();
                params.avatarLayoutId = R.id.avatar;
                params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
                params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
                params.phoneNumberLayoutId = R.id.phoneNumber;
                params.usernameLayoutId = R.id.username;
                return new DrawerHeaderItemHolder(view, params);
            }
        };
    }

    public void setAvatarUri(Uri avatarUri) {
        this.mAvatarUri = avatarUri;
    }

    private void bindView(DrawerHeaderItemHolder holder, int position) {
        //setting avatar
        if (mAvatarUri != null) {
//            holder.getAvatarAbbreviation().setVisibility(View.INVISIBLE);
//            holder.getBackgroundAvatar().setVisibility(View.INVISIBLE);
            holder.getAvatarAbbreviation().setText(mAbbreviation);
            holder.getBackgroundAvatar().setImageDrawable(new ColorDrawable(mAvatarbackgroundColor));

            holder.getAvatar().setVisibility(View.VISIBLE);

            AwesomeApplication.getImageWorker().loadImage(
                    mAvatarUri,
                    holder.getAvatar(),
                    new UriImageWorker.LoadingImageParams()
                            .setCircle(true));
        } else {
            holder.getAvatar().setVisibility(View.INVISIBLE);
            holder.getAvatarAbbreviation().setVisibility(View.VISIBLE);
            holder.getBackgroundAvatar().setVisibility(View.VISIBLE);

            holder.getAvatarAbbreviation().setText(mAbbreviation);
            holder.getBackgroundAvatar().setImageDrawable(new ColorDrawable(mAvatarbackgroundColor));
        }

        //settings strings info
        holder.getUsername().setText(mUsername);
        holder.getPhoneNumber().setText(PhoneFormat.getInstance().format('+' + mPhoneNumber));
    }

    /**
     * @author Michael Spitsin
     * @since 2014-10-12
     */
    public static class Builder extends DrawerItem.Builder {
        private String username;
        private String phoneNumber;
        private int userId;
        private Uri avatarUri;

        public Builder(@NonNull Context context) {
            super(context);
        }

        @Override
        public Builder setName(DrawerElementName name) {
            super.setName(name);
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder setUserId(int userId) {
            this.userId = userId;
            return this;
        }

        public Builder setAvatarUri(Uri avatarUri) {
            this.avatarUri = avatarUri;
            return this;
        }

        public DrawerHeaderItem build() {
            return new DrawerHeaderItem(this);
        }
    }
}
