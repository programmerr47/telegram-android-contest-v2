package com.msds.awesometelechat.adapter;

import android.support.annotation.NonNull;

import com.msds.awesometelechat.AnimationListener;
import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.adapter.item.chat.DateItem;
import com.msds.awesometelechat.adapter.item.chat.LoadingItem;
import com.msds.awesometelechat.adapter.item.chat.NewMessagesItem;
import com.msds.awesometelechat.adapter.item.chat.message.ChatItem;
import com.msds.awesometelechat.adapter.item.chat.message.ChatItemCreator;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;
import com.msds.awesometelechat.callback.ChatItemNotifier;
import com.msds.awesometelechat.callback.ChatScrollManager;
import com.msds.awesometelechat.collections.MessagesManager;
import com.msds.awesometelechat.collections.ParticipantsManager;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.concurrent.tasks.AsyncTaskWithListener;
import com.msds.awesometelechat.util.LocaleUtils;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-05-26
 */
//todo fill with logic
public class ChatAdapter extends AbstractMultiTypeRecyclerAdapter<AdapterItem> implements
        ChatItemNotifier,
        AnimationListener {

    private MessagesManager mMessagesManager;

    private NewMessagesItem mNewMessagesItem = new NewMessagesItem(AwesomeApplication.getAppContext(), 0);
    private LoadingItem mLoadingItem = new LoadingItem(AwesomeApplication.getAppContext());

    private LazyKeeper mLazyKeeper = new LazyKeeper();
    private TopMessagesQueue mMessagesQueue = new TopMessagesQueue();

    private ChatScrollManager mScrollManager;

    private boolean isLoading;
    private boolean isSomeAnimationIsPerforming;
    private int mUnreadMessagesCount;

    public ChatAdapter(@NonNull MessagesManager messagesManager, @NonNull RecyclerItems<AdapterItem> container) {
        super(container);
        init(messagesManager);
    }

    public ChatAdapter(@NonNull MessagesManager messagesManager, @NonNull List<AdapterItem> container) {
        super(container);
        init(messagesManager);
    }

    @Override
    public void notifyItemChanged(AdapterItem item) {
        int position = mItems.indexOf(item);
        if (position != -1) {
            notifyItemChanged(position);
        }
    }

    @Override
    public void notifyFileItemCreating(FileableItem item) {
        Set<FileableItem> photoItems = mMessagesManager.getFileMessagesByFileId().get(item.getFileId());
        if (photoItems == null) {
            photoItems = new HashSet<>();
            mMessagesManager.getFileMessagesByFileId().put(item.getFileId(), photoItems);
        }

        mMessagesManager.getFileMessagesByFileId().get(item.getFileId()).add(item);
    }

    @Override
    public void notifyThumbItemCreating(FileableItem item) {
        Set<FileableItem> thumbItems = mMessagesManager.getMessagesWithThumbByThumbId().get(item.getThumbId());
        if (thumbItems == null) {
            thumbItems = new HashSet<>();
            mMessagesManager.getMessagesWithThumbByThumbId().put(item.getThumbId(), thumbItems);
        }

        mMessagesManager.getMessagesWithThumbByThumbId().get(item.getThumbId()).add(item);
    }

    @Override
    public void onAnimationEnd() {
        isSomeAnimationIsPerforming = false;
        if (hasItemsInQueue()) {
            releaseQueue();

            if (mScrollManager != null) {
                mScrollManager.scrollToTop();
            }
        }
    }

    @Override
    public void onAnimationStart() {
        isSomeAnimationIsPerforming = true;
    }

    public void setScrollManager(ChatScrollManager scrollManager) {
        mScrollManager = scrollManager;
        int t = 5;
    }

    public void processLastMessage(ParticipantsManager participantsManager, TdApi.Message lastMessage, boolean sending) {
        NewItemHolder holder = processMessageInternal(participantsManager, lastMessage, sending);
        addItemToTop(holder.newItem, holder.dateItem, sending);
    }

    public LazyKeeper getLazyKeeper() {
        return mLazyKeeper;
    }

    public void releaseLazyKeeper() {
        mMessagesManager.addAll(mLazyKeeper.mMessagesManager);
        connectNewPart(mLazyKeeper.mRepresentationList);
        mLazyKeeper.prepare();
    }

    public void setLoadingState(boolean isLoading) {
        if (isLoading) {
            if (mItems.indexOf(mLoadingItem) == -1) {
                mItems.add(mLoadingItem);
                notifyItemInserted(mItems.size() - 1);
            }
        } else {
            int position = mItems.indexOf(mLoadingItem);

            if (position != -1) {
                mItems.remove(mLoadingItem);
                notifyItemRemoved(position);
            }
        }
    }

    private NewItemHolder processMessageInternal(ParticipantsManager participantsManager, TdApi.Message lastMessage, boolean sending) {
        final ChatItem newItem = ChatItemCreator.INSTANCE.create(lastMessage, this);
        newItem.setSending(sending);
        participantsManager.setUsernameAndPhotoForItem(newItem, this, lastMessage);

        int datesComparisonResult;
        if (mMessagesQueue.hasItems()) {
            ChatItem previousItem = mMessagesQueue.mMessagesManager.getMessages().get(0);
            datesComparisonResult = LocaleUtils.compareDatesByDay(previousItem.getTimestamp(), newItem.getTimestamp());
        } else {
            if (getItemCount() > 0) {
                ChatItem previousItem = mMessagesManager.getMessages().get(0);
                datesComparisonResult = LocaleUtils.compareDatesByDay(previousItem.getTimestamp(), newItem.getTimestamp());
            } else {
                datesComparisonResult = -1;
            }
        }

        DateItem dateSeparatorItem = null;
        if (datesComparisonResult != 0) {
            dateSeparatorItem = new DateItem(AwesomeApplication.getAppContext(), newItem.getTimestamp());
        }

        NewItemHolder holder = new NewItemHolder();
        holder.dateItem = dateSeparatorItem;
        holder.newItem = newItem;
        return holder;
    }

    private void addItemToTop(ChatItem item, DateItem dateItem, boolean sending) {
        if (dateItem != null) {
            mItems.add(0, dateItem);
        }

        mItems.add(0, item);
        mMessagesManager.getMessages().add(0, item);
        mMessagesManager.getMapMessagesById().put(item.systemInfo.getMessageId(), item);

        if (dateItem != null) {
            notifyItemRangeInserted(0, 2);
        } else {
            notifyItemInserted(0);
        }

        checkNewMessagesItem(sending);
    }

    private void init(MessagesManager messagesManager) {
        this.mMessagesManager = messagesManager;
        mItems.clear();
        formatMessages(mMessagesManager.getMessages(), mItems);
    }

    private void checkNewMessagesItem(boolean isYouSendLastMessage) {
        checkNewMessagesItem(isYouSendLastMessage, 1);
    }

    private void checkNewMessagesItem(boolean isYouSendLastMessages, int countOfLastMessages) {
        if (mUnreadMessagesCount != 0) {
            int position = mItems.indexOf(mNewMessagesItem);

            if (position != -1) {
                if (isYouSendLastMessages) {
                    mUnreadMessagesCount = 0;
                    mItems.remove(mNewMessagesItem);
                    notifyItemRemoved(position);
                } else {
                    mUnreadMessagesCount+=countOfLastMessages;
                    mNewMessagesItem.setCount(mUnreadMessagesCount);
                    notifyItemChanged(position);
                }
            }
        }
    }

    private void connectNewPart(RecyclerItems<AdapterItem> formattedMessagesItems) {
        if (formattedMessagesItems.size() > 0) {
            if (mItems.size() > 0) {
                int position = mItems.size() - 1;
                mItems.remove(position);
                notifyItemRemoved(position);
            }

            int realOffset = mItems.size();
            mItems.addAll(formattedMessagesItems);
            mItems.add(new DateItem(AwesomeApplication.getAppContext(), mMessagesManager.getLastItem().getTimestamp()));
            int realLimit = mItems.size() - realOffset;
            notifyItemRangeInserted(realOffset, realLimit);
        }
    }

    private void connectNewPartToTop(MessagesManager newMessagesManagerPart, RecyclerItems<AdapterItem> formattedMessagesItems) {
        if (formattedMessagesItems.size() > 0) {
            mMessagesManager.addAllToTop(newMessagesManagerPart);
            int realLimit = formattedMessagesItems.size();
            mItems.addAll(0, formattedMessagesItems);
            notifyItemRangeInserted(0, realLimit);
            checkNewMessagesItem(true, newMessagesManagerPart.messagesCount());
        }
    }

    private boolean hasItemsInQueue() {
        return mMessagesQueue.hasItems();
    }

    private void releaseQueue() {
        connectNewPartToTop(mMessagesQueue.mMessagesManager, mMessagesQueue.mRepresentationList);
        mMessagesQueue.prepare();
    }

    /**
     * Convert simple messages list to formatted messages list with dates and "new messages" item
     *
     * @param sourceMessages
     * @param targetChatList
     */
    private void formatMessages(RecyclerItems<ChatItem> sourceMessages, RecyclerItems<AdapterItem> targetChatList) {
        //TODO fill
    }

    public static abstract class MessageSender<Params, Progress, Result> extends AsyncTaskWithListener<Params, Progress, Result> {

        private ChatAdapter mAdapter;
        private TdApi.Chat mChat;
        private ParticipantsManager mParticipantsManager;

        public MessageSender(ChatAdapter adapter, TdApi.Chat chat, ParticipantsManager participantsManager) {
            mAdapter = adapter;
            mChat = chat;
            mParticipantsManager = participantsManager;
        }

        protected NewItemHolder processSending(TdApi.Message message) {
            return mAdapter.processMessageInternal(mParticipantsManager, message, true);
        }

        protected void addNewItemToChat(final NewItemHolder itemHolder) {
            AwesomeApplication.getUiHandler().post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.addItemToTop(itemHolder.newItem, itemHolder.dateItem, true);

                    if (mAdapter.mScrollManager != null) {
                        mAdapter.mScrollManager.scrollToTop();
                    }
                }
            });
        }

        protected void addNewItemToQueue(final NewItemHolder itemHolder) {
            mAdapter.mMessagesQueue.addMessageToQueue(itemHolder.newItem, itemHolder.dateItem);
        }

        protected boolean isAnimationPerformingInUi() {
            return mAdapter.isSomeAnimationIsPerforming;
        }

        protected TdApi.Chat getChat() {
            return mChat;
        }

        protected ParticipantsManager getParticipantsManager() {
            return mParticipantsManager;
        }
    }

    public static class NewItemHolder {
        private ChatItem newItem;
        private DateItem dateItem;

        private NewItemHolder() {
        }

        public ChatItem getNewItem() {
            return newItem;
        }
    }

    /**
     * Use his own collection container for keeping additional items, that will be added later.
     * Most common usage to fill adapter in background thread and then notify in UI.
     * Unfortunately it can't be done directly changing adapter container list, because
     * then we have liquid bug with updating items. As far as known Recycler view sometimes
     * fetch adapter container changes. So this class provides save logic.
     *
     * @author Michael Spitsin
     * @since 2015-05-27
     */
    public final class LazyKeeper {

        private RecyclerItems<AdapterItem> mRepresentationList = new RecyclerItems<>(new ArrayList<AdapterItem>());
        private MessagesManager mMessagesManager = new MessagesManager(new ArrayList<ChatItem>());

        private LazyKeeper() {}

        public void prepare() {
            mRepresentationList.clear();
            mMessagesManager.clear();
        }

        public void addNewItem(ChatItem newItem) {
            //TODO
//            if (message.fromId == UserInfo.getMe().id) {
//                if (message.id == mLastReadOutboxMessageId) {
//                    mHasLastReadOutboxMessage = true;
//                } else if (!mHasLastReadOutboxMessage) {
//                    item.setNew(true);
//                    mUnreadMessages.add(item);
//                }
//            }

            int datesComparisonResult = 0;
            Date lastDate = null;
            ChatItem lastItem = getLastItem();
            if (lastItem != null) {
                lastDate = lastItem.getTimestamp();
                datesComparisonResult = LocaleUtils.compareDatesByDay(lastItem.getTimestamp(), newItem.getTimestamp());
            }

            if (datesComparisonResult != 0) {
                DateItem dateSeparatorItem = new DateItem(AwesomeApplication.getAppContext(), lastDate);
                mRepresentationList.add(dateSeparatorItem);
            }

            if (ChatAdapter.this.mMessagesManager.messagesCount() + mMessagesManager.messagesCount() == mUnreadMessagesCount && mUnreadMessagesCount > 1) {
                mRepresentationList.add(mNewMessagesItem);
            }

            mMessagesManager.addNewMessage(newItem);
            mRepresentationList.add(newItem);
        }

        public int getMessagesCount() {
            return mMessagesManager.messagesCount();
        }

        private ChatItem getLastItem() {
            ChatItem lastItem = mMessagesManager.getLastItem();
            if (lastItem != null) {
                return lastItem;
            } else {
                return ChatAdapter.this.mMessagesManager.getLastItem();
            }
        }
    }

    private final class TopMessagesQueue {
        private RecyclerItems<AdapterItem> mRepresentationList = new RecyclerItems<>(new LinkedList<AdapterItem>());
        private MessagesManager mMessagesManager = new MessagesManager(new LinkedList<ChatItem>());

        private void prepare() {
            mRepresentationList.clear();
            mMessagesManager.clear();
        }

        private boolean hasItems() {
            return mRepresentationList.size() != 0;
        }

        private ChatItem getTopItem() {
            if (mMessagesManager.getMessages().size() > 0) {
                return mMessagesManager.getMessages().get(0);
            } else if (ChatAdapter.this.mMessagesManager.getMessages().size() > 0) {
                return ChatAdapter.this.mMessagesManager.getMessages().get(0);
            } else {
                return null;
            }
        }

        private void addMessageToQueue(ChatItem chatItem, DateItem dateItem) {
            if (dateItem != null) {
                mRepresentationList.add(0, dateItem);
            }

            mRepresentationList.add(0, chatItem);
            mMessagesManager.getMessages().add(0, chatItem);
            mMessagesManager.getMapMessagesById().put(chatItem.systemInfo.getMessageId(), chatItem);
        }
    }
}
