package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Holder for view representation of chats list item.
 *
 * @author Michael Spitsin
 * @since 2015-04-24
 */
public final class ChatListItemHolder extends AvatarItemHolder {

    private ImageView mClockMark;
    private ImageView mErrorMark;
    private ImageView mGroupMark;
    private ImageView mUnreadOutgoingMessagesMark;

    private TextView mText;
    private TextView mTimestamp;
    private TextView mUsername;
    private TextView mUnreadCount;

    public ChatListItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mClockMark = (ImageView) itemView.findViewById(params.clockMark);
        mErrorMark = (ImageView) itemView.findViewById(params.errorMark);
        mGroupMark = (ImageView) itemView.findViewById(params.groupMark);
        mUnreadOutgoingMessagesMark = (ImageView) itemView.findViewById(params.unreadOutgoingMessagesMark);

        mText = (TextView) itemView.findViewById(params.text);
        mTimestamp = (TextView) itemView.findViewById(params.timestamp);
        mUsername = (TextView) itemView.findViewById(params.username);
        mUnreadCount = (TextView) itemView.findViewById(params.unreadCount);
    }

    public ImageView getClockMark() {
        return mClockMark;
    }

    public ImageView getErrorMark() {
        return mErrorMark;
    }

    public ImageView getGroupMark() {
        return mGroupMark;
    }

    public ImageView getUnreadOutgoingMessagesMark() {
        return mUnreadOutgoingMessagesMark;
    }

    public TextView getText() {
        return mText;
    }

    public TextView getTimestamp() {
        return mTimestamp;
    }

    public TextView getUsername() {
        return mUsername;
    }

    public TextView getUnreadCount() {
        return mUnreadCount;
    }

    public static final class ResourceParams extends AvatarItemHolder.ResourceParams {
        public int clockMark;
        public int errorMark;
        public int groupMark;
        public int unreadOutgoingMessagesMark;
        public int text;
        public int timestamp;
        public int username;
        public int unreadCount;
    }
}
