package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.SingleTextItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.CustomTypefaceSpan;
import com.msds.awesometelechat.util.UserInfo;
import com.msds.awesometelechat.util.UserNameFormatter;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-18
 */
public class ChatRemoveParticipantItem extends ChatItem {

    private String mParticipantName;
    private int mUserId;

    private SpannableStringBuilder mStatus;

    public static ChatRemoveParticipantItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message) {
        TdApi.MessageChatDeleteParticipant content = (TdApi.MessageChatDeleteParticipant) message.message;
        ChatRemoveParticipantItem result = new ChatRemoveParticipantItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.mParticipantName = UserNameFormatter.fullName(content.user);
        result.mUserId = content.user.id;
        result.generateStatus();
        return result;
    }

    protected ChatRemoveParticipantItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        SingleTextItemHolder holder = (SingleTextItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_status, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new SingleTextItemHolder(view, R.id.fileInfo);
            }
        };
    }

    @Override
    protected void onUsernameIsSet() {
        generateStatus();
    }

    private void bindView(SingleTextItemHolder holder, int position) {
        holder.getTextView().setText(mStatus);
    }

    private void generateStatus() {
        if (mUserId == systemInfo.getUserId()) {
            mStatus = getMultiFontStringSelf();
        } else if (systemInfo.getUserId() != UserInfo.getMe().id) {
            mStatus = getMultiFontString();
        } else {
            mStatus = getMultiFontStringSenderSelf();
        }
    }

    private SpannableStringBuilder getMultiFontString() {
        String sourceString = String.format(mContext.getString(R.string.REMOVAL_INFO), getUsername(), mParticipantName);
        int defaultPartLength = sourceString.length() - getUsername().length() - mParticipantName.length();

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoBold), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff569ace), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        result.setSpan(new CustomTypefaceSpan("", robotoRegular), getUsername().length(), getUsername().length() + defaultPartLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), getUsername().length(), getUsername().length() + defaultPartLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        result.setSpan(new CustomTypefaceSpan("", robotoBold), getUsername().length() + defaultPartLength, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff569ace), getUsername().length() + defaultPartLength, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }

    private SpannableStringBuilder getMultiFontStringSelf() {
        String sourceString = String.format(mContext.getString(R.string.LEFT_GROUP_INFO), getUsername());

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoBold), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff569ace), 0, getUsername().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        result.setSpan(new CustomTypefaceSpan("", robotoRegular), getUsername().length(), sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), getUsername().length(), sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }

    private SpannableStringBuilder getMultiFontStringSenderSelf() {
        String sourceString = String.format(mContext.getString(R.string.REMOVAL_INFO), "You", mParticipantName);
        int defaultPartLength = sourceString.length() - mParticipantName.length();

        Typeface robotoRegular = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(mContext.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoRegular), 0, defaultPartLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff333333), 0, defaultPartLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        result.setSpan(new CustomTypefaceSpan("", robotoBold), defaultPartLength, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new ForegroundColorSpan(0xff569ace), defaultPartLength, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }
}
