package com.msds.awesometelechat.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.collections.Countries;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.country.CountrySectionItem;
import com.msds.awesometelechat.adapter.item.section.SectionHeaderItem;
import com.msds.awesometelechat.adapter.item.section.SectionItem;
import com.msds.awesometelechat.login.OnCountrySelectedListener;
import com.tonicartos.superslim.GridSLM;
import com.tonicartos.superslim.LayoutManager;
import com.tonicartos.superslim.LinearSLM;

import java.util.List;

import static com.msds.awesometelechat.collections.Countries.Country;

/**
 * @author Michael Spitsin
 * @since 2015-04-05
 */
//todo descr
public class CountriesAdapter extends AbstractMultiTypeRecyclerAdapter<SectionItem> {

    private Context mContext;
    private OnCountrySelectedListener mListener;

    public CountriesAdapter(@NonNull Context context, @NonNull List<SectionItem> container) {
        super(fillAdapter(context, container));
        this.mContext = context;
    }

    public CountriesAdapter(@NonNull Context context, @NonNull List<SectionItem> container, OnCountrySelectedListener listener) {
        super(fillAdapter(context, container));
        this.mContext = context;
        this.mListener = listener;
    }

    public void setOnCountrySelectedListener(OnCountrySelectedListener listener) {
        this.mListener = listener;
    }

    public final boolean isItemHeader(int position) {
        return position == getItem(position).getSectionPositionInList();
    }

    @Override
    protected void onPostBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final SectionItem item = getItem(position);
        final GridSLM.LayoutParams lp = GridSLM.LayoutParams.from(holder.itemView.getLayoutParams());

        if (isItemHeader(position)) {
            lp.headerDisplay = LayoutManager.LayoutParams.HEADER_STICKY;

            if (lp.isHeaderInline() || lp.isHeaderOverlay()) {
                lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
            } else {
                lp.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            }

            lp.headerEndMarginIsAuto = false;
            lp.headerEndMarginIsAuto = false;
        } else {
            final CountrySectionItem countryItem = (CountrySectionItem) item;
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onSelect(countryItem.getCountry());
                    }
                }
            });
        }

        lp.setSlm(LinearSLM.ID);
        lp.setColumnWidth(mContext.getResources().getDimensionPixelSize(R.dimen.grid_column_width));
        lp.setFirstPosition(item.getSectionPositionInList());
        holder.itemView.setLayoutParams(lp);
    }

    private static RecyclerItems<SectionItem> fillAdapter(Context context, List<SectionItem> container){
        container.clear();

        RecyclerItems<SectionItem> result = new RecyclerItems<>(container);

        Countries countries = Countries.getInstance();
        List<Country> countryList = countries.getCountries();

        String lastAbbr = "";
        int sectionPosition = 0;
        int sectionCount = 0;
        for (int i = 0; i < countryList.size(); i++) {
            String abbr = countryList.get(i).getName().substring(0, 1);
            if (!lastAbbr.equals(abbr)) {
                sectionPosition = i + sectionCount;
                lastAbbr = abbr;
                sectionCount++;
                result.add(new SectionHeaderItem(context, abbr, sectionPosition));
            }

            result.add(new CountrySectionItem(context,countryList.get(i), sectionPosition));
        }

        return result;
    }
}
