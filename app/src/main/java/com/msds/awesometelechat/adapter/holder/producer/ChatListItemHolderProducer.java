package com.msds.awesometelechat.adapter.holder.producer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatListItemHolder;

/**
 * Class that produces view holder for
 * {@link com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem}.
 *
 * @author Michael Spitsin
 * @since 2015-04-24
 */
public class ChatListItemHolderProducer implements HolderProducer {

    private final Context mContext;

    public ChatListItemHolderProducer(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder produce(ViewGroup parentView) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.chatter_item, parentView, false);

        if (view == null) {
            throw new IllegalStateException("View not created");
        }

        ChatListItemHolder.ResourceParams params = new ChatListItemHolder.ResourceParams();
        params.avatarLayoutId = R.id.avatar;
        params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
        params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
        params.clockMark = R.id.clockMark;
        params.errorMark = R.id.errorMark;
        params.groupMark = R.id.groupMark;
        params.text = R.id.text;
        params.timestamp = R.id.timestamp;
        params.unreadCount = R.id.unreadCount;
        params.unreadOutgoingMessagesMark = R.id.unreadMark;
        params.username = R.id.username;
        return new ChatListItemHolder(view, params);
    }
}
