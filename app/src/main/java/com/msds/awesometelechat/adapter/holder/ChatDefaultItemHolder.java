package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.msds.awesometelechat.adapter.item.chat.message.ChatDefaultItem;

/**
 * @author Michael Spitsin
 * @since 2015-05-11
 */
public final class ChatDefaultItemHolder extends ChatItemHolder {

    private TextView mMessage;

    public ChatDefaultItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mMessage = (TextView) itemView.findViewById(params.messageId);
    }

    public TextView getMessage() {
        return mMessage;
    }

    public static final class ResourceParams extends ChatItemHolder.ResourceParams {
        public int messageId;
    }
}
