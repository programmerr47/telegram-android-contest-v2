package com.msds.awesometelechat.adapter.item.drawer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.IconTitleItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;

/**
 * Represents drawer elements that show primary pages.
 *
 * @author Michael Spitsin
 * @since 2015-05-08
 */
public final class DrawerPrimaryItem extends DrawerItem {

    private int iconResId;
    private int titleResId;

    private DrawerPrimaryItem(Builder builder) {
        super(builder);

        this.iconResId = builder.iconResId;
        this.titleResId = builder.titleResId;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        IconTitleItemHolder holder = (IconTitleItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.drawer_primary_item, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                IconTitleItemHolder.ResourceParams params = new IconTitleItemHolder.ResourceParams();
                params.iconLayoutId = R.id.icon;
                params.titleLayoutId = R.id.title;
                return new IconTitleItemHolder(view, params);
            }
        };
    }

    private void bindView(IconTitleItemHolder holder, int position) {
        ImageView icon = holder.getIcon();
        TextView title = holder.getTitle();

        icon.setImageResource(iconResId);
        title.setText(titleResId);
    }

    /**
     * @author Michael Spitsin
     * @since 2014-10-12
     */
    public static final class Builder extends DrawerItem.Builder {
        private int iconResId;
        private int titleResId;

        public Builder(@NonNull Context context) {
            super(context);
        }

        public Builder setName(DrawerElementName name) {
            super.setName(name);
            return this;
        }

        public Builder setIcon(int iconResId) {
            this.iconResId = iconResId;
            return this;
        }

        public Builder setTitle(int titleResId) {
            this.titleResId = titleResId;
            return this;
        }

        @Override
        public DrawerPrimaryItem build() {
            return new DrawerPrimaryItem(this);
        }
    }
}
