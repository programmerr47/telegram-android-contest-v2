package com.msds.awesometelechat.adapter.item.photo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.PhotoItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.util.AnimationUtil;

/**
 * @author Michael Spitsin
 * @since 2015-05-16F
 */
public class PhotoItem implements AdapterItem {

    private Context mContext;

    private Uri mThumbnailUri;
    private Uri mOriginalUri;

    private int mOrientation;
    private int mThumbnailWidth;
    private int mThumbnailHeight;

    private boolean isChecked;
    private boolean isChecking;
    private boolean isUnchecking;

    private PhotoItem(Builder builder) {
        this.mContext = builder.mContext;
        this.mThumbnailUri = builder.mThumbnailUri;
        this.mOriginalUri = builder.mOriginalUri;
        this.mOrientation = builder.mOrientation;
        this.mThumbnailWidth = builder.mWidth;
        this.mThumbnailHeight = builder.mHeight;
    }


    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        PhotoItemHolder holder = (PhotoItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {

            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.attachment_photo_thumb, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                PhotoItemHolder.ResourceParams params = new PhotoItemHolder.ResourceParams();
                params.checkImageboxId = R.id.checkbox;
                params.imageContainerId = R.id.image_thumb;
                params.darkCoverViewId= R.id.dark_cover;

                return new PhotoItemHolder(view, params);
            }
        };
    }

    private void bindView(PhotoItemHolder holder, int position) {
        AwesomeApplication.getImageWorker().loadImage(
                mThumbnailUri,
                holder.getImageContainer(),
                new UriImageWorker.LoadingImageParams()
                        .setActualOrientation(mOrientation));

        Log.v("PHOTO_ITEM", "Item with position " + position + " is checked = " + isChecked + " and process: " + isChecking + "; " + isUnchecking);
        if (isChecking || isUnchecking) {
            Animator animator;

            if (isChecking) {
                animator = AnimationUtil.getCheckViewAnimator(mContext, holder.getCheckboxImage(), holder.getDarkCoverView());
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        isChecking = false;
                        isUnchecking= false;
                        isChecked = true;
                    }
                });
            } else {
                animator = AnimationUtil.getUnCheckViewAnimator(mContext, holder.getCheckboxImage(), holder.getDarkCoverView());
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        isChecking = false;
                        isUnchecking= false;
                        isChecked = false;
                    }
                });
            }

            animator.start();
        } else {
            holder.getCheckboxImage().setVisibility(isChecked ? View.VISIBLE : View.GONE);
            holder.getDarkCoverView().setVisibility(isChecked ? View.VISIBLE : View.GONE);

            if (isChecked) {
                holder.getDarkCoverView().setAlpha(0.2f);
                holder.getCheckboxImage().setXFraction(1.0f);
            } else {
                holder.getDarkCoverView().setAlpha(0.0f);
                holder.getCheckboxImage().setXFraction(0.0f);
            }
        }
    }

    public void setChecked(boolean isChecked) {
        if (this.isChecked != isChecked) {
            if (isChecked) {
                isChecking = true;
                isUnchecking = false;
            } else {
                isUnchecking = true;
                isChecking = false;
            }
        }
    }

    public boolean isChecked() {
        return isChecked;
    }

    public boolean isProcessingCkeckState() {
        return isChecking || isUnchecking;
    }

    public Uri getThumbnailUri() {
        return mThumbnailUri;
    }

    public Uri getOriginalUri() {
        return mOriginalUri;
    }

    public int getOrientation() {
        return mOrientation;
    }

    public static class Builder {
        private Context mContext;
        private Uri mThumbnailUri;
        private Uri mOriginalUri;
        private int mWidth;
        private int mHeight;
        private int mOrientation;

        public Builder(@NonNull Context context, @NonNull Uri thumbnailUri, @NonNull Uri originalUri) {
            this.mContext = context;
            this.mThumbnailUri= thumbnailUri;
            this.mOriginalUri = originalUri;
        }

        public Builder setWidth(int width) {
            this.mWidth = width;
            return this;
        }

        public Builder setHeight(int height) {
            this.mHeight = height;
            return this;
        }

        public Builder setOrientation(int orientation) {
            this.mOrientation = orientation;
            return this;
        }

        public PhotoItem build() {
            return new PhotoItem(this);
        }
    }
}
