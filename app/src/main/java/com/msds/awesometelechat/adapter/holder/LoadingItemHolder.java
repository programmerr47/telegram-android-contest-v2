package com.msds.awesometelechat.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

/**
 * @author Michael Spitsin
 * @since 2015-05-19
 */
public class LoadingItemHolder extends RecyclerView.ViewHolder {

    private ProgressBar mProgress;

    public LoadingItemHolder(View itemView, int progressId) {
        super(itemView);

        mProgress = (ProgressBar) itemView.findViewById(progressId);
    }

    public ProgressBar getProgress() {
        return mProgress;
    }
}
