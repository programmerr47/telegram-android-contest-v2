package com.msds.awesometelechat.adapter.item.chat.message.abstractions;

import android.net.Uri;

import com.msds.awesometelechat.adapter.item.AdapterItem;

/**
 * Markering interface that tells that some {@link AdapterItem}
 * has posibility to keep or present some image. So this interface
 * force to implement get and set methods for {@link Uri} of
 * image.
 *
 * @author Michael Spitsin
 * @since 2015-06-27
 */
public interface ImageableItem extends AdapterItem {
    Uri getUri();
    void setUri(Uri uri);
}
