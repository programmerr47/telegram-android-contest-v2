package com.msds.awesometelechat.adapter.item.drawer;

/**
 * All drawer identificators.
 *
 * @author Michael Spitsin
 * @since 2014-10-09
 */
public enum DrawerElementName {
    HEADER,
    LOG_OUT
}
