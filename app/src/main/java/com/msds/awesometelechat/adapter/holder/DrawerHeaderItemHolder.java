package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.TextView;

/**
 * @author Michael Spitsin
 * @since 2015-05-08
 */
public final class DrawerHeaderItemHolder extends AvatarItemHolder {

    private TextView mUsername;
    private TextView mPhoneNumber;

    public DrawerHeaderItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mUsername = (TextView) itemView.findViewById(params.usernameLayoutId);
        mPhoneNumber = (TextView) itemView.findViewById(params.phoneNumberLayoutId);
    }

    public TextView getUsername() {
        return mUsername;
    }

    public TextView getPhoneNumber() {
        return mPhoneNumber;
    }

    public static final class ResourceParams extends AvatarItemHolder.ResourceParams {
        public int usernameLayoutId;
        public int phoneNumberLayoutId;
    }
}
