package com.msds.awesometelechat.adapter.item.chat.chatlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.LoadingItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;

/**
 * @author Michael Spitsin
 * @since 2015-05-19
 */
public class ChatListLoadingItem extends ChatListItem {

    public ChatListLoadingItem(@NonNull Context context) {
        super(context, null, false);
        isDeactivated = true;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        //Empty because it just needed to show progress or it will be deleted from list
        // so nothing to bind here, but we need to prevent binding from parent
        // class, so we just override this method, but nothing more.
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.loading_item, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new LoadingItemHolder(view, R.id.progress);
            }
        };
    }
}
