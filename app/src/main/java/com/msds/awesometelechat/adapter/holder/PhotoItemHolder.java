package com.msds.awesometelechat.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.msds.awesometelechat.view.XFractionalFrameLayout;

/**
 * @author Michael Spitsin
 * @since 2015-05-16
 */
public class PhotoItemHolder extends RecyclerView.ViewHolder {

    private ImageView mImageContainer;
    private XFractionalFrameLayout mCheckboxImage;
    private View mDarkCoverView;

    public PhotoItemHolder(View itemView, ResourceParams params) {
        super(itemView);

        mImageContainer = (ImageView) itemView.findViewById(params.imageContainerId);
        mCheckboxImage = (XFractionalFrameLayout) itemView.findViewById(params.checkImageboxId);
        mDarkCoverView = itemView.findViewById(params.darkCoverViewId);
    }

    public ImageView getImageContainer() {
        return mImageContainer;
    }

    public XFractionalFrameLayout getCheckboxImage() {
        return mCheckboxImage;
    }

    public View getDarkCoverView() {
        return mDarkCoverView;
    }

    public static final class ResourceParams {
        public int imageContainerId;
        public int checkImageboxId;
        public int darkCoverViewId;
    }
}
