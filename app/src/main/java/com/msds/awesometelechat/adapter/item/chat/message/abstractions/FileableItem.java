package com.msds.awesometelechat.adapter.item.chat.message.abstractions;

import android.net.Uri;

import com.msds.awesometelechat.adapter.item.AdapterItem;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-07-12
 */
public interface FileableItem extends AdapterItem {
    int getFileId();
    String getFilePath();
    void setFilePath(String filePath);
    int getFileSize();
    void setFileSize(int fileSize);
    int getFileDownloadedSize();
    void setFileDownloadedSize(int fileDownloadedSize);
    int getThumbId();
    Uri getThumbUri();
    void setThumbUri(Uri uri);
}
