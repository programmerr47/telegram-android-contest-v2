package com.msds.awesometelechat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.msds.awesometelechat.callback.ChatListItemNotifier;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;

import java.util.List;

/**
 * //todo descr
 * @author Michael Spitsin
 * @since 2015-04-28
 */
//todo fill
public class ChatListAdapter extends AbstractMultiTypeRecyclerAdapter<ChatListItem> implements ChatListItemNotifier {

    private View.OnClickListener mListener;

    public ChatListAdapter(@NonNull RecyclerItems<ChatListItem> items) {
        super(items);
    }

    public ChatListAdapter(@NonNull List<ChatListItem> items) {
        super(items);
    }

    public ChatListAdapter(@NonNull RecyclerItems<ChatListItem> items, View.OnClickListener listener) {
        super(items);
        this.mListener = listener;
    }

    public ChatListAdapter(@NonNull List<ChatListItem> items, View.OnClickListener listener) {
        super(items);
        this.mListener = listener;
    }

    public void setChatListListener(View.OnClickListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPostBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (mListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(holder.itemView);
                }
            });
        }
    }

    @Override
    public void notifyElementChanged(ChatListItem item) {
        int position = mItems.indexOf(item);
        if (position != -1) {
            notifyItemChanged(position);
        }
    }
}
