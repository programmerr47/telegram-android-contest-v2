package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.adapter.holder.ChatItemHolder;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.util.ChatAvatarUtil;
import com.msds.awesometelechat.util.LocaleUtils;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public abstract class ChatItem implements AdapterItem {

    public final ApiInfo systemInfo;
    protected Context mContext;

    private String mUsername = "";
    private Date mTimestamp;
    private String mFormattedTimestamp;

    private boolean isNew;
    private boolean isSending;

    private String mAbbreviation;
    private int mAvatarbackgroundColor;
    private Drawable mAvatarBackgroundColorDrawable;
    private Uri mAvatarUri;

    protected ChatItem(@NonNull Context context, int messageId, int userId, long chatId) {
        this.mContext = context;
        this.systemInfo = new ApiInfo(messageId, userId, chatId);

        this.mAbbreviation = ChatAvatarUtil.getAbbreviationFromString(mUsername);
        this.mAvatarbackgroundColor = ChatAvatarUtil.getBackgroundAvatarColor(systemInfo.userId);
        this.mAvatarBackgroundColorDrawable = new ColorDrawable(mAvatarbackgroundColor);
    }

    public Date getTimestamp() {
        return mTimestamp;
    }

    public String getUsername() {
        return mUsername;
    }

    public boolean isNew() {
        return isNew;
    }

    //TODO uncomment this, when will fix broken logic
    public void setNew(boolean isNew) {
//        this.isNew = isNew;
    }

    //TODO uncomment this, when will fix broken logic
    public void setSending(boolean isSending) {
//        this.isSending = isSending;
    }

    public void setAvatar(Uri avatarUri) {
        this.mAvatarUri = avatarUri;
    }

    //todo remove нахуй
    public void setDate(Date date) {
        this.mTimestamp = date;
        this.mFormattedTimestamp = LocaleUtils.formatterDay.format(mTimestamp);
    }

    public void setUsername(String username) {
        this.mUsername = username;
        this.mAbbreviation = ChatAvatarUtil.getAbbreviationFromString(mUsername);
        onUsernameIsSet();
    }

    public void setTimestamp(Date timestamp) {
        this.mTimestamp = timestamp;
        this.mFormattedTimestamp = LocaleUtils.formatterDay.format(mTimestamp);
    }

    protected void onUsernameIsSet() {
    }

    protected void bindBaseView(ChatItemHolder holder, int position) {
        //setting avatar
        if (mAvatarUri != null) {
//            holder.getAvatarAbbreviation().setVisibility(View.INVISIBLE);
//            holder.getBackgroundAvatar().setVisibility(View.INVISIBLE);
            holder.getAvatarAbbreviation().setText(mAbbreviation);
            holder.getBackgroundAvatar().setImageDrawable(mAvatarBackgroundColorDrawable);

            holder.getAvatar().setVisibility(View.VISIBLE);

            //Todo compute avatar size (width and height) and pass ass params
            AwesomeApplication.getImageWorker().loadImage(
                    mAvatarUri,
                    holder.getAvatar(),
                    new UriImageWorker.LoadingImageParams()
                            .setCircle(true));
        } else {
            holder.getAvatar().setVisibility(View.INVISIBLE);
            holder.getAvatarAbbreviation().setVisibility(View.VISIBLE);
            holder.getBackgroundAvatar().setVisibility(View.VISIBLE);

            holder.getAvatarAbbreviation().setText(mAbbreviation);
            holder.getBackgroundAvatar().setImageDrawable(mAvatarBackgroundColorDrawable);
        }

        //setting marks
        if (isNew) {
            holder.getNewMark().setVisibility(View.VISIBLE);
        } else {
            holder.getNewMark().setVisibility(View.INVISIBLE);
        }

        if (isSending) {
            holder.getSendingMark().setVisibility(View.VISIBLE);
        } else {
            holder.getSendingMark().setVisibility(View.INVISIBLE);
        }

        //setting timestamp
        if (mTimestamp != null) {
            holder.getTimestamp().setVisibility(View.VISIBLE);
            holder.getTimestamp().setText(mFormattedTimestamp);
        } else {
            holder.getTimestamp().setVisibility(View.INVISIBLE);
        }

        //setting username
        if (mUsername != null) {
            holder.getUsername().setVisibility(View.VISIBLE);
            holder.getUsername().setText(mUsername);
        } else {
            holder.getUsername().setVisibility(View.INVISIBLE);
        }
    }

    //todo add forward info
    public static final class ApiInfo {
        private long chatId;
        private int messageId;
        private int userId;

        private ApiInfo(int messageId, int userId, long chatId) {
            this.messageId = messageId;
            this.userId = userId;
            this.chatId = chatId;
        }

        public long getChatId(){
            return chatId;
        }

        public int getMessageId() {
            return messageId;
        }

        public int getUserId() {
            return userId;
        }
    }
}
