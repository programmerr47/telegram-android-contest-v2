package com.msds.awesometelechat.adapter.item.chat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.SingleTextItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.adapter.item.SingleTextItem;
import com.msds.awesometelechat.util.LocaleUtils;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
public class DateItem implements AdapterItem {

    private final Context mContext;
    private final Date mDate;
    private String mFormattedDate;

    public DateItem(@NonNull Context context, long timeInMillis) {
        this.mContext = context;
        this.mDate = new Date(timeInMillis);
        this.mFormattedDate = LocaleUtils.formatDateForChatDateSeparator(mDate);
    }

    public DateItem(@NonNull Context context, Date date) {
        this.mContext = context;
        this.mDate = new Date(date.getTime());
        this.mFormattedDate = LocaleUtils.formatDateForChatDateSeparator(mDate);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        SingleTextItemHolder holder = (SingleTextItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_date, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new SingleTextItemHolder(view, R.id.date);
            }
        };
    }

    private void bindView(SingleTextItemHolder viewHolder, int position) {
        viewHolder.getTextView().setText(mFormattedDate);
    }
}
