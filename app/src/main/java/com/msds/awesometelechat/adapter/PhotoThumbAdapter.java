package com.msds.awesometelechat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.photo.PhotoItem;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-05-17
 */
public class PhotoThumbAdapter extends AbstractMultiTypeRecyclerAdapter<PhotoItem> {

    private Set<PhotoItem> mCheckedItems;
    private OnItemCheckStateListener mListener;

    public PhotoThumbAdapter(@NonNull RecyclerItems<PhotoItem> items) {
        super(items);
        mCheckedItems = new LinkedHashSet<>();
    }

    public PhotoThumbAdapter(@NonNull List<PhotoItem> photoItems) {
        super(photoItems);
        mCheckedItems = new LinkedHashSet<>();
    }

    public void setOnItemCheckStateListener(OnItemCheckStateListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPostBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (mListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PhotoItem item = getItem(position);

                    if (!item.isProcessingCkeckState()) {
                        boolean prev = item.isChecked();
                        int prevCheckItemSize = mCheckedItems.size();

                        item.setChecked(!prev);
                        notifyItemChanged(position);

                        if (prev) {
                            mCheckedItems.remove(item);
                        } else {
                            mCheckedItems.add(item);
                        }

                        mListener.onCheckStateChange(prevCheckItemSize, mCheckedItems.size());
                    }
                }
            });
        }
    }

    public int getCheckedItemCount() {
        return mCheckedItems.size();
    }

    public Set<PhotoItem> getCkeckedItems() {
        return mCheckedItems;
    }

    public void uncheckAll() {
        for (PhotoItem item : mCheckedItems) {
            item.setChecked(false);
        }
        mCheckedItems.clear();
        notifyDataSetChanged();
    }

    public static interface OnItemCheckStateListener {
        void onCheckStateChange(int oldCheckedItemCount, int newCheckedItemCount);
    }
}
