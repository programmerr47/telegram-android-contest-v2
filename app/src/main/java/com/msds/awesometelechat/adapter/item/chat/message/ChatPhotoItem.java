package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatPhotoItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.ImageableItem;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.callback.ChatItemNotifier;
import com.msds.awesometelechat.util.FileUtil;
import com.msds.awesometelechat.util.PhotoUtil;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatPhotoItem extends ChatItem implements ImageableItem, FileableItem {

    private Uri mPhotoUri;
    private int mPhotoWidth;
    private int mPhotoHeight;
    private int mOrientation;

    private int mFileId;
    private int mFileSize;
    private int mFileDownloaded;

    private int mThumbId;
    private Uri mThumbUri;

    private LinearLayout.LayoutParams mRepresentationParams;
    private Drawable mPhotoBackgroundDrawable = new ColorDrawable(0xff888888);
    private int mProgress;

    public static ChatPhotoItem createFromMessage(@NonNull Context context, TdApi.Message message, ChatItemNotifier notifier) {
        TdApi.MessagePhoto content = (TdApi.MessagePhoto) message.message;
        ChatPhotoItem result = new ChatPhotoItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));

        TdApi.PhotoSize photo = PhotoUtil.getMinPhotoSizeForMessage(content.photo);
        if (photo != null){
            result.mFileId = FileUtil.getFileId(photo.photo);
            result.mThumbId = FileUtil.getFileId(content.photo.photos[0].photo);
            result.mPhotoWidth = photo.width;
            result.mPhotoHeight = photo.height;
            result.mFileSize = ChatCreator.getSizeOfFile(photo.photo);

            if (notifier != null) {
                notifier.notifyThumbItemCreating(result);
                notifier.notifyFileItemCreating(result);
                ChatCreator.downloadThumb(result, content.photo.photos[0].photo);
                ChatCreator.downloadFile(result, photo.photo);
            }

            Pair<Integer, Integer> dimensionsForMessageContainer = PhotoUtil.getMessagePhotoDimensions(photo);
            result.mRepresentationParams = new LinearLayout.LayoutParams(dimensionsForMessageContainer.first, dimensionsForMessageContainer.second);
        }

        return result;
    }

    protected ChatPhotoItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        ChatPhotoItemHolder holder = (ChatPhotoItemHolder) viewHolder;
        bindBaseView(holder, position);
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_message_photo, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                ChatPhotoItemHolder.ResourceParams params = new ChatPhotoItemHolder.ResourceParams();
                params.avatarLayoutId = R.id.avatar;
                params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
                params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
                params.photoContainerId = R.id.photoContainer;
                params.photoLayoutId = R.id.photo;
                params.downloadingProgress = R.id.downloadingProgress;
                params.progressLabel = R.id.progressLabel;
                params.newMarkId = R.id.new_mark;
                params.sendingMarkId = R.id.sendingMark;
                params.timestampId = R.id.timestamp;
                params.usernameId = R.id.username;

                return new ChatPhotoItemHolder(view, params);
            }
        };
    }

    @Override
    public void setUri(Uri photoUri) {
        this.mPhotoUri = photoUri;
    }

    @Override
    public Uri getUri() {
        return mPhotoUri;
    }

    public void setOrientation(int orientation) {
        mOrientation = orientation;

        if (orientation == 90 || orientation == 270) {
            setDimensions(new Pair<>(mPhotoHeight, mPhotoWidth));
        }
    }

    public void setDimensions(Pair<Integer, Integer> dimensions) {
        mPhotoWidth = dimensions.first;
        mPhotoHeight = dimensions.second;
        Pair<Integer, Integer> dimensionsForMessageContainer = PhotoUtil.getMessagePhotoDimensions(dimensions);
        mRepresentationParams = new LinearLayout.LayoutParams(dimensionsForMessageContainer.first, dimensionsForMessageContainer.second);
    }

    private void bindView(ChatPhotoItemHolder holder, int position) {
        holder.getPhotoContainer().setLayoutParams(mRepresentationParams);

        //setting photo
        if (mPhotoUri != null) {
            holder.getDownloadingProgress().setVisibility(View.GONE);
            holder.getProgressLabel().setVisibility(View.GONE);
            AwesomeApplication.getImageWorker().loadImage(
                    mPhotoUri,
                    holder.getPhoto(),
                    new UriImageWorker.LoadingImageParams()
                            .setReqWidth(mRepresentationParams.width)
                            .setReqHeight(mRepresentationParams.height)
                            .setActualOrientation(mOrientation));
        } else {
            holder.getDownloadingProgress().setVisibility(View.VISIBLE);

            if (mThumbUri != null) {
                AwesomeApplication.getImageWorker().loadImage(
                        mThumbUri,
                        holder.getPhoto(),
                        new UriImageWorker.LoadingImageParams()
                                .setReqWidth(mRepresentationParams.width)
                                .setReqHeight(mRepresentationParams.height)
                                .setActualOrientation(mOrientation));
            } else {
                holder.getPhoto().setImageDrawable(mPhotoBackgroundDrawable);
            }

            //setting progress
            if (mProgress == -1) {
                holder.getProgressLabel().setVisibility(View.GONE);

                holder.getDownloadingProgress().setIndeterminate(true);
            } else {
                holder.getProgressLabel().setVisibility(View.VISIBLE);
                holder.getProgressLabel().setText(mProgress + "%");

                holder.getDownloadingProgress().setIndeterminate(false);
                holder.getDownloadingProgress().setProgress(mProgress);
            }
        }
    }

    @Override
    public int getFileId() {
        return mFileId;
    }

    @Override
    public String getFilePath() {
        return mPhotoUri != null ? mPhotoUri.getPath() : null;
    }

    @Override
    public void setFilePath(String filePath) {
        mPhotoUri = filePath != null ? Uri.parse(filePath) : null;
    }

    @Override
    public int getFileSize() {
        return mFileSize;
    }

    @Override
    public void setFileSize(int fileSize) {
        if (mFileSize != fileSize) {
            mFileSize = fileSize;
            mFileDownloaded = 0;
            mProgress = 0;
        }

        if (mFileSize == 0) {
            mProgress = -1;
        }
    }

    @Override
    public int getFileDownloadedSize() {
        return mFileDownloaded;
    }

    @Override
    public void setFileDownloadedSize(int fileDownloadedSize) {
        if (mFileDownloaded != fileDownloadedSize) {
            mFileDownloaded = fileDownloadedSize;

            if (mFileSize != 0) {
                mProgress = (int)(mFileDownloaded * 1.0 / mFileSize * 100);
            } else {
                mProgress = -1;
            }
        }
    }

    @Override
    public int getThumbId() {
        return mThumbId;
    }

    @Override
    public Uri getThumbUri() {
        return mThumbUri;
    }

    @Override
    public void setThumbUri(Uri thumbUri) {
        mThumbUri = thumbUri;
    }
}
