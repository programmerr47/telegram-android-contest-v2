package com.msds.awesometelechat.adapter.item.section;

import com.msds.awesometelechat.adapter.item.AdapterItem;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
//todo descr
public interface SectionItem extends AdapterItem {
    int getSectionPositionInList();
}
