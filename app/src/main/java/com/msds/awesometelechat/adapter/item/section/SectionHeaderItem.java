package com.msds.awesometelechat.adapter.item.section;

import android.content.Context;
import android.support.annotation.NonNull;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.item.SingleTextItem;

/**
 * @author Michael Spitsin
 * @since 2014-05-04
 */
public class SectionHeaderItem extends SingleTextItem implements SectionItem {

    private final int mSectionPosition;

    public SectionHeaderItem(@NonNull Context context, String text, int sectionPosition) {
        super(context, text);
        this.mSectionPosition = sectionPosition;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.sticky_header_item;
    }

    @Override
    protected int getTextViewId() {
        return R.id.text;
    }

    @Override
    public int getSectionPositionInList() {
        return mSectionPosition;
    }
}
