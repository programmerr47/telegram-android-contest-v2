package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatFileItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;
import com.msds.awesometelechat.callback.ChatItemNotifier;
import com.msds.awesometelechat.util.FileUtil;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatFileItem extends ChatItem implements FileableItem {

    public enum FileStatus {
        NOT_DOWNLOADED(R.drawable.ic_download_blue),
        DOWNLOADING(R.drawable.ic_pause_blue),
        DOWNLOADED(R.drawable.ic_file);

        private int iconId;

        FileStatus(int statusIconResId) {
            this.iconId = statusIconResId;
        }

        public int getStatusIconResId() {
            return iconId;
        }
    }

    private String mFilename;
    private String mFilePath;
    private String mFileMimeType;

    private int mFileId;
    private int mFileSize;
    private int mFileDownloaded;

    private String mFileSizeString;
    private String mFileDownloadedString;

    private int mProgress;

    private int mThumbId;
    private Uri mThumbUri;

    private FileStatus mFileStatus;

    private ChatItemNotifier mNotifier;

    public static ChatFileItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message, ChatItemNotifier notifier) {
        TdApi.MessageDocument content = (TdApi.MessageDocument) message.message;
        ChatFileItem result = new ChatFileItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.mFilename = content.document.fileName;
        result.mFileSize = ChatCreator.getSizeOfFile(content.document.document);
        result.mFileSizeString = Util.getAppropriateFileSizeString(result.mFileSize);
        result.mFileId = FileUtil.getFileId(content.document.document);
        result.mThumbId = FileUtil.getFileId(content.document.thumb.photo);
        result.mFileMimeType = content.document.mimeType;
        result.mNotifier = notifier;

        if (notifier != null) {
            notifier.notifyThumbItemCreating(result);
            notifier.notifyFileItemCreating(result);
            ChatCreator.downloadThumb(result, content.document.thumb.photo);
        }

        if (content.document.document instanceof TdApi.FileLocal) {
            result.mFileStatus = FileStatus.DOWNLOADED;
            result.mFilePath = ((TdApi.FileLocal) content.document.document).path;
        } else {
            result.mFileStatus = FileStatus.NOT_DOWNLOADED;
        }

        return result;
    }

    protected ChatFileItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        ChatFileItemHolder holder = (ChatFileItemHolder) viewHolder;
        bindBaseView(holder, position);
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_message_file, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                ChatFileItemHolder.ResourceParams params = new ChatFileItemHolder.ResourceParams();
                params.avatarLayoutId = R.id.avatar;
                params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
                params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
                params.newMarkId = R.id.new_mark;
                params.sendingMarkId = R.id.sendingMark;
                params.timestampId = R.id.timestamp;
                params.usernameId = R.id.username;
                params.downloadCancelOpenId = R.id.downloadCancelOpen;
                params.filenameId = R.id.filename;
                params.fileInfoId = R.id.fileInfo;
                params.fileStatusImageId = R.id.statusImage;
                params.progressId = R.id.downloadingProgress;
                params.thumbId = R.id.thumb;

                return new ChatFileItemHolder(view, params);
            }
        };
    }

    private void bindView(ChatFileItemHolder holder, int position) {
        //settings thumb
        if (mThumbUri != null) {
            holder.getThumbPhoto().setVisibility(View.VISIBLE);
            AwesomeApplication.getImageWorker().loadImage(mThumbUri, holder.getThumbPhoto());
        } else {
            holder.getThumbPhoto().setVisibility(View.GONE);
        }

        //setting filename
        if (mFilename != null) {
            holder.getFilename().setVisibility(View.VISIBLE);
            holder.getFilename().setText(mFilename);
        } else {
            holder.getFilename().setVisibility(View.INVISIBLE);
        }

        //setting file info
        if (mFileStatus == FileStatus.NOT_DOWNLOADED || mFileStatus == FileStatus.DOWNLOADED) {
            if (mFileSizeString != null) {
                holder.getFileInfo().setVisibility(View.VISIBLE);
                holder.getFileInfo().setText(mFileSizeString);
            } else {
                holder.getFileInfo().setVisibility(View.GONE);
            }
        } else if (mFileStatus == FileStatus.DOWNLOADING) {
            holder.getFileInfo().setVisibility(View.VISIBLE);
            if (mFileSizeString != null) {
                if (mFileDownloadedString != null) {
                    holder.getFileInfo().setText(String.format(mContext.getString(R.string.DOWNLOADED_FILE_PART_INFO), mFileDownloadedString, mFileSizeString));
                } else {
                    holder.getFileInfo().setText(String.format(mContext.getString(R.string.DOWNLOADING_FILE_DETERMINATE_SIZE), mFileSizeString));
                }
            } else {
                holder.getFileInfo().setText(R.string.DOWNLOADING_FILE_UNDETERMINATE_SIZE);
            }
        } else {
            holder.getFileInfo().setVisibility(View.GONE);
        }

        //setting file status
        holder.getFileStatusImage().setImageResource(mFileStatus.getStatusIconResId());

        //setting progress
        if (mFileStatus == FileStatus.DOWNLOADING) {
            holder.getProgress().setVisibility(View.VISIBLE);
            holder.getProgress().setProgressWithAnimation(mProgress);
        } else {
            holder.getProgress().setVisibility(View.GONE);
        }

        //setting on click listeners if necessary
        if (mFileStatus == FileStatus.DOWNLOADING) {
            holder.getDownloadCancelOpenView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFileStatus = FileStatus.NOT_DOWNLOADED;
                    ChatCreator.cancelDownloadFile(mFileId);

                    if (mNotifier != null) {
                        mNotifier.notifyItemChanged(ChatFileItem.this);
                    }
                }
            });
        } else if (mFileStatus == FileStatus.NOT_DOWNLOADED) {
            holder.getDownloadCancelOpenView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFileStatus = FileStatus.DOWNLOADING;
                    ChatCreator.downloadFile(mFileId);

                    if (mNotifier != null) {
                        mNotifier.notifyItemChanged(ChatFileItem.this);
                    }
                }
            });
        } else if (mFileStatus == FileStatus.DOWNLOADED) {
            holder.getDownloadCancelOpenView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(mFilePath);
                    Uri fileUri = FileProvider.getUriForFile(AwesomeApplication.getAppContext(), "com.msds.awesometelechat", file);
                    Intent viewFileIntent = new Intent();
                    viewFileIntent.setAction(Intent.ACTION_VIEW);
                    viewFileIntent.setDataAndType(fileUri, mFileMimeType);
                    viewFileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewFileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    try {
                        AwesomeApplication.getAppContext().startActivity(viewFileIntent);
                    } catch (ActivityNotFoundException ex) {
                        Toast.makeText(AwesomeApplication.getAppContext(), R.string.OPENING_APP_NOT_FOUND, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public int getFileId() {
        return mFileId;
    }

    @Override
    public String getFilePath() {
        return mFilePath;
    }

    @Override
    public void setFilePath(String filePath) {
        this.mFilePath = filePath;
        this.mFileStatus = FileStatus.DOWNLOADED;
    }

    @Override
    public int getFileSize() {
        return mFileSize;
    }

    @Override
    public void setFileSize(int fileSize) {
        if (mFileSize != fileSize) {
            mFileSize = fileSize;
            mFileDownloaded = 0;
            mProgress = 0;
        }

        if (mFileSize == 0) {
            mProgress = -1;
        }

        mFileSizeString = Util.getAppropriateFileSizeString(mFileSize);
    }

    @Override
    public int getFileDownloadedSize() {
        return mFileDownloaded;
    }

    @Override
    public void setFileDownloadedSize(int fileDownloadedSize) {
        if (mFileDownloaded != fileDownloadedSize) {
            mFileDownloaded = fileDownloadedSize;

            if (mFileSize != 0) {
                mProgress = (int)(mFileDownloaded * 1.0 / mFileSize * 100);
            } else {
                mProgress = -1;
            }
        }

        mFileDownloadedString = Util.getAppropriateFileSizeString(mFileDownloaded);
        mFileStatus = FileStatus.DOWNLOADING;
    }

    @Override
    public int getThumbId() {
        return mThumbId;
    }

    @Override
    public Uri getThumbUri() {
        return mThumbUri;
    }

    @Override
    public void setThumbUri(Uri uri) {
        this.mThumbUri = uri;
    }
}
