package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatDefaultItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-11
 */
public final class ChatDefaultItem extends ChatItem {

    private String mText;

    public static ChatDefaultItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message) {
        TdApi.MessageText content = (TdApi.MessageText) message.message;
        ChatDefaultItem result = new ChatDefaultItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.mText = content.text != null ? content.text : context.getString(R.string.SYSTEM_TEXT_EMPTY_MESSAGE_TEXT);
        return result;
    }

    protected ChatDefaultItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    public void setText(String text) {
        this.mText = text;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        ChatDefaultItemHolder holder = (ChatDefaultItemHolder) viewHolder;
        bindBaseView(holder, position);
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_message_default, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                ChatDefaultItemHolder.ResourceParams params = new ChatDefaultItemHolder.ResourceParams();
                params.avatarLayoutId = R.id.avatar;
                params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
                params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
                params.messageId = R.id.message;
                params.newMarkId = R.id.new_mark;
                params.sendingMarkId = R.id.sendingMark;
                params.timestampId = R.id.timestamp;
                params.usernameId = R.id.username;

                return new ChatDefaultItemHolder(view, params);
            }
        };
    }

    protected void bindView(ChatDefaultItemHolder holder, int position) {
        holder.getMessage().setText(mText);
    }
}
