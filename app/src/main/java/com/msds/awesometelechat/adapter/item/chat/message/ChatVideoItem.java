package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.ChatVideoItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.ImageableItem;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.callback.ChatItemNotifier;
import com.msds.awesometelechat.util.FileUtil;
import com.msds.awesometelechat.util.PhotoUtil;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatVideoItem extends ChatItem implements FileableItem {

    public enum FileStatus {
        NOT_DOWNLOADED(R.drawable.ic_download),
        DOWNLOADING(R.drawable.ic_pause),
        DOWNLOADED(R.drawable.ic_play);

        private int iconId;

        FileStatus(int statusIconResId) {
            this.iconId = statusIconResId;
        }

        public int getStatusIconResId() {
            return iconId;
        }
    }

    private int mHeight;
    private int mWidth;

    private String mFilePath;

    private int mFileId;
    private int mFileSize;
    private int mFileDownloaded;

    private int mThumbId;
    private Uri mThumbUri;

    private FileStatus mFileStatus;

    private ChatItemNotifier mNotifier;

    private Drawable mVideoThumbBackgroundDrawable = new ColorDrawable(0xff888888);
    private LinearLayout.LayoutParams mRepresentationParams;
    private int mProgress;

    public static ChatVideoItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message, ChatItemNotifier notifier) {
        TdApi.MessageVideo content = (TdApi.MessageVideo) message.message;
        ChatVideoItem result = new ChatVideoItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.mHeight = content.video.height;
        result.mWidth = content.video.width;
        result.mFileSize = ChatCreator.getSizeOfFile(content.video.video);
        result.mFileId = FileUtil.getFileId(content.video.video);
        result.mThumbId = FileUtil.getFileId(content.video.thumb.photo);
        result.mNotifier = notifier;

        if (notifier != null) {
            notifier.notifyThumbItemCreating(result);
            notifier.notifyFileItemCreating(result);
            ChatCreator.downloadThumb(result, content.video.thumb.photo);
        }

        if (content.video.video instanceof TdApi.FileLocal) {
            result.mFileStatus = FileStatus.DOWNLOADED;
            result.mFilePath = ((TdApi.FileLocal) content.video.video).path;
        } else {
            result.mFileStatus = FileStatus.NOT_DOWNLOADED;
        }

        Pair<Integer, Integer> dimensionsForMessageContainer = PhotoUtil.getMessagePhotoDimensions(content.video.thumb);
        result.mRepresentationParams = new LinearLayout.LayoutParams(dimensionsForMessageContainer.first, dimensionsForMessageContainer.second);

        return result;
    }

    protected ChatVideoItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        ChatVideoItemHolder holder = (ChatVideoItemHolder) viewHolder;
        bindBaseView(holder, position);
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_message_video, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                ChatVideoItemHolder.ResourceParams params = new ChatVideoItemHolder.ResourceParams();
                params.avatarLayoutId = R.id.avatar;
                params.backgroundAvatarLayoutId = R.id.backgroundAvatar;
                params.avatarAbbreviationId = R.id.backgroundAvatarAbbreviation;
                params.thumbContainerId = R.id.thumbContainer;
                params.thumbViewId = R.id.thumb;
                params.newMarkId = R.id.new_mark;
                params.sendingMarkId = R.id.sendingMark;
                params.timestampId = R.id.timestamp;
                params.usernameId = R.id.username;
                params.progressId = R.id.downloadingProgress;
                params.fileStatusImageId = R.id.statusImage;

                return new ChatVideoItemHolder(view, params);
            }
        };
    }

    private void bindView(ChatVideoItemHolder holder, int position) {
        //setting video thumb width/height
        holder.getThumbContainer().setLayoutParams(mRepresentationParams);

        //setting video thumb
        if (mThumbUri != null) {
            AwesomeApplication.getImageWorker().loadImage(
                    mThumbUri,
                    holder.getThumb(),
                    new UriImageWorker.LoadingImageParams()
                            .setReqWidth(mRepresentationParams.width)
                            .setReqHeight(mRepresentationParams.height));
        } else {
            holder.getThumb().setImageDrawable(mVideoThumbBackgroundDrawable);
        }

        //setting video status
        holder.getFileStatusImage().setImageResource(mFileStatus.getStatusIconResId());
        if (mFileStatus == FileStatus.DOWNLOADED) {
            int paddingLeft = (int)AwesomeApplication.getAppContext().getResources().getDimension(R.dimen.padding_medium);
            holder.getFileStatusImage().setPadding(paddingLeft, 0, 0, 0);
        }

        //setting progress
        if (mFileStatus == FileStatus.DOWNLOADING) {
            holder.getProgress().setVisibility(View.VISIBLE);
            holder.getProgress().setProgressWithAnimation(mProgress);
        } else {
            holder.getProgress().setVisibility(View.GONE);
        }

        //setting on click listeners if necessary
        if (mFileStatus == FileStatus.DOWNLOADING) {
            holder.getThumbContainer().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFileStatus = FileStatus.NOT_DOWNLOADED;
                    ChatCreator.cancelDownloadFile(mFileId);

                    if (mNotifier != null) {
                        mNotifier.notifyItemChanged(ChatVideoItem.this);
                    }
                }
            });
        } else if (mFileStatus == FileStatus.NOT_DOWNLOADED) {
            holder.getThumbContainer().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFileStatus = FileStatus.DOWNLOADING;
                    ChatCreator.downloadFile(mFileId);

                    if (mNotifier != null) {
                        mNotifier.notifyItemChanged(ChatVideoItem.this);
                    }
                }
            });
        } else if (mFileStatus == FileStatus.DOWNLOADED) {
            holder.getThumbContainer().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(mFilePath);
                    Uri fileUri = FileProvider.getUriForFile(AwesomeApplication.getAppContext(), "com.msds.awesometelechat", file);
                    Intent viewFileIntent = new Intent();
                    viewFileIntent.setAction(Intent.ACTION_VIEW);
                    viewFileIntent.setDataAndType(fileUri, "video/*");
                    viewFileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    viewFileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    try {
                        AwesomeApplication.getAppContext().startActivity(viewFileIntent);
                    } catch (ActivityNotFoundException ex) {
                        Toast.makeText(AwesomeApplication.getAppContext(), R.string.OPENING_APP_NOT_FOUND, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public int getFileId() {
        return mFileId;
    }

    @Override
    public String getFilePath() {
        return mFilePath;
    }

    @Override
    public void setFilePath(String filePath) {
        this.mFilePath = filePath;
        this.mFileStatus = FileStatus.DOWNLOADED;
    }

    @Override
    public int getFileSize() {
        return mFileSize;
    }

    @Override
    public void setFileSize(int fileSize) {
        if (mFileSize != fileSize) {
            mFileSize = fileSize;
            mFileDownloaded = 0;
            mProgress = 0;
        }

        if (mFileSize == 0) {
            mProgress = -1;
        }
    }

    @Override
    public int getFileDownloadedSize() {
        return mFileDownloaded;
    }

    @Override
    public void setFileDownloadedSize(int fileDownloadedSize) {
        if (mFileDownloaded != fileDownloadedSize) {
            mFileDownloaded = fileDownloadedSize;

            if (mFileSize != 0) {
                mProgress = (int)(mFileDownloaded * 1.0 / mFileSize * 100);
            } else {
                mProgress = -1;
            }
        }

        mFileStatus = FileStatus.DOWNLOADING;
    }

    @Override
    public int getThumbId() {
        return mThumbId;
    }

    @Override
    public Uri getThumbUri() {
        return mThumbUri;
    }

    @Override
    public void setThumbUri(Uri uri) {
        this.mThumbUri = uri;
    }
}
