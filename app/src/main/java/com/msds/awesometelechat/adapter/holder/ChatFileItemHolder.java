package com.msds.awesometelechat.adapter.holder;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.msds.awesometelechat.view.CircularProgressBar;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatFileItemHolder extends ChatItemHolder {

    private View mDownloadCancelOpenView;
    private CircularProgressBar mProgress;
    private ImageView mFileStatusImage;
    private TextView mFilename;
    private TextView mFileInfo;
    private ImageView mThumbPhoto;

    public ChatFileItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mDownloadCancelOpenView = itemView.findViewById(params.downloadCancelOpenId);
        mProgress = (CircularProgressBar) itemView.findViewById(params.progressId);
        mFileStatusImage = (ImageView) itemView.findViewById(params.fileStatusImageId);
        mFilename = (TextView) itemView.findViewById(params.filenameId);
        mFileInfo = (TextView) itemView.findViewById(params.fileInfoId);
        mThumbPhoto = (ImageView) itemView.findViewById(params.thumbId);
    }

    public View getDownloadCancelOpenView() {
        return mDownloadCancelOpenView;
    }

    public CircularProgressBar getProgress() {
        return mProgress;
    }

    public ImageView getFileStatusImage() {
        return mFileStatusImage;
    }

    public TextView getFilename() {
        return mFilename;
    }

    public TextView getFileInfo() {
        return mFileInfo;
    }

    public ImageView getThumbPhoto() {
        return mThumbPhoto;
    }

    public static final class ResourceParams extends ChatItemHolder.ResourceParams {
        public int downloadCancelOpenId;
        public int progressId;
        public int fileStatusImageId;
        public int filenameId;
        public int fileInfoId;
        public int thumbId;
    }
}
