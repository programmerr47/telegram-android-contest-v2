package com.msds.awesometelechat.adapter.item.chat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.SingleTextItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.AdapterItem;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
public class NewMessagesItem implements AdapterItem {

    private final Context mContext;
    private int mCount;
    private String mStatus;

    public NewMessagesItem(@NonNull Context context, int count) {
        this.mContext = context;
        this.mCount = count;
        this.mStatus = String.format(mContext.getResources().getQuantityString(R.plurals.NEW_MESSAGES_INFO, mCount), mCount);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        SingleTextItemHolder holder = (SingleTextItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_new_messages, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new SingleTextItemHolder(view, R.id.countOfNewMessages);
            }
        };
    }

    public void setCount(int count) {
        this.mCount = count;
        this.mStatus = String.format(mContext.getResources().getQuantityString(R.plurals.NEW_MESSAGES_INFO, mCount), mCount);
    }

    private void bindView(SingleTextItemHolder viewHolder, int position) {
        viewHolder.getTextView().setText(mStatus);
    }
}
