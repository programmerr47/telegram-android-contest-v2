package com.msds.awesometelechat.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
public final class CountryItemHolder extends RecyclerView.ViewHolder {

    private TextView mCountryName;
    private TextView mCountryCode;

    public CountryItemHolder(View itemView, ResourceParams params) {
        super(itemView);

        mCountryName = (TextView) itemView.findViewById(params.countryNameId);
        mCountryCode = (TextView) itemView.findViewById(params.countryCodeId);
    }

    public TextView getCountryName() {
        return mCountryName;
    }

    public TextView getCountryCode() {
        return mCountryCode;
    }

    public static final class ResourceParams {
        public int countryNameId;
        public int countryCodeId;
    }
}
