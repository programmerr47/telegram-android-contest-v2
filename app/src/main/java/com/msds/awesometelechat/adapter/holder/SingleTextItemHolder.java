package com.msds.awesometelechat.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
public class SingleTextItemHolder extends RecyclerView.ViewHolder {

    TextView mTextView;

    public SingleTextItemHolder(View itemView, int textId) {
        super(itemView);

        mTextView = (TextView) itemView.findViewById(textId);
    }

    public TextView getTextView() {
        return mTextView;
    }
}
