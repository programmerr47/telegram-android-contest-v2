package com.msds.awesometelechat.adapter.holder;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.msds.awesometelechat.view.CircularProgressBar;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class ChatVideoItemHolder extends ChatItemHolder {

    FrameLayout mThumbContainer;
    ImageView mThumb;
    CircularProgressBar mProgress;
    private ImageView mFileStatusImage;

    public ChatVideoItemHolder(View itemView, ResourceParams params) {
        super(itemView, params);

        mThumbContainer = (FrameLayout) itemView.findViewById(params.thumbContainerId);
        mThumb = (ImageView) itemView.findViewById(params.thumbViewId);
        mProgress = (CircularProgressBar) itemView.findViewById(params.progressId);
        mFileStatusImage = (ImageView) itemView.findViewById(params.fileStatusImageId);
    }

    public FrameLayout getThumbContainer() {
        return mThumbContainer;
    }

    public ImageView getThumb() {
        return mThumb;
    }

    public CircularProgressBar getProgress() {
        return mProgress;
    }

    public ImageView getFileStatusImage() {
        return mFileStatusImage;
    }

    public static final class ResourceParams extends ChatItemHolder.ResourceParams {
        public int thumbContainerId;
        public int thumbViewId;
        public int progressId;
        public int fileStatusImageId;
    }
}
