package com.msds.awesometelechat.adapter.item.chat.chatlist;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.util.UserNameFormatter;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-05-11
 */
public final class ChatListLastMessageVisitor {

    public static ChatListItem setlastTextMessage(ChatListItem item, TdApi.Message message, TdApi.User sender, String senderName) {
        TdApi.MessageContent content = message.message;

        if (content instanceof TdApi.MessageText) {
            item.setLastMessage(((TdApi.MessageText) content).text, senderName);
        } else if (content instanceof TdApi.MessageAudio) {
            item.setLastSystemMessage(senderName + item.getContext().getString(R.string.audio));
        } else if (content instanceof TdApi.MessageVideo) {
            item.setLastSystemMessage(senderName + item.getContext().getString(R.string.video));
        } else if (content instanceof TdApi.MessagePhoto) {
            item.setLastSystemMessage(senderName + item.getContext().getString(R.string.photo));
        } else if (content instanceof TdApi.MessageDocument) {
            item.setLastSystemMessage(senderName + item.getContext().getString(R.string.document));
        }  else if (content instanceof TdApi.MessageContact) {
            item.setLastSystemMessage(senderName + item.getContext().getString(R.string.contact));
        }   else if (content instanceof TdApi.MessageGeoPoint) {
            item.setLastSystemMessage(senderName + item.getContext().getString(R.string.geo_point));
        } else if (content instanceof TdApi.MessageGroupChatCreate) {
            item.setLastSystemMessage(R.string.SYSTEM_TEXT_CREATED_GROUP, sender);
        } else if (content instanceof TdApi.MessageChatAddParticipant) {
            TdApi.User user = ((TdApi.MessageChatAddParticipant) content).user;
            item.setLastSystemMessage(R.string.SYSTEM_TEXT_JOINED_GROUP, user);
        } else if (content instanceof TdApi.MessageChatDeleteParticipant) {
            TdApi.User user = ((TdApi.MessageChatDeleteParticipant) content).user;
            item.setLastSystemMessage(R.string.SYSTEM_TEXT_LEFT_GROUP, user);
        } else if (content instanceof TdApi.MessageChatChangeTitle) {
            item.setLastSystemMessage(R.string.SYSTEM_TEXT_RENAMED_GROUP, sender);
        } else if (content instanceof TdApi.MessageChatChangePhoto) {
            item.setLastSystemMessage(R.string.SYSTEM_TEXT_CHANGED_GROUP_PHOTO, sender);
        } else if (content instanceof TdApi.MessageChatDeletePhoto) {
            item.setLastSystemMessage(R.string.SYSTEM_TEXT_REMOVED_GROUP_PHOTO, sender);
        }

        item.setLastTime(new Date(message.date * 1000L));
        return item;
    }
}
