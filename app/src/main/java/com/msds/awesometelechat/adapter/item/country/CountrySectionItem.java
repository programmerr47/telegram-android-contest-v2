package com.msds.awesometelechat.adapter.item.country;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.CountryItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;
import com.msds.awesometelechat.adapter.item.section.SectionItem;

import static com.msds.awesometelechat.collections.Countries.Country;

/**
 * @author Michael Spitsin
 * @since 2014-05-05
 */
public class CountrySectionItem implements SectionItem {
    private final Context mContext;
    private final Country mCountry;
    private final int mSectionItemPosition;

    public CountrySectionItem(@NonNull Context context, Country country, int sectionItemPosition) {
        this.mContext = context;
        this.mCountry = country;
        this.mSectionItemPosition = sectionItemPosition;
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        CountryItemHolder holder = (CountryItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.country_item, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                CountryItemHolder.ResourceParams params = new CountryItemHolder.ResourceParams();
                params.countryNameId = R.id.name;
                params.countryCodeId = R.id.code;
                return new CountryItemHolder(view, params);
            }
        };
    }

    @Override
    public int getSectionPositionInList() {
        return mSectionItemPosition;
    }

    public Country getCountry() {
        return mCountry;
    }

    private void bindView(CountryItemHolder viewHolder, int position) {
        viewHolder.getCountryCode().setText(mCountry.getCode());
        viewHolder.getCountryName().setText(mCountry.getName());
    }
}
