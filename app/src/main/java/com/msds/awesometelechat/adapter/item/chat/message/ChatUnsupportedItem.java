package com.msds.awesometelechat.adapter.item.chat.message;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.holder.SingleTextItemHolder;
import com.msds.awesometelechat.adapter.holder.producer.HolderProducer;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Date;

/**
 * @author Michael Spitsin
 * @since 2015-06-28
 */
public class ChatUnsupportedItem extends ChatItem {

    private String type;

    public static ChatUnsupportedItem createFromMessage(@NonNull Context context, @NonNull TdApi.Message message) {
        ChatUnsupportedItem result = new ChatUnsupportedItem(context, message.id, message.fromId, message.chatId);
        result.setTimestamp(new Date(message.date * 1000L));
        result.type = message.message.getClass().getSimpleName();
        return result;
    }

    protected ChatUnsupportedItem(@NonNull Context context, int messageId, int userId, long chatId) {
        super(context, messageId, userId, chatId);
    }

    @Override
    public void bindView(RecyclerView.ViewHolder viewHolder, int position) {
        SingleTextItemHolder holder = (SingleTextItemHolder) viewHolder;
        bindView(holder, position);
    }

    @Override
    public HolderProducer getViewHolderProducer() {
        return new HolderProducer() {
            @Override
            public RecyclerView.ViewHolder produce(ViewGroup parentView) {
                LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.single_chat_status, parentView, false);

                if (view == null) {
                    throw new IllegalStateException("View not created");
                }

                return new SingleTextItemHolder(view, R.id.fileInfo);
            }
        };
    }

    private void bindView(SingleTextItemHolder holder, int position) {
        holder.getTextView().setText(mContext.getString(R.string.UNSUPPORTED_MESSAGE_INFO) + ": " + type);
    }
}
