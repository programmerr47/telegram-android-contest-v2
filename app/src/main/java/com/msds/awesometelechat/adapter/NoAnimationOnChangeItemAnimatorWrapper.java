package com.msds.awesometelechat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

/**
 * @author Michael Spitsin
 * @since 2015-08-07
 */
public class NoAnimationOnChangeItemAnimatorWrapper extends RecyclerView.ItemAnimator {

    private RecyclerView.ItemAnimator mWrappedAnimator;

    public NoAnimationOnChangeItemAnimatorWrapper(@NonNull RecyclerView.ItemAnimator wrappedAnimator) {
        this.mWrappedAnimator = wrappedAnimator;
    }

    @Override
    public void runPendingAnimations() {
        mWrappedAnimator.runPendingAnimations();
    }

    @Override
    public boolean animateRemove(RecyclerView.ViewHolder holder) {
        return mWrappedAnimator.animateRemove(holder);
    }

    @Override
    public boolean animateAdd(RecyclerView.ViewHolder holder) {
        return mWrappedAnimator.animateAdd(holder);
    }

    @Override
    public boolean animateMove(RecyclerView.ViewHolder holder, int fromX, int fromY, int toX, int toY) {
        return mWrappedAnimator.animateMove(holder, fromX, fromY, toX, toY);
    }

    @Override
    public boolean animateChange(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder, int fromLeft, int fromTop, int toLeft, int toTop) {
        return true;
    }

    @Override
    public void endAnimation(RecyclerView.ViewHolder item) {
        mWrappedAnimator.endAnimation(item);
    }

    @Override
    public void endAnimations() {
        mWrappedAnimator.endAnimations();
    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
