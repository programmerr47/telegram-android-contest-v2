package com.msds.awesometelechat.login;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.msds.awesometelechat.AnimationListener;
import com.msds.awesometelechat.DrawerActivity;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.CustomTypefaceSpan;
import com.msds.awesometelechat.util.ErrorCode;
import com.msds.awesometelechat.util.PhoneFormat.PhoneFormat;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginActivityCodeFragment extends Fragment {

    private Client mClient;
    private AnimationListener mListener;
    private LoginActivityCallbacks mCallbacks;

    private TextView mAuthMessage;
    private EditText mAuthCode;
    private TextView mErrorMessage;
    private Toolbar mToolbar;
    private View mActionDone;

    private Handler mUiHandler;
    private ProgressDialog mProgressDialog;

    private boolean isFirstRequestAfterCreation;

    private String mPhoneNumber;

    public static LoginActivityCodeFragment createInstance() {
        return new LoginActivityCodeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mUiHandler = new Handler();
        mClient = TG.getClientInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_code, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mAuthMessage = (TextView) view.findViewById(R.id.authMessage);
        mAuthCode = (EditText) view.findViewById(R.id.authCode);
        mErrorMessage = (TextView) view.findViewById(R.id.errorMessage);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mActionDone = view.findViewById(R.id.action_done);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mToolbar.setTitle(getString(R.string.ACTIVATION_CODE));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        SpannableStringBuilder formattedString = getMultiFontString(getActivity(), R.string.SEND_CODE_INFO, mPhoneNumber);
        mAuthMessage.setText(formattedString);

        isFirstRequestAfterCreation = true;

        mAuthCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    actionCheckActivationCode();
                    return true;
                }
                return false;
            }
        });
        mAuthCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mErrorMessage != null) {
                    mErrorMessage.setText("");
                }
            }
        });

        mActionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionCheckActivationCode();
            }
        });

        mCallbacks.setToolbar(mToolbar);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isVisible()) {
            if (isFirstRequestAfterCreation) {
                mAuthCode.requestFocus();
                isFirstRequestAfterCreation = false;
            } else {
                Util.forceRequestFocusOnView(getActivity(), mAuthCode);
            }
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        Util.forceClearFocusOnView(getActivity(), mAuthCode);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        int id = enter ? R.animator.fragment_slide_in : R.animator.fragment_slide_out;
        final Animator anim = AnimatorInflater.loadAnimator(getActivity(), id);
        if (enter) {
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mListener.onAnimationEnd();
                }
            });
        }
        return anim;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (AnimationListener) activity;
            mCallbacks = (LoginActivityCallbacks)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AnimationListener");
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = PhoneFormat.getInstance().format(phoneNumber);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public void actionCheckActivationCode() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.LOADING));
        mProgressDialog.show();

        mClient.send(new TdApi.AuthSetCode(mAuthCode.getText().toString()),
                new UIResultHandler(new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }

                        if (object instanceof TdApi.AuthStateOk) {
                            Intent intent = new Intent(getActivity(), DrawerActivity.class);
                            intent.setFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (object instanceof TdApi.Error) {
                            if (getActivity() != null) {
                                mErrorMessage.setText(ErrorCode.getUnderstandableErrorMessage(getActivity(), (TdApi.Error) object));
                            }
                        }
                    }
                }));
    }

    private SpannableStringBuilder getMultiFontString(@NonNull Context context, int smsCodeInfoResId, String mPhoneNumber) {
        String sourceString = String.format(context.getString(smsCodeInfoResId), mPhoneNumber);
        int defaultPartLength = sourceString.length() - mPhoneNumber.length();

        Typeface robotoRegular = Typeface.createFromAsset(context.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR);
        Typeface robotoBold = Typeface.createFromAsset(context.getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_BOLD);

        SpannableStringBuilder result = new SpannableStringBuilder(sourceString);
        result.setSpan(new CustomTypefaceSpan("", robotoRegular), 0, defaultPartLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        result.setSpan(new CustomTypefaceSpan("", robotoBold), defaultPartLength, sourceString.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        return result;
    }
}
