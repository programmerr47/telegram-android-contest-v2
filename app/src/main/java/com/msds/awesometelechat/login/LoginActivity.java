package com.msds.awesometelechat.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.msds.awesometelechat.AnimationListener;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.collections.Countries;


//TODO REFACTOR: rewrite this workarounds. Use methods to get information about top element of back stack instead of visibility flags
public class LoginActivity extends AppCompatActivity implements
        LoginActivityCallbacks,
        AnimationListener,
        FragmentManager.OnBackStackChangedListener,
        OnCountrySelectedListener {

    private static final String MAIN_FRAGMENT_VISIBILITY_STATE = "MAIN_FRAGMENT_VISIBILITY_STATE";
    private static final String ACTIVATION_CODE_FRAGMENT_VISIBILITY_STATE = "ACTIVATION_CODE_FRAGMENT_VISIBILITY_STATE";
    private static final String COUNTRY_FRAGMENT_VISIBILITY_STATE = "COUNTRY_FRAGMENT_VISIBILITY_STATE";
    private static final String REGISTER_FRAGMENT_VISIBILITY_STATE = "REGISTER_FRAGMENT_VISIBILITY_STATE";

    private static final String LAST_FRAGMENT_VISIBILITY_STATE = "LAST_FRAGMENT_VISIBILITY_STATE";

    private LoginActivityFragment mFillPhoneFragment;
    private LoginActivityCodeFragment mFillSmsCodeFragment;
    private LoginChooseCountryFragment mGetCountryFragment;
    private RegisterFragment mRegisterFragment;
    private View mDarkCoverView;
    private View mDarkCoverViewUpper;

    //Temp booleans from saved state to manage visibility of fragments
    //TODO remove them and use back stack info (use not null names when adding to backstack, getting for name and other stuff)
    private boolean isFillSmsCodeFragmentVisible;
    private boolean isFillPhoneFragmentVisible = true;
    private boolean isGetCountryFragmentVisible;
    private boolean isRegisterFragmentVisible;

    private boolean isLastVisibleFragmentRegister;

    private boolean isAnimating;
    private boolean didUpFragmentSlideOut;
    private boolean didUppestFragmentSlideOut;

    private boolean isCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (savedInstanceState != null) {
            getSavedState(savedInstanceState);
        }

        isCreated = false;

        FragmentManager fm = getFragmentManager();

        mFillPhoneFragment = (LoginActivityFragment) fm.findFragmentById(R.id.fillPhoneFragmentContainer);
        if (mFillPhoneFragment == null) {
            mFillPhoneFragment = LoginActivityFragment.createInstance();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.fillPhoneFragmentContainer, mFillPhoneFragment);
            transaction.commit();
        }

        mFillSmsCodeFragment = (LoginActivityCodeFragment) fm.findFragmentById(R.id.move_to_back_container_upper);
        if (mFillSmsCodeFragment == null) {
            mFillSmsCodeFragment = LoginActivityCodeFragment.createInstance();
        } else {
            didUppestFragmentSlideOut = true;
        }

        Fragment f = fm.findFragmentById(R.id.move_to_back_container);
        if (f == null) {
            mRegisterFragment = RegisterFragment.createInstance();
            mGetCountryFragment = LoginChooseCountryFragment.createInstance();
        } else {
            if (f instanceof RegisterFragment) {
                mRegisterFragment = (RegisterFragment) f;
                mGetCountryFragment = LoginChooseCountryFragment.createInstance();
            } else if (f instanceof LoginChooseCountryFragment) {
                mGetCountryFragment = (LoginChooseCountryFragment) f;
                mRegisterFragment = RegisterFragment.createInstance();
            } else {
                throw new IllegalStateException("Uknown fragment: " + f);
            }
            didUpFragmentSlideOut = true;
        }

        mDarkCoverView = findViewById(R.id.darkCover);
        mDarkCoverViewUpper = findViewById(R.id.darkCoverUpper);

        mDarkCoverView.setAlpha(0f);
        mDarkCoverViewUpper.setAlpha(0f);

        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        isCreated = true;

        Toolbar toolbar = getApropriateToolbar();
        updateToolbar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (isFillPhoneFragmentVisible) {
                throw new IllegalStateException("Up button clicked on Main Login Fragment");
            } else if (isGetCountryFragmentVisible) {
                goToChooseCountryFragment();
            } else if (isFillSmsCodeFragmentVisible) {
                goToFillSMSCodeFragment(null, isLastVisibleFragmentRegister);
            } else if (isRegisterFragmentVisible) {
                goToRegisterFragment(null);
            } else {
                throw new IllegalStateException("illegal state all fragments are invisible");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(MAIN_FRAGMENT_VISIBILITY_STATE, isFillPhoneFragmentVisible);
        outState.putBoolean(ACTIVATION_CODE_FRAGMENT_VISIBILITY_STATE, isFillSmsCodeFragmentVisible);
        outState.putBoolean(COUNTRY_FRAGMENT_VISIBILITY_STATE, isGetCountryFragmentVisible);
        outState.putBoolean(REGISTER_FRAGMENT_VISIBILITY_STATE, isRegisterFragmentVisible);

        outState.putBoolean(LAST_FRAGMENT_VISIBILITY_STATE, isLastVisibleFragmentRegister);
    }

    @Override
    public void goToRegisterFragment(String phone) {
        if (isAnimating) {
            return;
        }
        isAnimating = true;
        if (didUpFragmentSlideOut) {
            didUpFragmentSlideOut = false;
            getFragmentManager().popBackStack();
            setFillPhoneFragmentVisible();
        } else {
            didUpFragmentSlideOut = true;
            isLastVisibleFragmentRegister = false;

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(
                    R.animator.fragment_slide_in,
                    R.animator.fragment_slide_out,
                    R.animator.fragment_slide_in,
                    R.animator.fragment_slide_out);
            mRegisterFragment.setPhoneNumber(phone);
            transaction.replace(R.id.move_to_back_container, mRegisterFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            makeBaseFragmentDarker(mDarkCoverView);
            setRegisterFragmentVisible();
        }
    }

    @Override
    public void goToFillSMSCodeFragment(String phone, boolean isCallingFragmentIsRegister) {
        if (isAnimating) {
            return;
        }
        isAnimating = true;
        if (didUppestFragmentSlideOut) {
            didUppestFragmentSlideOut = false;
            getFragmentManager().popBackStack();
            if (isLastVisibleFragmentRegister) {
                setRegisterFragmentVisible();
            } else {
                setFillPhoneFragmentVisible();
            }
        } else {
            didUppestFragmentSlideOut = true;
            isLastVisibleFragmentRegister = isCallingFragmentIsRegister;

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(
                    R.animator.fragment_slide_in,
                    R.animator.fragment_slide_out,
                    R.animator.fragment_slide_in,
                    R.animator.fragment_slide_out);
            mFillSmsCodeFragment.setPhoneNumber(phone);
            transaction.replace(R.id.move_to_back_container_upper, mFillSmsCodeFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            makeBaseFragmentDarker(mDarkCoverViewUpper);
            setFillSmsCodeFragmentVisible();
        }
    }

    @Override
    public void goToChooseCountryFragment() {
        if (isAnimating) {
            return;
        }
        isAnimating = true;
        if (didUpFragmentSlideOut) {
            didUpFragmentSlideOut = false;
            getFragmentManager().popBackStack();
            setFillPhoneFragmentVisible();
        } else {
            didUpFragmentSlideOut = true;
            isLastVisibleFragmentRegister = false;

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(
                    R.animator.fragment_slide_up,
                    R.animator.fragment_slide_down,
                    R.animator.fragment_slide_up,
                    R.animator.fragment_slide_down);
            transaction.replace(R.id.move_to_back_container, mGetCountryFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            makeBaseFragmentDarker(mDarkCoverView);
            setCountryFragmentVisible();
        }
    }

    @Override
    public void setToolbar(Toolbar toolbar) {
        if (isCreated) {
            updateToolbar(toolbar);
        }
    }

    @Override
    public void onAnimationEnd() {
        isAnimating = false;
        invalidateOptionsMenu();
    }

    @Override
    public void onAnimationStart() {
        isAnimating = true;
    }

    private void getSavedState(Bundle savedInstanceState) {
        isFillPhoneFragmentVisible = savedInstanceState.getBoolean(MAIN_FRAGMENT_VISIBILITY_STATE);
        isFillSmsCodeFragmentVisible = savedInstanceState.getBoolean(ACTIVATION_CODE_FRAGMENT_VISIBILITY_STATE);
        isGetCountryFragmentVisible = savedInstanceState.getBoolean(COUNTRY_FRAGMENT_VISIBILITY_STATE);
        isRegisterFragmentVisible = savedInstanceState.getBoolean(REGISTER_FRAGMENT_VISIBILITY_STATE);

        isLastVisibleFragmentRegister = savedInstanceState.getBoolean(LAST_FRAGMENT_VISIBILITY_STATE);
    }

    private void makeBaseFragmentDarker(View darkView) {
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(darkView, "alpha", 0.0f, 0.5f);
        darkHoverViewAnimator.setInterpolator(new DecelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet s = new AnimatorSet();
        s.play(darkHoverViewAnimator);
        s.start();
    }

    private void makeBaseFragmentLighter(View darkView) {
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(darkView, "alpha", 0.5f, 0.0f);
        darkHoverViewAnimator.setInterpolator(new AccelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet s = new AnimatorSet();
        s.play(darkHoverViewAnimator);
        s.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimating = false;
                updateToolbar(getApropriateToolbar());
            }
        });
        s.start();
    }

    @Override
    public void onBackStackChanged() {
        if (!didUpFragmentSlideOut || !didUppestFragmentSlideOut) {
            if (!didUppestFragmentSlideOut) {
                makeBaseFragmentLighter(mDarkCoverViewUpper);
            }

            if (!didUpFragmentSlideOut) {
                makeBaseFragmentLighter(mDarkCoverView);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (didUppestFragmentSlideOut) {
            didUppestFragmentSlideOut = false;
            getFragmentManager().popBackStack();
            if (isLastVisibleFragmentRegister) {
                setRegisterFragmentVisible();
            } else {
                setFillPhoneFragmentVisible();
            }
        } else if (didUpFragmentSlideOut) {
            didUpFragmentSlideOut = false;
            getFragmentManager().popBackStack();
            setFillPhoneFragmentVisible();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSelect(Countries.Country country) {
        mFillPhoneFragment.setSelectedCountry(country);
        goToChooseCountryFragment();
    }

    public void setFillPhoneFragmentVisible() {
        isFillPhoneFragmentVisible = true;
        isGetCountryFragmentVisible = false;
        isFillSmsCodeFragmentVisible = false;
        isRegisterFragmentVisible = false;
    }

    public void setFillSmsCodeFragmentVisible() {
        isFillPhoneFragmentVisible = false;
        isGetCountryFragmentVisible = false;
        isFillSmsCodeFragmentVisible = true;
        isRegisterFragmentVisible = false;
    }

    public void setCountryFragmentVisible() {
        isFillPhoneFragmentVisible = false;
        isGetCountryFragmentVisible = true;
        isFillSmsCodeFragmentVisible = false;
        isRegisterFragmentVisible = false;
    }

    public void setRegisterFragmentVisible() {
        isFillPhoneFragmentVisible = false;
        isGetCountryFragmentVisible = false;
        isFillSmsCodeFragmentVisible = false;
        isRegisterFragmentVisible = true;
    }

    private Toolbar getApropriateToolbar() {
        if (isFillSmsCodeFragmentVisible) {
            return mFillSmsCodeFragment.getToolbar();
        } else if (isGetCountryFragmentVisible) {
            return mGetCountryFragment.getToolbar();
        } else if (isFillPhoneFragmentVisible) {
            return mFillPhoneFragment.getToolbar();
        } else if (isRegisterFragmentVisible) {
            return mRegisterFragment.getToolbar();
        } else {
            throw new IllegalStateException("illegal state all fragments are invisible");
        }
    }

    private void fillActionBar(ActionBar actionBar) {
        if (isFillSmsCodeFragmentVisible) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else if (isGetCountryFragmentVisible) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else if (isFillPhoneFragmentVisible) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        } else if (isRegisterFragmentVisible) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
            throw new IllegalStateException("illegal state all fragments are invisible");
        }
    }

    private void updateToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        fillActionBar(actionBar);
    }
}
