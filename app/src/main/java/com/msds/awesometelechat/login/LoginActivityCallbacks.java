package com.msds.awesometelechat.login;

import android.support.v7.widget.Toolbar;

/**
 * @author Michael Spitsin
 * @since 2015-05-03
 */
//TODO descr
interface LoginActivityCallbacks {
    void goToRegisterFragment(String phone);
    //TODO remove this hardcode param
    void goToFillSMSCodeFragment(String phone, boolean isCallingFragmentIsRegister);
    void goToChooseCountryFragment();
    void setToolbar(Toolbar toolar);
}
