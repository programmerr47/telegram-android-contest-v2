package com.msds.awesometelechat.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.msds.awesometelechat.R;
import com.msds.awesometelechat.collections.Countries;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.util.Constants;
import com.msds.awesometelechat.util.ErrorCode;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Map;

import static com.msds.awesometelechat.collections.Countries.Country;

/**
 * A placeholder fragment containing a simple view.
 */
//TODO implement request focuses and other stuff
//TODO think where initialise Countries.class
public class LoginActivityFragment extends Fragment implements Client.ResultHandler {

    private Client mClient;
    private LoginActivityCallbacks mLoginActivityCallbacks;

    private EditText mCountry;
    private EditText mPhoneCode;
    private EditText mPhoneNumber;
    private Toolbar mToolbar;
    private View mActionDone;

    private ProgressDialog mProgressDialog;
    private AlertDialog mErrorDialog;

    private final Country mDefaultCountry = Countries.getInstance().getDefaultCountry();
    private final Map<String, Country> mCodesMap = Countries.getInstance().getCodesMap();
    private Country mSelectedCountry;
    private String mUserWrittenCode;

    private boolean isFirstRequestAfterCreation;

    public static LoginActivityFragment createInstance() {
        return new LoginActivityFragment();
    }

    public LoginActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mClient = TG.getClientInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mCountry = (EditText) view.findViewById(R.id.country);
        mPhoneCode = (EditText) view.findViewById(R.id.phoneCode);
        mPhoneNumber = (EditText) view.findViewById(R.id.phoneNumber);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mActionDone = view.findViewById(R.id.action_done);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        isFirstRequestAfterCreation = true;

        mToolbar.setTitle(getString(R.string.PHONE_NUMBER));

        mActionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    actionSendPhoneNumber();
            }
        });

        mPhoneCode.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR));
        mPhoneNumber.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), Constants.ASSETS_FONTS_DIR + Constants.ROBOTO_REGULAR));

        mPhoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    actionSendPhoneNumber();
                    return true;
                }
                return false;
            }
        });

        mPhoneNumber.setFocusableInTouchMode(true);
        mPhoneNumber.requestFocus();

        mPhoneCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mUserWrittenCode = s.toString();

                if (s.length() < 2) {
                    if (s.length() == 0) {
                        s.append(Constants.COUNTRY_CODE_PREFIX);
                    }

                    mCountry.setText("");
                    mCountry.setHint(R.string.CHOOSE_COUNTRY);
                    mCountry.setHintTextColor(getResources().getColor(R.color.gray));
                } else {
                    Country codeCountry = mCodesMap.get(mUserWrittenCode);
                    if (codeCountry == null) {
                        mCountry.setText("");
                        mCountry.setHint(R.string.WRONG_COUNTRY_CODE);
                        mCountry.setHintTextColor(getResources().getColor(R.color.red_moderate));
                    } else {
                        mCountry.setText(codeCountry.getName());
                        mCountry.setHint(R.string.CHOOSE_COUNTRY);
                        mCountry.setHintTextColor(getResources().getColor(R.color.gray));
                    }
                }
            }
        });

        if (mUserWrittenCode != null) {
            mPhoneCode.setText(mUserWrittenCode);
        } else if (mSelectedCountry != null) {
            mPhoneCode.setText(mSelectedCountry.getCode());
        } else {
            mPhoneCode.setText(mDefaultCountry.getCode());
        }

        mCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginActivityCallbacks.goToChooseCountryFragment();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isVisible()) {
            if (mPhoneCode.getText().length() < 2) {
                if (isFirstRequestAfterCreation) {
                    mPhoneCode.requestFocus();
                    isFirstRequestAfterCreation = false;
                } else {
                    Util.forceRequestFocusOnView(getActivity(), mPhoneCode);
                }
            } else {
                if (isFirstRequestAfterCreation){
                    mPhoneNumber.requestFocus();
                    isFirstRequestAfterCreation = false;
                } else {
                    Util.forceRequestFocusOnView(getActivity(), mPhoneNumber);
                }
            }
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        Util.forceClearFocusOnView(getActivity(), mPhoneCode);
        Util.forceClearFocusOnView(getActivity(), mPhoneNumber);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mLoginActivityCallbacks = (LoginActivityCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement LoginActivityCallbacks");
        }
    }

    @Override
    public void onResult(final TdApi.TLObject object) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (object instanceof TdApi.Error) {
            if (getActivity() != null) {
                TdApi.Error error = (TdApi.Error) object;
                mErrorDialog = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.app_name)
                        .setMessage(ErrorCode.getUnderstandableErrorMessage(getActivity(), error))
                        .setPositiveButton(R.string.OK, null)
                        .show();
            }
        } else if (object instanceof TdApi.AuthStateWaitSetCode) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoginActivityCallbacks.goToFillSMSCodeFragment(mPhoneCode.getText().toString() + mPhoneNumber.getText().toString(), false);
                }
            });
        } else if (object instanceof TdApi.AuthStateWaitSetName) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoginActivityCallbacks.goToRegisterFragment(mPhoneCode.getText().toString() + mPhoneNumber.getText().toString());
                }
            });
        } else {
            throw new IllegalStateException("Unknown object: " + object);
        }
    }

    public void setSelectedCountry(Country country) {
        this.mSelectedCountry = country;
        this.mPhoneCode.setText(country.getCode());
    }

    protected void clearFocuses() {
        mPhoneNumber.clearFocus();
        mPhoneCode.clearFocus();
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    private void actionSendPhoneNumber() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.LOADING));
        mProgressDialog.show();

        TdApi.AuthSetPhoneNumber setPhoneNumberFunc = new TdApi.AuthSetPhoneNumber(mPhoneCode.getText().toString() + mPhoneNumber.getText().toString());
        mClient.send(setPhoneNumberFunc, new UIResultHandler(LoginActivityFragment.this));
    }
}
