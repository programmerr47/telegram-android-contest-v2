package com.msds.awesometelechat.login;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.AnimationListener;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.CountriesAdapter;
import com.msds.awesometelechat.adapter.item.section.SectionItem;
import com.msds.awesometelechat.view.YFractionalFrameLayout;
import com.tonicartos.superslim.LayoutManager;

import java.util.ArrayList;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
public class LoginChooseCountryFragment extends Fragment {

    private AnimationListener mAnimationListener;
    private OnCountrySelectedListener mCountrySelectedListener;
    private LoginActivityCallbacks mCallbacks;

    private YFractionalFrameLayout mLayoutView;
    private RecyclerView mRecyclerView;
    private CountriesAdapter mCountryAdapter;
    private Toolbar mToolbar;

    public static LoginChooseCountryFragment createInstance() {
        return new LoginChooseCountryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_countries, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLayoutView = (YFractionalFrameLayout) view;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.countryList);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mToolbar.setTitle(getString(R.string.COUNTRY));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        mCountryAdapter = new CountriesAdapter(getActivity(), new ArrayList<SectionItem>(), mCountrySelectedListener);
        mRecyclerView.setLayoutManager(new LayoutManager(getActivity()));
        mRecyclerView.setAdapter(mCountryAdapter);

        mCallbacks.setToolbar(mToolbar);
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
    {
        int id = enter ? R.animator.fragment_slide_up : R.animator.fragment_slide_down;
        final Animator anim = AnimatorInflater.loadAnimator(getActivity(), id);
        if (enter) {
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
//                    //TODO need to do this because of list hide first element :(
//                    mRecyclerView.getLayoutManager().scrollToPosition(0);
//                    mRecyclerView.smoothScrollBy(0, (int) - getActivity().getResources().getDisplayMetrics().density * getActivity().getResources().getInteger(R.integer.TOOLBAR_STANDARD_HEIGHT));
                    mAnimationListener.onAnimationEnd();
                }
            });
        }
        return anim;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mAnimationListener = (AnimationListener) activity;
            mCountrySelectedListener = (OnCountrySelectedListener) activity;
            mCallbacks = (LoginActivityCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AnimationListener and LoginActivityCallbacks");
        }
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }
}
