package com.msds.awesometelechat.login;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.msds.awesometelechat.AnimationListener;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.util.ErrorCode;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-14
 */
public class RegisterFragment extends Fragment {

    private Client mClient;

    private AnimationListener mListener;
    private LoginActivityCallbacks mCallbacks;

    private EditText mFirstNameField;
    private EditText mLastNameField;
    private Toolbar mToolbar;
    private View mActionDone;

    private String mPhoneNumber;

    private ProgressDialog mProgressDialog;
    private AlertDialog mErrorDialog;

    public static RegisterFragment createInstance() {
        return new RegisterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mClient = TG.getClientInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mFirstNameField = (EditText) view.findViewById(R.id.firstNameField);
        mLastNameField = (EditText) view.findViewById(R.id.lastNameField);
        mActionDone = view.findViewById(R.id.action_done);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mToolbar.setTitle(getString(R.string.YOUR_NAME));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        mActionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionRegister();
            }
        });

        mCallbacks.setToolbar(mToolbar);
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        int id = enter ? R.animator.fragment_slide_in : R.animator.fragment_slide_out;
        final Animator anim = AnimatorInflater.loadAnimator(getActivity(), id);
        if (enter) {
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mListener.onAnimationEnd();
                }
            });
        }
        return anim;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (AnimationListener) activity;
            mCallbacks = (LoginActivityCallbacks)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AnimationListener");
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    private void actionRegister() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.LOADING));
        mProgressDialog.show();

        if ("".equals(mFirstNameField.getText().toString().trim())) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            mErrorDialog = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.FIRST_NAME_IS_REQUIRED)
                    .setPositiveButton(R.string.OK, null)
                    .show();
            return;
        }

        TdApi.TLFunction setName = new TdApi.AuthSetName(mFirstNameField.getText().toString().trim(), mLastNameField.getText().toString().trim());
        mClient.send(setName, new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                if (object instanceof TdApi.AuthStateWaitSetCode) {
                    mCallbacks.goToFillSMSCodeFragment(mPhoneNumber, true);
                } else if (object instanceof TdApi.Error) {
                    if (getActivity() != null) {
                        TdApi.Error error = (TdApi.Error) object;
                        mErrorDialog = new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.app_name)
                                .setMessage(ErrorCode.getUnderstandableErrorMessage(getActivity(), error))
                                .setPositiveButton(R.string.OK, null)
                                .show();
                    }
                }
            }
        }));
    }
}
