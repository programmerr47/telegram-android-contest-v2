package com.msds.awesometelechat.login;

import static com.msds.awesometelechat.collections.Countries.Country;

/**
 * @author Michael Spitsin
 * @since 2015-05-07
 */
//todo descr
public interface OnCountrySelectedListener {
    void onSelect(Country country);
}
