package com.msds.awesometelechat.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * @author Michael Spitsin
 * @since 2015-05-05
 */
//todo descr
public class YFractionalFrameLayout extends FrameLayout {

    private int mScreenHeight;

    private float mYFraction;

    public YFractionalFrameLayout(Context context) {
        super(context);
    }

    public YFractionalFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public YFractionalFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public YFractionalFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mScreenHeight = h;
        setY((mScreenHeight > 0) ? (mScreenHeight - mYFraction * mScreenHeight) : 0);
    }

    public float getYFraction() {
        return mYFraction;
    }

    public void setYFraction(float yFraction) {
        mYFraction = yFraction;
        setY((mScreenHeight > 0) ? (mScreenHeight - mYFraction * mScreenHeight) : 0);
    }
}
