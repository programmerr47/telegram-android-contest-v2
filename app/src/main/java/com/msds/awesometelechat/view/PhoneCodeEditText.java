package com.msds.awesometelechat.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class PhoneCodeEditText extends EditText {
    public PhoneCodeEditText(Context context) {
        super(context);
    }

    public PhoneCodeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PhoneCodeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PhoneCodeEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onSelectionChanged(int start, int end) {
        CharSequence text = getText();
        if (text != null && text.length() > 0) {
            if (start < 1 || end < 1 || start != end) {
                setSelection(1);
            }
        }

        super.onSelectionChanged(start, end);
    }
}
