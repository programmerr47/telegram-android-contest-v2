package com.msds.awesometelechat.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.msds.awesometelechat.R;

/**
 * Simple extension of {@link ImageView} that makes every image rounded.
 * Simply computes min side and make a circle inside view area with diameter
 * of this min side and fills it with src drawable that provided to this view.
 *
 * @author Michael Spitsin
 * @since 2015-04-23
 */
@Deprecated
public class RoundedImageView extends ImageView {

    private int mRaduis;

    public RoundedImageView(Context context) {
        super(context);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        retrieveAttrs(context, attrs);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        retrieveAttrs(context, attrs);
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {

        if (mRaduis != -1) {
            Bitmap imageViewBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas imageViewCanvas = new Canvas(imageViewBitmap);
            super.onDraw(imageViewCanvas);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setARGB(255, 255, 255, 255);
            canvas.drawARGB(255, 0, 0, 0);
            canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, mRaduis, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(imageViewBitmap, 0, 0, paint);
        } else {
            super.onDraw(canvas);
        }


//        Drawable drawable = getDrawable();
//
//        if (drawable == null) {
//            return;
//        }
//
//        if (getWidth() == 0 || getHeight() == 0) {
//            return;
//        }
//
//
//        Bitmap roundBitmap;
//        if (drawable instanceof BitmapDrawable) {
//            Bitmap b =  ((BitmapDrawable)drawable).getBitmap();
//            Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
//            roundBitmap =  getCroppedBitmap(bitmap, diameter);
//        } else if (drawable instanceof ColorDrawable) {
//            roundBitmap = getCroppedBitmap((ColorDrawable) drawable, getWidth(), getHeight(), diameter);
//        } else {
//            throw new IllegalStateException("No providing for " + drawable.getClass().getName());
//        }
//        canvas.drawBitmap(roundBitmap, 0,0, null);
//        if (mRaduis != -1) {
//            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//            canvas.drawCircle(getWidth() / 2.0f, getHeight() / 2.0f, mRaduis, paint);
//            paint.setXfermode(null);
//        }
    }

    private void retrieveAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RoundedImageView, 0, 0);
        try {
            mRaduis = a.getDimensionPixelSize(R.styleable.RoundedImageView_radius, -1);
        }finally {
            a.recycle();
        }
    }

}
