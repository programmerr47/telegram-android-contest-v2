package com.msds.awesometelechat.view;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.msds.awesometelechat.R;

/**
 * @author Michael Spitsin
 * @since 2015-05-12
 */
public class CircularProgressBar extends View {

    private static final int START_ANGLE = -90;

    private float mStrokeWidth = 4;
    private float mProgress = 0;
    private int mMin = 0;
    private int mMax = 100;
    private int mForegroundProgressColor = Color.DKGRAY;
    private int mBackgroundProgressColor = Color.LTGRAY;
    private RectF mRectF;
    private Paint mBackgroundPaint;
    private Paint mForegroundPaint;

    public CircularProgressBar(Context context) {
        super(context);
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mRectF = new RectF();
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularProgressBar, 0, 0);

        try {
            mStrokeWidth = typedArray.getDimension(R.styleable.CircularProgressBar_progressThickness, mStrokeWidth);
            mProgress = typedArray.getFloat(R.styleable.CircularProgressBar_progress, mProgress);
            mBackgroundProgressColor = typedArray.getColor(R.styleable.CircularProgressBar_progressBackgroundColor, mBackgroundProgressColor);
            mForegroundProgressColor = typedArray.getColor(R.styleable.CircularProgressBar_progressColor, mForegroundProgressColor);
            mMin = typedArray.getInt(R.styleable.CircularProgressBar_min, mMin);
            mMax = typedArray.getInt(R.styleable.CircularProgressBar_max, mMax);
        } finally {
            typedArray.recycle();
        }

        mBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBackgroundPaint.setColor(mBackgroundProgressColor);
        mBackgroundPaint.setStyle(Paint.Style.STROKE);
        mBackgroundPaint.setStrokeWidth(mStrokeWidth);

        mForegroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mForegroundPaint.setColor(mForegroundProgressColor);
        mForegroundPaint.setStyle(Paint.Style.STROKE);
        mForegroundPaint.setStrokeWidth(mStrokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawOval(mRectF, mBackgroundPaint);
        float angle = 360 * mProgress / mMax;
        canvas.drawArc(mRectF, START_ANGLE, angle, false, mForegroundPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);
        setMeasuredDimension(min, min);
        mRectF.set(getPaddingLeft() + mStrokeWidth / 2, getPaddingTop() + mStrokeWidth / 2, min - getPaddingRight() - mStrokeWidth / 2, min - getPaddingBottom() - mStrokeWidth / 2);
    }

    public float getStrokeWidth() {
        return mStrokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.mStrokeWidth = strokeWidth;
        mBackgroundPaint.setStrokeWidth(strokeWidth);
        mForegroundPaint.setStrokeWidth(strokeWidth);
        invalidate();
        requestLayout();//Because it should recalculate its bounds
    }

    public float getProgress() {
        return mProgress;
    }

    public void setProgress(float progress) {
        this.mProgress = progress;
        invalidate();
    }

    public int getMin() {
        return mMin;
    }

    public void setMin(int min) {
        this.mMin = min;
        invalidate();
    }

    public int getMax() {
        return mMax;
    }

    public void setMax(int max) {
        this.mMax = max;
        invalidate();
    }

    public int getForegroundProgressColor() {
        return mForegroundProgressColor;
    }

    public void setForegroundProgressColor(int foregroundProgressColor) {
        this.mForegroundProgressColor = foregroundProgressColor;
        mForegroundPaint.setColor(foregroundProgressColor);
        invalidate();
        requestLayout();
    }

    public int getBackgroundProgressColor() {
        return mBackgroundProgressColor;
    }

    public void setBackgroundProgressColor(int backgroundProgressColor) {
        this.mBackgroundProgressColor = backgroundProgressColor;
        mBackgroundPaint.setColor(backgroundProgressColor);
        invalidate();
        requestLayout();
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link android.animation.ObjectAnimator} Class automatically set the progress
     * so don't call the {@link CircularProgressBar#setProgress(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     */
    public void setProgressWithAnimation(float progress) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress);
        objectAnimator.setDuration(500);
        objectAnimator.setInterpolator(new AccelerateInterpolator());
        objectAnimator.start();
    }
}
