package com.msds.awesometelechat.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Michael Spitsin
 * @since 2015-05-15
 */
public class EmptyRecyclerView extends RecyclerView {
    private View mEmptyView;
    private View mLoadingView;
    private boolean isLoading;

    final private AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };

    public EmptyRecyclerView(Context context) {
        super(context);
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    void checkIfEmpty() {
        if (mEmptyView != null && getAdapter() != null) {
            final boolean emptyViewVisible = getAdapter().getItemCount() == 0;
            mEmptyView.setVisibility(emptyViewVisible ? VISIBLE : GONE);
            setVisibility(emptyViewVisible ? GONE : VISIBLE);
        }
    }

    @Override
    public void setAdapter(Adapter adapter) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }

        checkIfEmpty();
    }

    public void setLoadingView(View loadingView) {
        this.mLoadingView = loadingView;
        checkIfEmpty();
    }

    public void setEmptyView(View emptyView) {
        this.mEmptyView = emptyView;
        checkIfEmpty();
    }

    public void setLoadingState(boolean isLoading) {
        this.isLoading = isLoading;
        if (isLoading) {
            if (mLoadingView != null) {
                mLoadingView.setVisibility(VISIBLE);
            }
            if (mEmptyView != null) {
                mEmptyView.setVisibility(GONE);
            }
            setVisibility(GONE);
        } else {
            if (mLoadingView != null) {
                mLoadingView.setVisibility(GONE);
            }
            checkIfEmpty();
        }
    }

    public boolean getLoadingState(){
        return isLoading;
    }
}
