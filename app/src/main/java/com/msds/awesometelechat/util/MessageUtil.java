package com.msds.awesometelechat.util;

/**
 * @author Michael Spitsin
 * @since 2015-05-13
 */
public class MessageUtil {

    public static boolean isMessageSendable(String message) {
        for(int i = 0; i < message.length(); i++) {
            char symb = message.charAt(i);
            if (symb > '\u0020') {
                return true;
            }
        }

        return false;
    }
}
