package com.msds.awesometelechat.util;

import android.content.Context;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;

import org.drinkless.td.libcore.telegram.TdApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sydy on 07.05.2015.
 */
public class LocaleUtils {

    public static DateFormat formatterDay = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    public static DateFormat formatterWeek = new SimpleDateFormat("EEE", Locale.ENGLISH);
    public static DateFormat formatterMonth = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
    public static DateFormat formatterYear = new SimpleDateFormat("dd.MM.yy", Locale.ENGLISH);

    private static DateFormat chatDateItemFormatterMonth = new SimpleDateFormat("dd MMMM", Locale.ENGLISH);
    private static DateFormat chatDateItemFormatterYear = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);

    public static String formatDateForChatListElement(Date date) {
        Calendar rightNow = Calendar.getInstance();
        int day = rightNow.get(Calendar.DAY_OF_YEAR);
        int year = rightNow.get(Calendar.YEAR);
        rightNow.setTime(date);
        int dateDay = rightNow.get(Calendar.DAY_OF_YEAR);
        int dateYear = rightNow.get(Calendar.YEAR);

        if (year != dateYear) {
            return formatterYear.format(date);
        } else {
            int dayDiff = dateDay - day;
            if(dayDiff == 0
                    || dayDiff == -1
                    && System.currentTimeMillis() - date.getTime() < 1000L * 60 * 60 * 8)
            {
                return formatterDay.format(date);
            } else if(dayDiff > -7 && dayDiff <= -1) {
                return formatterWeek.format(date);
            } else {
                return formatterMonth.format(date);
            }
        }
    }

    public static String formatDateForChatDateSeparator(Date date) {
        Calendar rightNow = Calendar.getInstance();
        int year = rightNow.get(Calendar.YEAR);
        rightNow.setTime(date);
        int dateYear = rightNow.get(Calendar.YEAR);

        if (year != dateYear) {
            return chatDateItemFormatterYear.format(date);
        } else {
            return chatDateItemFormatterMonth.format(date);
        }
    }

    public static int compareDatesByDay(Date date1, Date date2) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        int day1 = calendar.get(Calendar.DAY_OF_YEAR);
        int year1 = calendar.get(Calendar.YEAR);
        calendar.setTime(date2);
        int day2 = calendar.get(Calendar.DAY_OF_YEAR);
        int year2 = calendar.get(Calendar.YEAR);

        if (year1 != year2) {
            return year1 > year2 ? 1 : -1;
        } else if (day1 != day2) {
            return day1 > day2 ? 1 : -1;
        }

        return 0;
    }

    public static String formatUserStatus(TdApi.UserStatus status) {
        Context context = AwesomeApplication.getAppContext();
        if (status instanceof TdApi.UserStatusOnline) {
            int currentTime = (int) (System.currentTimeMillis() / 1000);
            int expires = ((TdApi.UserStatusOnline) status).expires;
            if (expires > currentTime) {
                return context.getString(R.string.online);
            } else {
                return formatDateOnline(expires);
            }
        } else if (status instanceof TdApi.UserStatusRecently) {
            return context.getString(R.string.recently);
        } else if (status instanceof TdApi.UserStatusLastWeek) {
            return context.getString(R.string.within_a_week);
        } else if (status instanceof TdApi.UserStatusLastMonth) {
            return context.getString(R.string.within_a_month);
        } else if (status instanceof TdApi.UserStatusOffline) {
            return formatDateOnline(((TdApi.UserStatusOffline) status).wasOnline);
        }
        return "";
    }

    public static String formatDateOnline(long date) {
        Context context = AwesomeApplication.getAppContext();
        Calendar rightNow = Calendar.getInstance();
        int day = rightNow.get(Calendar.DAY_OF_YEAR);
        int year = rightNow.get(Calendar.YEAR);
        rightNow.setTimeInMillis(date * 1000);
        int dateDay = rightNow.get(Calendar.DAY_OF_YEAR);
        int dateYear = rightNow.get(Calendar.YEAR);

        if (dateDay == day && year == dateYear) {
            return String.format("%s %s %s", context.getString(R.string.last_seen), context.getString(R.string.today_at), formatterDay.format(new Date(date * 1000)));
        } else if (dateDay + 1 == day && year == dateYear) {
            return String.format("%s %s %s", context.getString(R.string.last_seen), context.getString(R.string.yesterday_at), formatterDay.format(new Date(date * 1000)));
        } else if (year == dateYear) {
            String format = context.getString(R.string.format_date_at_time, formatterMonth.format(new Date(date * 1000)), formatterDay.format(new Date(date * 1000)));
            return String.format("%s %s", context.getString(R.string.last_seen), format);
        } else {
            String format = context.getString(R.string.format_date_at_time, formatterYear.format(new Date(date * 1000)), formatterDay.format(new Date(date * 1000)));
            return String.format("%s %s", context.getString(R.string.last_seen), format);
        }
    }
}
