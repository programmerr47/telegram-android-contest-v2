package com.msds.awesometelechat.util;

import org.drinkless.td.libcore.telegram.TdApi;

public class UserNameFormatter {

    private UserNameFormatter() {
    }

    public static String fullName(TdApi.User user) {
        return (user.firstName + " " + user.lastName).trim();
    }
}
