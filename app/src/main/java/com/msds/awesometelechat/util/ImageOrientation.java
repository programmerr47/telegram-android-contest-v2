package com.msds.awesometelechat.util;

import android.media.ExifInterface;

/**
 * @author Michael Spitsin
 * @since 2015-07-15
 */
public enum ImageOrientation {
    NORMAL(ExifInterface.ORIENTATION_NORMAL, 0),
    ROTATE_90(ExifInterface.ORIENTATION_ROTATE_90, 90),
    ROTATE_180(ExifInterface.ORIENTATION_ROTATE_180, 180),
    ROTATE_270(ExifInterface.ORIENTATION_ROTATE_270, 270);

    private final int mTypeId;
    private final int mDegree;

    private ImageOrientation(int typeId, int degree) {
        mTypeId = typeId;
        mDegree = degree;
    }

    public int getDegree() {
        return mDegree;
    }

    public static ImageOrientation fromTypeId(int typeId) {
        for (ImageOrientation orientation : ImageOrientation.values()) {
            if (orientation.mTypeId == typeId) {
                return orientation;
            }
        }

        return NORMAL;
    }
}
