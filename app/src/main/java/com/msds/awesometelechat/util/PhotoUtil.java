package com.msds.awesometelechat.util;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Pair;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * All util functions related to photos in chats or anywhere.
 *
 * @author Michael Spitsin
 * @since 2015-07-04
 */
public class PhotoUtil {

    public static Pair<Integer, Integer> getMaxDimensionsOfMessagePhoto() {
        Resources applicationResources = AwesomeApplication.getAppContext().getResources();
        int maxWidth = applicationResources.getDimensionPixelSize(R.dimen.message_photo_width_max);
        DisplayMetrics displayMetrics = applicationResources.getDisplayMetrics();
        float ratio;
        if (displayMetrics.widthPixels < displayMetrics.heightPixels) {
            ratio = displayMetrics.heightPixels * 1.0f / displayMetrics.widthPixels;
        } else {
            ratio = displayMetrics.widthPixels * 1.0f / displayMetrics.heightPixels;
        }
        return new Pair<>(maxWidth, (int)(maxWidth * ratio));
    }

    public static Pair<Integer, Integer> getMessagePhotoDimensions(Pair<Integer, Integer> realDimensions, Pair<Integer, Integer> maxDimensions) {
        int scaledHeight = (int)(maxDimensions.first * realDimensions.second * 1.0 / realDimensions.first);
        int scaledWidth = maxDimensions.first;

        if (scaledHeight > maxDimensions.second) {
            scaledHeight = maxDimensions.second;
            scaledWidth = (int)(maxDimensions.second * realDimensions.first * 1.0 / realDimensions.second);
        }

        return new Pair<>(scaledWidth, scaledHeight);
    }

    public static Pair<Integer, Integer> getMessagePhotoDimensions(Pair<Integer, Integer> realDimensions) {
        return getMessagePhotoDimensions(realDimensions, getMaxDimensionsOfMessagePhoto());
    }

    public static Pair<Integer, Integer> getMessagePhotoDimensions(TdApi.PhotoSize photo) {
        return getMessagePhotoDimensions(new Pair<>(photo.width, photo.height));
    }

    public static TdApi.PhotoSize getMinPhotoSize(TdApi.Photo photoContainer, Pair<Integer, Integer> minDimensions) {
        TdApi.PhotoSize[] photos = photoContainer.photos;

        for (TdApi.PhotoSize photo : photos) {
            if (photo.width >= minDimensions.first || photo.height >= minDimensions.second) {
                return photo;
            }
        }

        if (photos.length > 0) {
            return photos[photos.length - 1];
        } else {
            return null;
        }
    }

    public static TdApi.PhotoSize getMinPhotoSizeForMessage(TdApi.Photo photo) {
        return getMinPhotoSize(photo, getMaxDimensionsOfMessagePhoto());
    }
}
