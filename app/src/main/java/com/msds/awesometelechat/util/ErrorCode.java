package com.msds.awesometelechat.util;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.msds.awesometelechat.R;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-19
 */
public enum ErrorCode {
    INVALID_PHONE_NUMBER(400, "PHONE_NUMBER_INVALID", R.string.INVALID_PHONE_NUMBER),
    INVALID_PHONE_CODE(400, "PHONE_CODE_INVALID", R.string.INVALID_PHONE_CODE);

    private int mCode;
    private String mText;
    private int mMessageResId;

    private ErrorCode(int code, String text, int messageResId) {
        this.mCode = code;
        this.mText = text;
        this.mMessageResId = messageResId;
    }

    public int getMessageResId() {
        return mMessageResId;
    }

    public static ErrorCode getFrom(TdApi.Error error) {
        for (ErrorCode errorCode : ErrorCode.values()) {
            if (errorCode.mCode == error.code && errorCode.mText != null && errorCode.mText.equals(error.text)) {
                return errorCode;
            }
        }

        return null;
    }

    public static String getUnderstandableErrorMessage(@NonNull Context context, TdApi.Error error) {
        ErrorCode errorCode = getFrom(error);
        return errorCode != null ? context.getString(errorCode.mMessageResId) : error.code + ": " + error.text;
    }
}
