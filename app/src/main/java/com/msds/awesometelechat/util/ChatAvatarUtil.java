package com.msds.awesometelechat.util;

import java.util.Locale;

/**
 * All common functions related to avatars in chats (related with ChatList page, Chat page, Drawer, ChatPageToolbar)
 *
 * @author Michael Spitsin
 * @since 2015-05-07
 */
public class ChatAvatarUtil {

    /**
     * Array of limited colors, for providing only a "good" selection by some information according to some method of hashing.
     */
    private final static int[] goodColors = {0xffe56555, 0xfff28c48, 0xffeec764, 0xff76c84d, 0xff5fbed5, 0xff549cdd, 0xff8e85ee, 0xfff2749a};

    public static int[] getAllBackgroundAvatarColors() {
        return goodColors;
    }

    public static int getBackgroundAvatarColor(int id) {
        int index = getHashIndex(id, goodColors.length);
        return goodColors[index];
    }

    /**
     * Computes non negative hash number according to given some integer, limiting this number by upper range.
     * <br><br>
     * <strong>Note: </strong> this method uses MD5 algorithm
     *
     * @param id given id
     * @param limit max number that can be returned, passing -1 will cancel limit
     * @return number in range {@code [0..limit]} or {@code [0..}{@link java.lang.Integer#MAX_VALUE}{@code ]} depends on limit param
     */
    public static int getHashIndex(int id, int limit) {
        if (id >= 0 && id < limit) {
            return id;
        }

        try {
            String str;
            if (id >= 0) {
                str = String.format(Locale.US, "%d%d", id, 0/*todo UserConfig.getClientUserId()*/);
            } else {
                str = String.format(Locale.US, "%d", id);
            }

            if (str.length() > 15) {
                str = str.substring(0, 15);
            }

            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] digest = md.digest(str.getBytes());

            int b = digest[Math.abs(id % 16)];
            if (b < 0) {
                b += 256;
            }

            return Math.abs(b) % limit;
        } catch (Exception e) {
//            FileLog.e("tmessages", e);todo
        }

        return id % limit;
    }

    public static String getAbbreviationFromString(String str) {
        if (str == null || str.isEmpty() || str.trim().isEmpty()) {
            return "";
        }

        String[] parts = str.trim().split(" ");

        if (parts.length == 0) {
            return "";
        } else if (parts.length == 1) {
            return parts[0].substring(0, 1).toUpperCase();
        } else {
            int checkedIndex = -1;
            StringBuilder result = new StringBuilder("");
            for(int i = 0; i < parts.length; i++) {
                if (!parts[i].isEmpty()) {
                    checkedIndex = i;
                    result.append(parts[i].charAt(0));
                    break;
                }
            }

            if (checkedIndex != -1) {
                for(int i = parts.length - 1; i > checkedIndex; i--) {
                    if (!parts[i].isEmpty()) {
                        result.append(parts[i].charAt(0));
                        break;
                    }
                }
            }

            return result.toString().toUpperCase();
        }
    }
}
