package com.msds.awesometelechat.util;

import android.os.Build;

/**
 * @author Michael Spitsin
 * @since 2015-07-07
 */
public class AndroidUtils {

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }
}
