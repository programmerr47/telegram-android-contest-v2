package com.msds.awesometelechat.util;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.msds.awesometelechat.R;

/**
 * @author Michael Spitsin
 * @since 2015-05-13
 */
//todo refactoring: itroduce animation wrapper classes and anim managers classes
//todo for more flexible and readable code
public class AnimationUtil {

    public static Animator showChatScroller(@NonNull Context context, @NonNull final View scrollerView) {
        Animator showingViewAnim = AnimatorInflater.loadAnimator(context, R.animator.fade_in);

        showingViewAnim.setTarget(scrollerView);

        AnimatorSet animation = new AnimatorSet();
        animation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                scrollerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                scrollerView.setVisibility(View.GONE);
            }
        });

        animation.play(showingViewAnim);
        animation.start();
        return animation;
    }

    public static Animator hideChatScroller(@NonNull Context context, @NonNull final View scrollerView) {
        Animator hidingViewAnim = AnimatorInflater.loadAnimator(context, R.animator.fade_out);

        hidingViewAnim.setTarget(scrollerView);

        AnimatorSet animation = new AnimatorSet();
        animation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                scrollerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                scrollerView.setVisibility(View.GONE);
            }
        });

        animation.play(hidingViewAnim);
        animation.start();
        return animation;
    }

    public static Animator showSendMessageButton(@NonNull Context context, @NonNull final View sendView, @NonNull final View hidingView) {
        Animator sendViewAnim = AnimatorInflater.loadAnimator(context, R.animator.send_icon_slide_in);
        Animator hidingViewAnim = AnimatorInflater.loadAnimator(context, R.animator.scale_down);

        sendViewAnim.setTarget(sendView);
        hidingViewAnim.setTarget(hidingView);

        AnimatorSet animation = new AnimatorSet();
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                sendView.setVisibility(View.VISIBLE);
                hidingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                hidingView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                sendView.setVisibility(View.GONE);
                hidingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animation.play(sendViewAnim).with(hidingViewAnim);
        animation.start();
        return animation;
    }

    public static Animator hideSendMessageButton(@NonNull Context context, @NonNull final View sendView, @NonNull final View revealView) {
        Animator sendViewAnim = AnimatorInflater.loadAnimator(context, R.animator.send_icon_slide_out);
        Animator revealViewAnim = AnimatorInflater.loadAnimator(context, R.animator.scale_up);

        sendViewAnim.setTarget(sendView);
        revealViewAnim.setTarget(revealView);

        AnimatorSet animation = new AnimatorSet();
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                revealView.setVisibility(View.VISIBLE);
                sendView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                sendView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                sendView.setVisibility(View.VISIBLE);
                revealView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animation.play(sendViewAnim).with(revealViewAnim);
        animation.start();
        return animation;
    }

    public static Animator getOpenAttachmentsAnimator(@NonNull Context context, @NonNull View attachments, @NonNull final View darkCover) {
        Animator openAttachmentsAnimator = AnimatorInflater.loadAnimator(context, R.animator.photos_layout_slide_up);
        openAttachmentsAnimator.setTarget(attachments);

        darkCover.setBackgroundColor(0xff000000);
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(darkCover, "alpha", 0.0f, 0.5f);
        darkHoverViewAnimator.setInterpolator(new DecelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet resultAnimation = new AnimatorSet();
        resultAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                darkCover.setVisibility(View.VISIBLE);
            }
        });
        resultAnimation
                .play(openAttachmentsAnimator)
                .with(darkHoverViewAnimator);
        resultAnimation.start();
        return resultAnimation;
    }

    public static Animator getCloseAttachmentsAnimator(@NonNull Context context, @NonNull View attachments, @NonNull final View darkCover) {
        Animator closeAttachmentsAnimator = AnimatorInflater.loadAnimator(context, R.animator.photos_layout_slide_down);
        closeAttachmentsAnimator.setTarget(attachments);

        darkCover.setBackgroundColor(0xff000000);
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(darkCover, "alpha", 0.5f, 0.0f);
        darkHoverViewAnimator.setInterpolator(new AccelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet resultAnimation = new AnimatorSet();
        resultAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                darkCover.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                darkCover.setVisibility(View.VISIBLE);
            }
        });
        resultAnimation
                .play(closeAttachmentsAnimator)
                .with(darkHoverViewAnimator);
        return resultAnimation;
    }

    public static Animator getCheckViewAnimator(@NonNull Context context, @NonNull final View checkbox, @NonNull final View darkCover){
        Animator checkViewAnimator = AnimatorInflater.loadAnimator(context, R.animator.checkbox_slide_in);
        checkViewAnimator.setTarget(checkbox);

        darkCover.setBackgroundColor(0xff000000);
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(darkCover, "alpha", 0.0f, 0.2f);
        darkHoverViewAnimator.setInterpolator(new DecelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet resultAnimation = new AnimatorSet();
        resultAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                darkCover.setVisibility(View.VISIBLE);
                checkbox.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                darkCover.setVisibility(View.VISIBLE);
                checkbox.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                darkCover.setVisibility(View.GONE);
                checkbox.setVisibility(View.GONE);
            }
        });
        resultAnimation
                .play(checkViewAnimator)
                .with(darkHoverViewAnimator);
        return resultAnimation;
    }

    public static Animator getUnCheckViewAnimator(@NonNull Context context, @NonNull final View checkbox, @NonNull final View darkCover){
        Animator checkViewAnimator = AnimatorInflater.loadAnimator(context, R.animator.checkbox_slide_out);
        checkViewAnimator.setTarget(checkbox);

        darkCover.setBackgroundColor(0xff000000);
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(darkCover, "alpha", 0.2f, 0.0f);
        darkHoverViewAnimator.setInterpolator(new AccelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet resultAnimation = new AnimatorSet();
        resultAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                darkCover.setVisibility(View.VISIBLE);
                checkbox.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                darkCover.setVisibility(View.GONE);
                checkbox.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                darkCover.setVisibility(View.VISIBLE);
                checkbox.setVisibility(View.VISIBLE);
            }
        });
        resultAnimation
                .play(checkViewAnimator)
                .with(darkHoverViewAnimator);
        return resultAnimation;
    }
}
