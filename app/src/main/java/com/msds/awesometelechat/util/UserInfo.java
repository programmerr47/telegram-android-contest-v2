package com.msds.awesometelechat.util;

import org.drinkless.td.libcore.telegram.TdApi;

public class UserInfo {
    private static TdApi.User me;

    private UserInfo() {
    }

    public static TdApi.User getMe() {
        return me;
    }

    public static void setMe(TdApi.User me) {
        UserInfo.me = me;
    }
}
