package com.msds.awesometelechat.util;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-07-12
 */
public class FileUtil {
    public static int getFileId(TdApi.File file) {
        if (file instanceof TdApi.FileEmpty) {
            return ((TdApi.FileEmpty) file).id;
        } else {
            return ((TdApi.FileLocal) file).id;
        }
    }
}
