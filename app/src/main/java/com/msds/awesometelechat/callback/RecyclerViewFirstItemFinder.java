package com.msds.awesometelechat.callback;

import android.support.v7.widget.RecyclerView;

/**
 * @author Michael Spitsin
 * @since 2015-05-07
 */
public interface RecyclerViewFirstItemFinder {
    int findFirstVisibleItemPosition(RecyclerView recyclerView);
}
