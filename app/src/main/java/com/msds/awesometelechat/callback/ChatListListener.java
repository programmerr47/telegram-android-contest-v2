package com.msds.awesometelechat.callback;

import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;

/**
 * //todo descr
 * @author Michael Spitsin
 * @since 2015-04-29
 */
public interface ChatListListener {

    /**
     * Called when an item in list of chats (for example in
     * {@link com.msds.awesometelechat.adapter.ChatListAdapter this list})
     * is clicked
     *
     * @param item clicked item
     * @param position position of clicked item
     */
    void onChatListItemClick(ChatListItem item, int position);
}
