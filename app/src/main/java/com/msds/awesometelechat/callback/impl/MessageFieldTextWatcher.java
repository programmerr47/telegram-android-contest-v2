package com.msds.awesometelechat.callback.impl;

import android.animation.Animator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.msds.awesometelechat.util.AnimationUtil;
import com.msds.awesometelechat.util.MessageUtil;

/**
 * @author Michael Spitsin
 * @since 2015-05-13
 */
public final class MessageFieldTextWatcher implements TextWatcher {

    private View mSendView;
    private View mAttachmentsView;
    private Context mContext;

    private Animator mShowingAnim;
    private Animator mHidingAnim;

    public MessageFieldTextWatcher(@NonNull Context context, @NonNull View sendView, @NonNull View attachmentsView) {
        this.mContext = context;
        this.mSendView = sendView;
        this.mAttachmentsView = attachmentsView;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        String message = s.toString().trim();todo
//        if (!message.isEmpty()) {
//            TG.getClientInstance().send(new TdApi.SendMessageTypingAction(), EmptyResultHandler.INSTANCE);
//        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() > 0 && MessageUtil.isMessageSendable(s.toString())) {
            if (mHidingAnim != null && mHidingAnim.isStarted()) {
                mHidingAnim.cancel();
            }

            if (mSendView.getVisibility() != View.VISIBLE) {
                mShowingAnim = AnimationUtil.showSendMessageButton(mContext, mSendView, mAttachmentsView);
            }
        } else {
            if (mShowingAnim != null && mShowingAnim.isStarted()) {
                mShowingAnim.cancel();
            }

            if (mAttachmentsView.getVisibility() != View.VISIBLE) {
                mHidingAnim = AnimationUtil.hideSendMessageButton(mContext, mSendView, mAttachmentsView);
            }
        }
    }
}
