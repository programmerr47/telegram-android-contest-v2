package com.msds.awesometelechat.callback;

/**
 * @author Michael Spitsin
 * @since 2015-07-14
 */
public interface ChatScrollManager {
    void scrollToTop();
}
