package com.msds.awesometelechat.callback;

import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.adapter.item.chat.DateItem;
import com.msds.awesometelechat.adapter.item.chat.message.ChatItem;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;

/**
 * @author Michael Spitsin
 * @since 2015-05-27
 */
public interface ChatItemNotifier {
    void notifyItemChanged(AdapterItem item);
    void notifyFileItemCreating(FileableItem item);
    void notifyThumbItemCreating(FileableItem item);
}
