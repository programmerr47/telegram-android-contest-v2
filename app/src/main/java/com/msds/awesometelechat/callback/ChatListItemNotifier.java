package com.msds.awesometelechat.callback;

import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;

/**
 * @author Michael Spitsin
 * @since 2015-07-07
 */
public interface ChatListItemNotifier {
    void notifyElementChanged(ChatListItem item);
}
