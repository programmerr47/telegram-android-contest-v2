package com.msds.awesometelechat.callback;

import com.msds.awesometelechat.adapter.item.drawer.DrawerItem;

/**
 * Callbacks interface that all activities using this fragment must implement.
 *
 * @author Michael Spitsin
 * @since 2015-04-12
 */
public interface NavigationDrawerListener {

    /**
     * Called when an item in the navigation drawer is selected.
     *
     * @param selectedItem item that was selected
     * @param position position of item that was selected
     */
    void onNavigationDrawerItemSelected(DrawerItem selectedItem, int position);
}
