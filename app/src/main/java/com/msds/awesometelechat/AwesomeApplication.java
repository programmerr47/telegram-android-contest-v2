package com.msds.awesometelechat;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.msds.awesometelechat.caches.images.ImageCache;
import com.msds.awesometelechat.caches.images.UriImageWorker;
import com.msds.awesometelechat.collections.Countries;
import com.msds.awesometelechat.adapter.item.photo.PhotoItem;
import com.msds.awesometelechat.libhelpers.updating.UpdateManager;
import com.msds.awesometelechat.libhelpers.updating.UpdaterCreator;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.List;

/**
 * @author Michael Spitsin
 * @since 2015-05-02
 */
public class AwesomeApplication extends Application {

    private static volatile Context mContext;

    private static String internalDbDir;
    private static Handler mUiHanlder;

    private static UpdateManager mUpdateManager = new UpdateManager(UpdaterCreator.UI);
    private static PhotoThumbsHolder photoThumbsHolder = new PhotoThumbsHolder();
    private static UriImageWorker imageWorker;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();

        UriImageWorker.init(mContext);
        imageWorker = UriImageWorker.getInstance();
        imageWorker.addImageCache(new ImageCache.ImageCacheParams());

        mUiHanlder = new Handler();

        internalDbDir = getFilesDir().toString();
        TG.setUpdatesHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                //Log.v("FUCK", "Update: " + object);
                mUpdateManager.update(object);
            }
        });
        TG.setDir(AwesomeApplication.getDbDir());

        //TODO replace it in loginActivity
        Countries.getInstance(this);
    }

    public static UpdateManager getUpdateManager() {
        return mUpdateManager;
    }

    public static Handler getUiHandler() {
        return mUiHanlder;
    }

    public static String getDbDir() {
        return internalDbDir;
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static PhotoThumbsHolder getPhotoThumbsHolder() {
        return photoThumbsHolder;
    }

    public static UriImageWorker getImageWorker() {
        return imageWorker;
    }

    public static final class PhotoThumbsHolder {
        private List<PhotoItem> mItems;

        private PhotoThumbsHolder() {
        }

        public void placeNewItems(List<PhotoItem> items) {
            mItems = items;
        }

        public List<PhotoItem> release() {
            if (mItems != null) {
                List<PhotoItem> tempItems = mItems;
                mItems = null;
                return tempItems;
            } else {
                return null;
            }
        }
    }
}
