package com.msds.awesometelechat;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.adapter.AbstractMultiTypeRecyclerAdapter;
import com.msds.awesometelechat.adapter.SpecialAdapters;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.drawer.DrawerElementName;
import com.msds.awesometelechat.adapter.item.drawer.DrawerHeaderItem;
import com.msds.awesometelechat.adapter.item.drawer.DrawerItem;
import com.msds.awesometelechat.adapter.item.drawer.DrawerPrimaryItem;
import com.msds.awesometelechat.callback.NavigationDrawerListener;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 *
 * @author Michael Spitsin
 * @since 2014-10-14
 */
public class NavigationDrawerFragment extends Fragment implements NavigationDrawerListener {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerListener mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerRecyclerView;
    private AbstractMultiTypeRecyclerAdapter<DrawerItem> mDrawerAdapter;
    private View mFragmentContainerView;
    private Toolbar mToolbar;

    private RecyclerItems<DrawerItem> mDrawerItems;

    private Handler mUiHandler = new Handler();

    private int mCurrentSelectedPosition = 1;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_drawer_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mDrawerRecyclerView = (RecyclerView) view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);

        mDrawerItems = new RecyclerItems<>(new ArrayList<DrawerItem>());
        mDrawerItems.add(new DrawerHeaderItem.Builder(getActivity()).setPhoneNumber("").setUsername("").setUserId(-1).setName(DrawerElementName.HEADER).build());
        mDrawerItems.add(new DrawerPrimaryItem.Builder(getActivity()).setIcon(R.drawable.ic_logout).setTitle(R.string.LOG_OUT).setName(DrawerElementName.LOG_OUT).build());

        mDrawerAdapter = SpecialAdapters.createDrawerAdapter(mDrawerItems, this);

        mDrawerRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mDrawerRecyclerView.setAdapter(mDrawerAdapter);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mToolbar = toolbar;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                mToolbar,             /* nav drawer image to replace 'Up' caret */
                R.string.NAVIGATION_DRAWER_OPEN,  /* "open drawer" description for accessibility */
                R.string.NAVIGATION_DRAWER_CLOSE  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                syncState();
            }
        };

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void setMe(final Context context, TdApi.User me) {
        final DrawerHeaderItem updatedHeaderItem = new DrawerHeaderItem.Builder(context)
            .setName(DrawerElementName.HEADER)
            .setUsername(me.firstName + " " + me.lastName)
            .setPhoneNumber(me.phoneNumber)
            .setUserId(me.id)
            .build();
        if (me.photoSmall instanceof TdApi.FileLocal) {
            updatedHeaderItem.setAvatarUri(Uri.parse(((TdApi.FileLocal) me.photoSmall).path));
        } else if (me.photoSmall instanceof TdApi.FileEmpty) {
            final int photoId = ((TdApi.FileEmpty) me.photoSmall).id;
            if (photoId != 0) { //todo test
                Client client = TG.getClientInstance();
                client.send(new TdApi.DownloadFile(photoId), new UIResultHandler(new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object instanceof TdApi.Ok) {
                            AwesomeApplication.getUpdateManager().addWatcher(
                                    TdApi.UpdateFile.class,
                                    new UpdateWatcher<TdApi.UpdateFile>() {
                                        @Override
                                        public void onUpdate(TdApi.UpdateFile file) {
                                            if (file.fileId == photoId) {
                                                updatedHeaderItem.setAvatarUri(Uri.parse(file.path));
                                                AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateFile.class, this);
                                                mDrawerAdapter.notifyItemChanged(0);
                                            }
                                        }
                                    });
                        }
                    }
                }));
            }
        }
        mDrawerItems.set(0, updatedHeaderItem);
        mDrawerAdapter.notifyItemChanged(0);
    }

    private void selectItem(int position) {
        selectItem(mDrawerItems.get(position), position);
    }

    private void selectItem(DrawerItem item, int position) {
        mCurrentSelectedPosition = position;

        if (mDrawerLayout != null && item.getName() != DrawerElementName.HEADER) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }

        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(item, position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private ActionBar getActionBar() {
        AppCompatActivity parentActivity = (AppCompatActivity) getActivity();
        return parentActivity.getSupportActionBar();
    }

    @Override
    public void onNavigationDrawerItemSelected(DrawerItem selectedItem, int position) {
        selectItem(position);
    }
}
