package com.msds.awesometelechat.collections;

import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.adapter.item.AdapterItem;
import com.msds.awesometelechat.adapter.item.chat.LoadingItem;
import com.msds.awesometelechat.adapter.item.chat.NewMessagesItem;
import com.msds.awesometelechat.adapter.item.chat.message.ChatItem;
import com.msds.awesometelechat.adapter.item.chat.message.abstractions.FileableItem;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-05-26
 */
public class MessagesManager {
    private RecyclerItems<ChatItem> mMessages;
    private List<ChatItem> mUnreadMessages = new LinkedList<>();
    private Map<Integer, ChatItem> mMessagesById = new HashMap<>();
    private Map<Integer, Set<FileableItem>> mPhotoMessagesByPhotoId = new HashMap<>();
    private Map<Integer, Set<FileableItem>> mMessagesWithThumbByThumbId = new HashMap<>();

    public MessagesManager(@NonNull RecyclerItems<ChatItem> messages) {
        this.mMessages = messages;
    }

    public MessagesManager(@NonNull List<ChatItem> messagesContainer) {
        this.mMessages = new RecyclerItems<>(messagesContainer);
    }

    public RecyclerItems<ChatItem> getMessages() {
        return mMessages;
    }

    public Map<Integer, ChatItem> getMapMessagesById() {
        return mMessagesById;
    }

    public List<ChatItem> getUnreadMessages() {
        return mUnreadMessages;
    }

    public Map<Integer, Set<FileableItem>> getFileMessagesByFileId() {
        return mPhotoMessagesByPhotoId;
    }

    public Map<Integer, Set<FileableItem>> getMessagesWithThumbByThumbId() {
        return mMessagesWithThumbByThumbId;
    }

    public void addNewMessage(ChatItem item) {
        mMessages.add(item);
        mMessagesById.put(item.systemInfo.getMessageId(), item);
        if (item.isNew()) {
            mUnreadMessages.add(item);
        }
    }

    public ChatItem getLastItem() {
        int index = mMessages.size() - 1;
        if (index >= 0) {
            return mMessages.get(index);
        } else {
            return null;
        }
    }

    public int messagesCount() {
        return mMessages.size();
    }

    public void addAll(MessagesManager messagesManager) {
        mMessages.addAll(messagesManager.mMessages);
        mMessagesById.putAll(messagesManager.mMessagesById);
        mUnreadMessages.addAll(messagesManager.mUnreadMessages);
        mPhotoMessagesByPhotoId.putAll(messagesManager.mPhotoMessagesByPhotoId);
    }

    public void addAllToTop(MessagesManager messagesManager) {
        mMessagesById.putAll(messagesManager.mMessagesById);
        mPhotoMessagesByPhotoId.putAll(messagesManager.mPhotoMessagesByPhotoId);
        mMessages.addAll(0, messagesManager.mMessages);
        mUnreadMessages.addAll(0, messagesManager.mUnreadMessages);
    }

    public void clear() {
        mMessages.clear();
        mMessagesById.clear();
        mUnreadMessages.clear();
        mPhotoMessagesByPhotoId.clear();
    }
}
