package com.msds.awesometelechat.collections;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.msds.awesometelechat.adapter.item.chat.message.ChatItem;
import com.msds.awesometelechat.callback.ChatItemNotifier;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.util.UserNameFormatter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-05-27
 */
public class ParticipantsManager {
    private Set<TdApi.User> mUsers = new HashSet<>();
    private Map<Integer, TdApi.User> mMapUserById = new HashMap<>();
    private Map<Integer, Set<ChatItem>> mUserChatItems = new HashMap<>();
    private Map<Integer, Set<ChatItem>> mPhotoChatItems = new HashMap<>();

    public void setUsernameAndPhotoForItem(@NonNull ChatItem item, @NonNull ChatItemNotifier chatItemNotifier, @NonNull TdApi.Message message) {
        setUsernameAndPhotoForItem(item, chatItemNotifier, message.fromId);
    }

    public void setUsernameAndPhotoForItem(@NonNull ChatItem item, @NonNull final ChatItemNotifier chatItemNotifier, final int messageFromId) {
        TdApi.User user = mMapUserById.get(messageFromId);
        if (user == null) {
            Set<ChatItem> items = mUserChatItems.get(messageFromId);
            if (items == null) {
                items = new HashSet<>();
                items.add(item);
                mUserChatItems.put(messageFromId, items);
                TG.getClientInstance().send(new TdApi.GetUser(messageFromId), new UIResultHandler(new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        TdApi.User user = (TdApi.User) object;
                        mMapUserById.put(messageFromId, user);
                        Set<ChatItem> items = mUserChatItems.get(user.id);
                        for (ChatItem item : items) {
                            setUsernameAndPhoto(item, user);
                            chatItemNotifier.notifyItemChanged(item);
                        }
                        mUserChatItems.remove(user.id);
                    }
                }));
            } else {
                items.add(item);
            }
        } else {
            setUsernameAndPhoto(item, user);
        }
    }

    private void setUsernameAndPhoto(final ChatItem item, TdApi.User user) {
        item.setUsername(UserNameFormatter.fullName(user));
        if (user.photoSmall instanceof TdApi.FileLocal) {
            item.setAvatar(Uri.parse(((TdApi.FileLocal) user.photoSmall).path));
        } else if (user.photoSmall instanceof TdApi.FileEmpty) {
            final int photoId = ((TdApi.FileEmpty) user.photoSmall).id;
            if (photoId != 0) { //todo test
                Set<ChatItem> items = mPhotoChatItems.get(photoId);
                if (items != null) {
                    items.add(item);
                } else {
                    TG.getClientInstance().send(new TdApi.DownloadFile(photoId), new UIResultHandler(new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                            if (object instanceof TdApi.Ok) {
                                Set<ChatItem> items = new HashSet<>();
                                items.add(item);
                                mPhotoChatItems.put(photoId, items);
                            }
                        }
                    }));
                }
            }
        }
    }

    public Set<ChatItem> getMessagesByAvatar(int avatarId) {
        return mPhotoChatItems.get(avatarId);
    }

    public void clearAvatarWaitingMessages(int avatarId) {
        mPhotoChatItems.remove(avatarId);
    }

    public TdApi.User getMessageAuthor(int messageId) {
        return mMapUserById.get(messageId);
    }

    public Set<ChatItem> getAllMessagesFromUser(int userId) {
        return mUserChatItems.get(userId);
    }
}
