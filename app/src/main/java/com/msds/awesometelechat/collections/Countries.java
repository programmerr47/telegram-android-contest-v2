package com.msds.awesometelechat.collections;

import android.content.Context;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;

import com.msds.awesometelechat.util.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-04-02
 */
//todo descr
public final class Countries {

    private static Countries instance;

    public static Countries getInstance() {
        return getInstance(null);
    }

    public static Countries getInstance(Context applicationContext) {
        if (instance == null) {
            if (applicationContext != null){
                instance = new Countries(applicationContext);
            }   else{
                throw new IllegalStateException("Param applicationContext can be null only if instance already initialized");
            }
        }

        return instance;
    }

    private final List<Country> mCountries = new ArrayList<>();

    private final Map<String, Country> mCodesMap = new HashMap<>();
    private final Map<String, Country> mCountriesMap = new HashMap<>();
    private final Map<String, Country> mLanguagesMap = new HashMap<>();

    private final Set<String> mCodes = new HashSet<>();

    private final Context mContext;

    private Countries(@NonNull Context context) {
        this.mContext = context;

        BufferedReader countriesReader = null;
        try {
            countriesReader = new BufferedReader(new InputStreamReader(context.getResources().getAssets().open(Constants.COUNTRIES_STANDARD_FILE_PATH)));
            String line;
            while ((line = countriesReader.readLine()) != null) {
                String[] parts = line.split(";");
                String code = Constants.COUNTRY_CODE_PREFIX + parts[0];
                Country country = new Country(code, parts[2], parts[1]);
                mCountries.add(country);
                mCodesMap.put(code, country);
                mCountriesMap.put(parts[2], country);
                mLanguagesMap.put(parts[1], country);
                mCodes.add(parts[0]);
            }

            Collections.sort(mCountries, new Comparator<Country>() {
                @Override
                public int compare(Country lhs, Country rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (countriesReader != null) {
                try {
                    countriesReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Country> getCountries() {
        return Collections.unmodifiableList(mCountries);
    }

    public Map<String, Country> getCodesMap() {
        return Collections.unmodifiableMap(mCodesMap);
    }

    public Map<String, Country> getCountriesMap() {
        return Collections.unmodifiableMap(mCountriesMap);
    }

    public Map<String, Country> getLanguagesMap() {
        return Collections.unmodifiableMap(mLanguagesMap);
    }

    public Set<String> getAllCodes() {
        return Collections.unmodifiableSet(mCodes);
    }

    public Country getDefaultCountry() {
        String lang = getDefaultLocale();
        if (lang != null) {
            return mLanguagesMap.get(lang);
        } else {
            return null;
        }
    }

    private String getDefaultLocale() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            return telephonyManager.getSimCountryIso().toUpperCase();
        } else {
            return null;
        }
    }

    public static final class Country {
        private String code;
        private String name;
        private String lang;

        private Country(@NonNull String code, @NonNull String name, @NonNull String lang) {
            this.code = code;
            this.name = name;
            this.lang = lang;
        }

        @NonNull
        public String getName() {
            return name;
        }

        @NonNull
        public String getCode() {
            return code;
        }

        @NonNull
        public String getLang() {
            return lang;
        }
    }
}
