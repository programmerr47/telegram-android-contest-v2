package com.msds.awesometelechat;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msds.awesometelechat.adapter.ChatListAdapter;
import com.msds.awesometelechat.adapter.NoAnimationOnChangeItemAnimatorWrapper;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;
import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListLoadingItem;
import com.msds.awesometelechat.callback.RecyclerViewFirstItemFinder;
import com.msds.awesometelechat.callback.impl.ToolbarHideScrollListener;
import com.msds.awesometelechat.concurrent.chattings.AbstractChatsDownloader;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;
import com.msds.awesometelechat.util.Util;
import com.msds.awesometelechat.view.EmptyRecyclerView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Fragment, that represents list of contacts (chatlists).
 *
 * @author Michael Spitsin
 * @since 2015-04-30
 */
public class ChatListFragment extends Fragment implements View.OnClickListener {

    private DrawerActivityCallBacks mCallBacks;

    private EmptyRecyclerView mChatListRecyclerView;
    private ChatListAdapter mChatListAdapter;
    private Toolbar mToolbar;
    private View mEmptyChatListView;
    private View mProgressChatListView;

    private ToolbarHideScrollListener mScrollListener = new ToolbarHideScrollListener(new RecyclerViewFirstItemFinder() {
        @Override
        public int findFirstVisibleItemPosition(RecyclerView recyclerView) {
            return ((LinearLayoutManager) mChatListRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        }
    });

    private RecyclerItems<ChatListItem> mChatListItems;
    private ChatListLoadingItem mLoadingItem;
    private Client mClient;

    private Map<Integer, ChatListItem> mPhotoChatListItems = new HashMap<>();
    private Map<Long, ChatListItem> mChatListItemsById = new HashMap<>();

    private String mConnectionState = DrawerActivity.CONNECTION_STATE_READY;

    private AbstractChatsDownloader mChatsDownloader = new AbstractChatsDownloader() {
        final RecyclerItems<ChatListItem> mDownloadedItems = new RecyclerItems<>(new ArrayList<ChatListItem>());
        final Map<Long, ChatListItem> mDownloadedItemsById = new HashMap<>();

        @Override
        protected void prepareLoading() {
            mDownloadedItems.clear();
            mDownloadedItemsById.clear();
        }

        @Override
        protected int doInBackground(TdApi.Chats chats, int limit) {
            for (TdApi.Chat chat : chats.chats) {
                final ChatListItem item = ChatListItem.createFromTdApiChat(AwesomeApplication.getAppContext(), mChatListAdapter, chat);
                if (item != null) {
                    TdApi.File photo = item.getPhotoSmall();
                    if (photo instanceof TdApi.FileEmpty) {
                        final int photoId = ((TdApi.FileEmpty) photo).id;
                        if (photoId != 0) { //todo test
                            mClient.send(new TdApi.DownloadFile(photoId), new UIResultHandler(new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    if (object instanceof TdApi.Ok) {
                                        mPhotoChatListItems.put(photoId, item);
                                    }
                                }
                            }));
                        }
                    }
                    mDownloadedItemsById.put(item.getId(), item);
                    mDownloadedItems.add(item);
                }
            }

            return mDownloadedItems.size();
        }

        @Override
        protected void onPartIsDownloaded() {
            int loadingItemPosition = mChatListItems.indexOf(mLoadingItem);
            if (loadingItemPosition != -1) {
                mChatListItems.remove(loadingItemPosition);
                mChatListAdapter.notifyItemRemoved(loadingItemPosition);
            }

            mChatListRecyclerView.setLoadingState(false);

            int realOffset = mChatListItems.size();
            mChatListItems.addAll(mDownloadedItems);
            mChatListItemsById.putAll(mDownloadedItemsById);
            mChatListAdapter.notifyItemRangeInserted(realOffset, mDownloadedItems.size());
        }
    };

    private UpdateWatcher<TdApi.UpdateFile> mPhotoDownloadHandler = new UpdateWatcher<TdApi.UpdateFile>() {
        @Override
        public void onUpdate(TdApi.UpdateFile file) {
            final ChatListItem item = mPhotoChatListItems.get(file.fileId);
            if (item != null) {
                item.setAvatar(Uri.parse(file.path));
                if (mChatListAdapter != null) {
                    mChatListAdapter.notifyItemChanged(mChatListItems.indexOf(item));
                }
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateNewMessage> mNewMessageHandler = new UpdateWatcher<TdApi.UpdateNewMessage>() {
        @Override
        public void onUpdate(TdApi.UpdateNewMessage object) {
            TdApi.Message message = object.message;
            ChatListItem oldItem = mChatListItemsById.get(message.chatId);
            processChatUpdating(message.chatId, oldItem);
        }
    };

    private UpdateWatcher<TdApi.UpdateMessageId> mNewMessageIdHandler = new UpdateWatcher<TdApi.UpdateMessageId>() {
        @Override
        public void onUpdate(TdApi.UpdateMessageId object) {
            ChatListItem oldItem = mChatListItemsById.get(object.chatId);
            processChatUpdating(object.chatId, oldItem);
        }
    };

    private UpdateWatcher<TdApi.UpdateChatReadInbox> mUpdateChatReadInbox = new UpdateWatcher<TdApi.UpdateChatReadInbox>() {
        @Override
        public void onUpdate(TdApi.UpdateChatReadInbox chatReadInbox) {
            ChatListItem item = mChatListItemsById.get(chatReadInbox.chatId);
            if (item != null) {
                item.setUnreadMessagesCount(chatReadInbox.unreadCount);
                final int position = mChatListItems.indexOf(item);
                if (mChatListAdapter != null) {
                    mChatListAdapter.notifyItemChanged(position);
                }
            }
        }
    };

    private UpdateWatcher<TdApi.UpdateChatReadOutbox> mUpdateChatReadOutbox = new UpdateWatcher<TdApi.UpdateChatReadOutbox>() {
        @Override
        public void onUpdate(TdApi.UpdateChatReadOutbox chatReadOutbox) {
            ChatListItem item = mChatListItemsById.get(chatReadOutbox.chatId);
            if (item != null && item.isLastOutgoingMessageUnread()) {
                item.setIsLastOutgoingMessageUnread(chatReadOutbox.lastRead != item.getChat().topMessage.id);
                final int position = mChatListItems.indexOf(item);
                mChatListAdapter.notifyItemChanged(position);
            }
        }
    };

    public ChatListFragment() {
        //Def counstr
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mClient = TG.getClientInstance();
        mChatListItems = new RecyclerItems<>(new ArrayList<ChatListItem>());

        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateFile.class, mPhotoDownloadHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateNewMessage.class, mNewMessageHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateMessageId.class, mNewMessageIdHandler);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateChatReadInbox.class, mUpdateChatReadInbox);
        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateChatReadOutbox.class, mUpdateChatReadOutbox);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chatter_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mChatListRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.recycleView);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mEmptyChatListView = view.findViewById(R.id.empty_chatlist_view);
        mProgressChatListView = view.findViewById(R.id.progress_chatlist_view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mLoadingItem == null) {
            mLoadingItem = new ChatListLoadingItem(getActivity());
        }

        if (mConnectionState.equals(DrawerActivity.CONNECTION_STATE_READY)) {
            mToolbar.setTitle(R.string.MESSAGES);
        } else {
            mToolbar.setTitle(mConnectionState);
        }
        mScrollListener.addView(mToolbar);
        mCallBacks.setToolbar(mToolbar);

        mChatListAdapter = new ChatListAdapter(mChatListItems, this);

        mChatListRecyclerView.setItemAnimator(new NoAnimationOnChangeItemAnimatorWrapper(mChatListRecyclerView.getItemAnimator()));
        mChatListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mChatListRecyclerView.setAdapter(mChatListAdapter);
        mChatListRecyclerView.setEmptyView(mEmptyChatListView);
        mChatListRecyclerView.setLoadingView(mProgressChatListView);

        if (mChatListItems.isEmpty()) {
            int visibleItemCount = Util.getMaxVisibleRowsInScreenRes(getActivity(), R.dimen.item_height_default);
            mChatListRecyclerView.setLoadingState(true);
            mChatsDownloader.download(0, (int)(visibleItemCount * 1.5));
        } else {
            mChatListRecyclerView.setLoadingState(false);
        }

        mChatListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mScrollListener.onScrolled(recyclerView, dx, dy);
                //todo will we always be sure that have instance of LinearLayoutManager
                if (mChatsDownloader.canDownload() && dy > 0) {
                    LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int count = manager.findLastVisibleItemPosition() - manager.findFirstVisibleItemPosition() + 1;
                    if (mChatListItems.size() < manager.findLastVisibleItemPosition() + count) {
                        if (!mChatListRecyclerView.getLoadingState()) {
                            mChatListItems.add(mLoadingItem);
                            mChatListAdapter.notifyDataSetChanged();
                        }

                        mChatsDownloader.download(mChatListItems.size(), count);
                    }
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallBacks = (DrawerActivityCallBacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AnimationListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mScrollListener.removeView(mToolbar);
        mChatListRecyclerView.removeOnScrollListener(mScrollListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateFile.class, mPhotoDownloadHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateNewMessage.class, mNewMessageHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateMessageId.class, mNewMessageIdHandler);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateChatReadInbox.class, mUpdateChatReadInbox);
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateChatReadOutbox.class, mUpdateChatReadOutbox);
    }

    @Override
    public void onClick(View view) {
        int position = mChatListRecyclerView.getChildAdapterPosition(view);
        ChatListItem clickedItem = mChatListAdapter.getItem(position);

        if (!clickedItem.isDeactivated()) {
            mCallBacks.goToChatFragment(clickedItem);
        }
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public void setConnectionState(String conntectionState) {
        this.mConnectionState = conntectionState == null ? DrawerActivity.CONNECTION_STATE_READY : conntectionState;
    }

    public void clearChatListItem(long chatId) {
        if (mChatListItemsById.containsKey(chatId)) {
            ChatListItem item = mChatListItemsById.get(chatId);
            int itemPosition = mChatListItems.indexOf(item);
            item.clear();

            if (itemPosition != -1) {
                mChatListAdapter.notifyItemChanged(itemPosition);
            }
        }
    }

    public void deleteChatListItem(long chatId) {
        if (mChatListItemsById.containsKey(chatId)) {
            ChatListItem item = mChatListItemsById.get(chatId);
            int itemPosition = mChatListItems.indexOf(item);

            mChatListItems.remove(item);
            mChatListItemsById.remove(chatId);

            if (itemPosition != -1) {
                mChatListAdapter.notifyItemRemoved(itemPosition);
            }
        }
    }

    private void processChatUpdating(long chatId, final ChatListItem oldItem) {
        //todo generalize
        mClient.send(new TdApi.GetChat(chatId), new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object instanceof TdApi.Chat) {
                    final int previousPosition = oldItem != null ? mChatListItems.indexOf(oldItem) : -1;
                    TdApi.Chat chat = (TdApi.Chat) object;
                    final ChatListItem item = ChatListItem.createFromTdApiChat(AwesomeApplication.getAppContext(), mChatListAdapter, chat);
                    if (item != null) {
                        TdApi.File photo = item.getPhotoSmall();
                        if (photo instanceof TdApi.FileEmpty) {
                            final int photoId = ((TdApi.FileEmpty) photo).id;
                            if (photoId != 0) { //todo test
                                mClient.send(new TdApi.DownloadFile(photoId), new UIResultHandler(new Client.ResultHandler() {
                                    @Override
                                    public void onResult(TdApi.TLObject object) {
                                        if (object instanceof TdApi.Ok) {
                                            mPhotoChatListItems.put(photoId, item);
                                        }
                                    }
                                }));
                            }
                        }

                        LinearLayoutManager manager = (LinearLayoutManager) mChatListRecyclerView.getLayoutManager();
                        boolean scrollToUp = false;
                        if (manager.findFirstVisibleItemPosition() == 0) {
                            scrollToUp = true;
                        }

                        if (oldItem != null) {
                            mChatListItems.remove(previousPosition);
                        }

                        mChatListItemsById.put(item.getId(), item);
                        mChatListItems.add(0, item);

                        if (mChatListAdapter != null) {
                            if (oldItem != null) {
                                mChatListAdapter.notifyItemChanged(previousPosition);
                                mChatListAdapter.notifyItemMoved(previousPosition, 0);
                            } else {
                                mChatListAdapter.notifyItemInserted(0);
                            }
                        }

                        if (scrollToUp) {
                            mChatListRecyclerView.scrollToPosition(0);
                        }
                    }
                }
            }
        }));
    }
}
