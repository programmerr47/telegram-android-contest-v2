package com.msds.awesometelechat.photos.gallery;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.item.photo.PhotoItem;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Michael Spitsin
 * @since 2015-05-17
 */
public class PhotoGalleryActivity extends AppCompatActivity implements GalleryActivityCallbacks {

    private PhotoGalleryFragment mGalleryFragment;
    private Toolbar mToolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);

        mGalleryFragment = (PhotoGalleryFragment) getFragmentManager().findFragmentById(R.id.fragment_container);
        if (mGalleryFragment == null) {
            mGalleryFragment = PhotoGalleryFragment.createInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, mGalleryFragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolbar(Toolbar toolbar) {
        this.mToolbar = toolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void sendPhotos(Collection<PhotoItem> photos) {
        AwesomeApplication.getPhotoThumbsHolder().placeNewItems(new ArrayList<>(photos));
        setResult(Activity.RESULT_OK);
        finish();
    }
}
