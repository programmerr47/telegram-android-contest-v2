package com.msds.awesometelechat.photos.gallery;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.R;
import com.msds.awesometelechat.adapter.PhotoThumbAdapter;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.adapter.item.photo.PhotoItem;
import com.msds.awesometelechat.callback.RecyclerViewFirstItemFinder;
import com.msds.awesometelechat.callback.impl.ToolbarHideScrollListener;
import com.msds.awesometelechat.concurrent.tasks.AsyncTaskWithListener;
import com.msds.awesometelechat.concurrent.tasks.GetGalleryImageUrisTask;
import com.msds.awesometelechat.view.EmptyRecyclerView;
import com.msds.awesometelechat.view.XFractionalFrameLayout;

import java.util.List;

/**
 * @author Michael Spitsin
 * @since 2015-05-17
 */
public class PhotoGalleryFragment extends Fragment implements
        AsyncTaskWithListener.OnTaskFinishedListener<List<PhotoItem>>,PhotoThumbAdapter.OnItemCheckStateListener, View.OnClickListener {

    private GalleryActivityCallbacks mCallBacks;

    private PhotoThumbAdapter mPhotosAdapter;
    private RecyclerItems<PhotoItem> mPhotos;

    private EmptyRecyclerView mPhotosRecyclerView;
    private View mEmptyView;
    private View mLoadingView;
    private Toolbar mToolbar;
    private XFractionalFrameLayout mActionSendContainer;

    private GridLayoutManager mPhotosLayoutManager;

    private ToolbarHideScrollListener mScrollListener = new ToolbarHideScrollListener(new RecyclerViewFirstItemFinder() {
        @Override
        public int findFirstVisibleItemPosition(RecyclerView recyclerView) {
            return ((LinearLayoutManager) mPhotosRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        }
    });

    private boolean isPhotosLoading;

    public static PhotoGalleryFragment createInstance() {
        return new PhotoGalleryFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.photo_gallery_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mPhotosRecyclerView = (EmptyRecyclerView) view.findViewById(R.id.photos);
        mEmptyView = view.findViewById(R.id.empty_photos_view);
        mLoadingView = view.findViewById(R.id.progress_photos_view);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mActionSendContainer = (XFractionalFrameLayout) view.findViewById(R.id.action_send_container);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mToolbar.setTitle(R.string.GALLERY);
        mCallBacks.setToolbar(mToolbar);

        mScrollListener.addView(mToolbar);
        mScrollListener.addView(mActionSendContainer);

        mPhotosLayoutManager = new GridLayoutManager(getActivity(), 1);
        mPhotosRecyclerView.setLayoutManager(mPhotosLayoutManager);
        mPhotosRecyclerView.setLoadingView(mLoadingView);
        mPhotosRecyclerView.setEmptyView(mEmptyView);

        mPhotosRecyclerView.addOnScrollListener(mScrollListener);

        mPhotosRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mPhotosRecyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int viewWidth = mPhotosRecyclerView.getMeasuredWidth();
                        float cardViewWidth = getActivity().getResources().getDimension(R.dimen.image_thumb_size);
                        int newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
                        mPhotosLayoutManager.setSpanCount(newSpanCount);
                        mPhotosLayoutManager.requestLayout();
                    }
                });

        if (mPhotos != null) {
            mPhotosRecyclerView.setLoadingState(false);
            mPhotosRecyclerView.setAdapter(mPhotosAdapter);
        } else {
            List<PhotoItem> result = AwesomeApplication.getPhotoThumbsHolder().release();
            mPhotos = result != null ? new RecyclerItems<>(result) : null;

            if (mPhotos != null) {
                mPhotosAdapter = new PhotoThumbAdapter(mPhotos);
                mPhotosAdapter.setOnItemCheckStateListener(this);
                mPhotosRecyclerView.setAdapter(mPhotosAdapter);
                mPhotosRecyclerView.setLoadingState(false);
            } else if (!isPhotosLoading) {
                isPhotosLoading = true;
                mPhotosRecyclerView.setLoadingState(true);
                new GetGalleryImageUrisTask(getActivity(), this).execute();
            }
        }

        if (mPhotosAdapter != null && mPhotosAdapter.getCheckedItemCount() > 0) {
            mActionSendContainer.setXFraction(1.0f);
        }

        mActionSendContainer.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mScrollListener.removeView(mToolbar);
        mScrollListener.removeView(mActionSendContainer);
        mPhotosRecyclerView.removeOnScrollListener(mScrollListener);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallBacks = (GalleryActivityCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AnimationListener");
        }
    }

    @Override
    public void onTaskFinished(Class<? extends AsyncTaskWithListener> completedTask, List<PhotoItem> photoItems) {
        isPhotosLoading = false;
        mPhotosRecyclerView.setLoadingState(false);
        if (mPhotos != null) {
            mPhotos = new RecyclerItems<>(photoItems);
            mPhotosAdapter.updateItems(mPhotos);
        }else {
            mPhotos = new RecyclerItems<>(photoItems);
            mPhotosAdapter = new PhotoThumbAdapter(mPhotos);
            mPhotosAdapter.setOnItemCheckStateListener(this);
            mPhotosRecyclerView.setAdapter(mPhotosAdapter);
        }
    }

    @Override
    public void onCheckStateChange(int oldCheckedItemCount, int newCheckedItemCount) {
        if (getActivity() != null) {
            if (oldCheckedItemCount == 0 && newCheckedItemCount > 0) {
                Animator animator = AnimatorInflater.loadAnimator(getActivity(), R.animator.fragment_slide_in);
                animator.setTarget(mActionSendContainer);
                animator.start();
            } else if (oldCheckedItemCount > 0 && newCheckedItemCount == 0) {
                Animator animator = AnimatorInflater.loadAnimator(getActivity(), R.animator.fragment_slide_out);
                animator.setTarget(mActionSendContainer);
                animator.start();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.action_send_container) {
            mCallBacks.sendPhotos(mPhotosAdapter.getCkeckedItems());
        }
    }
}
