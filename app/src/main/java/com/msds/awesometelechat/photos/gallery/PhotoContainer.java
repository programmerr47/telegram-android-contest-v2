package com.msds.awesometelechat.photos.gallery;

import android.net.Uri;

/**
 * @author Michael Spitsin
 * @since 2015-07-12
 */
public class PhotoContainer {
    private Uri mPhotoUri;
    private int mWidth;
    private int mHeight;

    public Uri getPhotoUri() {
        return mPhotoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        mPhotoUri = photoUri;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }
}
