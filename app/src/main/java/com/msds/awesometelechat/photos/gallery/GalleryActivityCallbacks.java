package com.msds.awesometelechat.photos.gallery;

import android.support.v7.widget.Toolbar;

import com.msds.awesometelechat.adapter.item.photo.PhotoItem;

import java.util.Collection;

/**
 * @author Michael Spitsin
 * @since 2015-05-18
 */
public interface GalleryActivityCallbacks {
    void setToolbar(Toolbar toolbar);
    void sendPhotos(Collection<PhotoItem> photos);
}
