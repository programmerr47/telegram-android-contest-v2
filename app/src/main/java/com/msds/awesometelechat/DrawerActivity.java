package com.msds.awesometelechat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.msds.awesometelechat.action.drawer.LogoutAction;
import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;
import com.msds.awesometelechat.adapter.item.drawer.DrawerItem;
import com.msds.awesometelechat.callback.NavigationDrawerListener;
import com.msds.awesometelechat.chat.ChatFragment;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.libhelpers.updating.UpdateWatcher;
import com.msds.awesometelechat.login.LoginActivity;
import com.msds.awesometelechat.util.UserInfo;
import com.msds.awesometelechat.util.Util;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Main activity for all fragments that represented by drawer items.
 *
 * @author Michael Spitsin
 * @since 2014-10-15
 */
public class DrawerActivity extends AppCompatActivity implements
        NavigationDrawerListener,
        FragmentManager.OnBackStackChangedListener,
        AnimationListener,
        DrawerActivityCallBacks{

    private static final String DID_CHAT_SLIDED_OUT = "DID_CHAT_SLIDED_OUT";

    public static final String CONNECTION_STATE_READY = "Ready...";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Main fragment with list of chats.
     */
    private ChatListFragment mChatListFragment;

    //TODO add ChatFragment, create it instance, fill it and override onCreateAnimator function
    //TODO see LoginActivityCodeFragment or LoginChooseCountryFragment for more details
    private ChatFragment mChatFragment;

    /**
     * New action bar for material design
     */
    private Toolbar mToolbar;

    private View mDarkCoverView;

    private boolean isAnimating;
    private boolean didChatFragmentSlideOut;

    private UpdateWatcher<TdApi.UpdateOption> mUpdateOptionHandler = new UpdateWatcher<TdApi.UpdateOption>() {
        @Override
        public void onUpdate(TdApi.UpdateOption updateOption) {
            if (updateOption.name.equals("connection_state")) {
                String connectionState = ((TdApi.OptionString) updateOption.value).value + "...";
                if (mChatListFragment != null && mChatListFragment.getToolbar() != null) {
                    if (connectionState.equals(CONNECTION_STATE_READY)) {
                        mChatListFragment.getToolbar().setTitle(R.string.MESSAGES);
                    } else {
                        mChatListFragment.getToolbar().setTitle(connectionState);
                    }
                    mChatListFragment.setConnectionState(connectionState);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        AwesomeApplication.getUpdateManager().addWatcher(TdApi.UpdateOption.class, mUpdateOptionHandler);

        if (savedInstanceState != null) {
            didChatFragmentSlideOut = savedInstanceState.getBoolean(DID_CHAT_SLIDED_OUT);
        }

        init();

        final Client client = TG.getClientInstance();
        client.send(new TdApi.AuthGetState(), new Client.ResultHandler() {

            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object instanceof TdApi.AuthStateOk) {
                    client.send(new TdApi.GetMe(), new UIResultHandler(new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            if (object instanceof TdApi.User) {
                                TdApi.User me = (TdApi.User) object;
                                UserInfo.setMe(me);
                                mNavigationDrawerFragment.setMe(DrawerActivity.this, me);
                            } else {
                                //TODO this is for know if we can receive something else. If so, need to fill this branch of conditions
                                throw new IllegalStateException("Unknown answer state: " + object);
                            }
                        }
                    }));
                } else if (object instanceof TdApi.AuthStateWaitSetPhoneNumber ||
                        object instanceof TdApi.AuthStateWaitSetName ||
                        object instanceof TdApi.AuthStateWaitSetCode) {
                    Intent intent = new Intent(DrawerActivity.this, LoginActivity.class);
                    intent.setFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK |
                                    Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            throw new IllegalStateException("Unknown answer object: " + object);
                        }
                    });
                }
            }
        });
    }

    private void init() {
        FragmentManager fm = getFragmentManager();

        mNavigationDrawerFragment = (NavigationDrawerFragment) fm.findFragmentById(R.id.navigation_drawer);

        mChatListFragment = (ChatListFragment) fm.findFragmentById(R.id.moveToBackContainer);
        if (mChatListFragment == null) {
            mChatListFragment = new ChatListFragment();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.moveToBackContainer, mChatListFragment);
            transaction.commit();
        }

        mChatFragment = (ChatFragment) fm.findFragmentById(R.id.slideFragmentContainer);

        // Change drawer width dynamically following by android guidelines
        // http://www.google.com/design/spec/layout/metrics-and-keylines.html#metrics-and-keylines-keylines-and-spacing
        //TODO replace this to another place
        ViewGroup.LayoutParams params = mNavigationDrawerFragment.getView().getLayoutParams();
        params.width = Util.getDrawerWidthPixels(getApplicationContext());
        mNavigationDrawerFragment.getView().setLayoutParams(params);

        mDarkCoverView = findViewById(R.id.darkCover);
        mDarkCoverView.setAlpha(0f);

        if (mToolbar != null && !didChatFragmentSlideOut) {
            setUpDrawerFragment();
        }

        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onNavigationDrawerItemSelected(DrawerItem element, int position) {
        if ((element != null) && (element.getName() != null)) {
            switch (element.getName()) {
                case HEADER:
                    return;
                case LOG_OUT:
                    new LogoutAction(this).perform();
                    return;
                default:
                    throw new IllegalArgumentException("There is no fragment associated with name: " + element.getName());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            mChatFragment.forceCloseKeyboardIfNecessary();
            goToChatFragment(null);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {
        if (!didChatFragmentSlideOut) {
            makeBaseFragmentLighter();
            mChatFragment.hideScroller();

            Toolbar toolbar = mChatListFragment.getToolbar();
            setToolbar(toolbar);
        }
    }

    @Override
    public void onBackPressed() {
        if (!isAnimating) {
            if (didChatFragmentSlideOut) {
                if (mChatFragment.isAttachmentsOpened()) {
                    mChatFragment.closeAttachments();
                    return;
                }

                didChatFragmentSlideOut = false;
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putBoolean(DID_CHAT_SLIDED_OUT, didChatFragmentSlideOut);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AwesomeApplication.getUpdateManager().deleteWatcher(TdApi.UpdateOption.class, mUpdateOptionHandler);
    }

    @Override
    public void onAnimationEnd() {
        isAnimating = false;
    }

    @Override
    public void onAnimationStart() {
        isAnimating = true;
    }

    @Override
    public void goToChatFragment(ChatListItem item) {
        if (isAnimating) {
            return;
        }
        isAnimating = true;
        if (didChatFragmentSlideOut) {
            didChatFragmentSlideOut = false;
            getFragmentManager().popBackStack();
        } else {
            didChatFragmentSlideOut = true;

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(
                    R.animator.fragment_slide_in,
                    R.animator.fragment_slide_out,
                    R.animator.fragment_slide_in,
                    R.animator.fragment_slide_out);
            mChatFragment = ChatFragment.newInstance(item);
            transaction.replace(R.id.slideFragmentContainer, mChatFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            makeBaseFragmentDarker(null);
        }
    }

    @Override
    public void setToolbar(Toolbar toolbar) {
        this.mToolbar = toolbar;
        setSupportActionBar(toolbar);

        if (!didChatFragmentSlideOut && mNavigationDrawerFragment != null) {
            setUpDrawerFragment();
        }
    }

    @Override
    public void clearMessage(long chatId) {
        if (mChatListFragment != null) {
            mChatListFragment.clearChatListItem(chatId);
        }
    }

    @Override
    public void deleteChat(long chatId) {
        if (mChatListFragment != null) {
            mChatListFragment.deleteChatListItem(chatId);
        }
    }

    private void setUpDrawerFragment() {
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout),
                mToolbar);
    }

    private void makeBaseFragmentDarker(Animator.AnimatorListener listener){
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(mDarkCoverView, "alpha", 0.0f, 0.5f);
        darkHoverViewAnimator.setInterpolator(new DecelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet s = new AnimatorSet();
        s.play(darkHoverViewAnimator);

        if (listener != null) {
            s.addListener(listener);
        }
        s.start();
    }

    private void makeBaseFragmentLighter()
    {
        ObjectAnimator darkHoverViewAnimator = ObjectAnimator.ofFloat(mDarkCoverView, "alpha", 0.5f, 0.0f);
        darkHoverViewAnimator.setInterpolator(new AccelerateInterpolator());
        darkHoverViewAnimator.setDuration(300);

        AnimatorSet s = new AnimatorSet();
        s.play(darkHoverViewAnimator);
        s.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimating = false;
            }
        });
        s.start();
    }
}
