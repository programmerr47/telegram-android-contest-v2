package com.msds.awesometelechat.action.drawer;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.msds.awesometelechat.action.Action;
import com.msds.awesometelechat.libhelpers.UIResultHandler;
import com.msds.awesometelechat.login.LoginActivity;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * Action that simply close existing activity task with all opened and "back stack"-ed
 * activities and goes to {@link com.msds.awesometelechat.login.LoginActivity}.
 *
 * @author Michael Spitsin
 * @since 2015-05-09
 */
public class LogoutAction extends Action<Void, Void> {

    public LogoutAction(@NonNull Context context) {
        super(context);
    }

    @Override
    public Void perform(Void... voids) {
        Client client = TG.getClientInstance();
        client.send(new TdApi.AuthReset(), new UIResultHandler(new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        }));
        return null;
    }
}
