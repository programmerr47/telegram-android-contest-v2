package com.msds.awesometelechat.action;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

/**
 * Main parent for all actions in app, like action bar/toolbar actions,
 * context action bar/toolbar actions, drawer actions and others.
 * <br><br>
 * {@code <Param>} - type describing parameters, that will be passed to main method.
 * <br>
 * {@code <Result>} - type desctibing in what format should we receive an answer.
 *
 * @author Michael Spitsin
 * @since 2015-05-08
 */
public abstract class Action<Param, Result> {

    /**
     * Handler to process ui tasks in ui thread, when part of
     * {@link this#perform(Object[])} method is handling
     * in another one.
     */
    protected Handler mUiHandler = new Handler();

    protected Context mContext;

    public Action(@NonNull Context context) {
        this.mContext = context;
    }

    /**
     * Main method that runs described action with specified parameters.
     *
     * @param params - additional specific parameters
     * @return result of performing operation, it can be any object. If there is no answer
     *          you should provide {@link java.lang.Void} as {@code <Result>}.
     */
    public abstract Result perform(Param... params);
}
