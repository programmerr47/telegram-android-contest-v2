package com.msds.awesometelechat.concurrent.tasks;

import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Pair;

import com.msds.awesometelechat.adapter.ChatAdapter;
import com.msds.awesometelechat.adapter.item.chat.message.ChatPhotoItem;
import com.msds.awesometelechat.adapter.item.photo.PhotoItem;
import com.msds.awesometelechat.collections.ParticipantsManager;
import com.msds.awesometelechat.photos.gallery.GalleryImageProvider;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * @author Michael Spitsin
 * @since 2015-07-12
 */
public class PhotosSender extends ChatAdapter.MessageSender<PhotoItem, Void, Void> {

    private Client mClient = TG.getClientInstance();
    private ExecutorService mSequenceExecutor = Executors.newSingleThreadExecutor();

    public PhotosSender(@NonNull ChatAdapter adapter, @NonNull TdApi.Chat chat, @NonNull ParticipantsManager participantsManager) {
        super(adapter, chat, participantsManager);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        if (!mSequenceExecutor.isShutdown()) {
            mSequenceExecutor.shutdown();
        }
    }

    @Override
    protected Void doInBackground(PhotoItem... params) {
        for (PhotoItem photoItem : params) {
            Uri uri = photoItem.getOriginalUri();
            GalleryImageProvider.galleryAddPic(uri);
            final BitmapFactory.Options bounds = decodeImageBounds(uri);
            int orientation = photoItem.getOrientation();
            sendAndLockThread(getChat().id, uri, bounds, orientation);
        }

        return null;
    }

    private BitmapFactory.Options decodeImageBounds(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri.getPath(), options);
        return options;
    }

    private void sendAndLockThread(long chatId, Uri uri, final BitmapFactory.Options bounds, final int orientation) {
        TdApi.InputMessageContent content = new TdApi.InputMessagePhoto(uri.getPath());
        TdApi.TLFunction sendFunc = new TdApi.SendMessage(chatId, content);
        mClient.send(sendFunc, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                new PhotoResultHandler(object, bounds, orientation).run();
            }
        });
    }

    private class PhotoResultHandler implements Runnable {

        private TdApi.TLObject object;
        private BitmapFactory.Options bounds;
        private int orientation;

        private PhotoResultHandler(TdApi.TLObject object, BitmapFactory.Options bounds, int orientation) {
            this.object = object;
            this.bounds = bounds;
            this.orientation = orientation;
        }

        @Override
        public void run() {
            int photoHeight = bounds.outHeight;
            int photoWidth = bounds.outWidth;
            TdApi.Message message = (TdApi.Message) object;
            final ChatAdapter.NewItemHolder holder = processSending(message);

            if (photoHeight > 0 && photoWidth > 0) {
                ChatPhotoItem newItem = (ChatPhotoItem) holder.getNewItem();
                newItem.setDimensions(new Pair<>(photoWidth, photoHeight));
                newItem.setOrientation(orientation);
            }

            if (isAnimationPerformingInUi()) {
                addNewItemToQueue(holder);
            } else {
                addNewItemToChat(holder);
            }
        }
    }
}
