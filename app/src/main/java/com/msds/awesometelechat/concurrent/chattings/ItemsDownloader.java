package com.msds.awesometelechat.concurrent.chattings;

import com.msds.awesometelechat.adapter.ChatAdapter;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-06-24
 */
//todo add descr
public interface ItemsDownloader {

    //todo add descr
    void download(TdApi.Chat chat, int offset, int limit);
}
