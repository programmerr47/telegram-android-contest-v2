package com.msds.awesometelechat.concurrent.tasks;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;

import com.msds.awesometelechat.adapter.item.photo.PhotoItem;
import com.msds.awesometelechat.util.ImageOrientation;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Spitsin
 * @since 2015-05-15
 */
public class GetGalleryImageUrisTask extends AsyncTaskWithListener<Void, Void, List<PhotoItem>> {

    private Context mContext;

    public GetGalleryImageUrisTask(@NonNull Context context) {
        super();
        this.mContext = context;
    }

    public GetGalleryImageUrisTask(@NonNull Context context, OnTaskFinishedListener<List<PhotoItem>> listener) {
        super(listener);
        this.mContext = context;
    }

    //TODO
    @Override
    protected List<PhotoItem> doInBackground(Void... params) {
        ContentResolver contentResolver = mContext.getContentResolver();
        String[] returnColumns = new String[] {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
        Cursor imageCursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, returnColumns, null, null, MediaStore.Images.Media.DATE_MODIFIED + " DESC");
        List<PhotoItem> result = new ArrayList<>();

        try {
            if (imageCursor != null && imageCursor.moveToFirst()) {
                do {
                    String fullPath = imageCursor.getString(0);
                    Uri originalUri = Uri.parse(fullPath);

                    int orientation = 0;
                    try {
                        ExifInterface exifInterface = new ExifInterface(fullPath);
                        int typeId = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        orientation = ImageOrientation.fromTypeId(typeId).getDegree();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String[] thumbColumns = new String[] {MediaStore.Images.Thumbnails.DATA, MediaStore.Images.Thumbnails.WIDTH, MediaStore.Images.Thumbnails.HEIGHT};
                    Cursor thumbCursor = contentResolver.query(
                            MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                            thumbColumns,
                            MediaStore.Images.Thumbnails.IMAGE_ID + "=?",
                            new String[] {imageCursor.getString(1)},
                            null);

                    try {
                        if (thumbCursor != null && thumbCursor.moveToFirst()) {
                            Uri thumbUri = Uri.parse(thumbCursor.getString(0));
                            PhotoItem.Builder photoBuilder = new PhotoItem.Builder(mContext, thumbUri, originalUri)
                                    .setWidth(Integer.parseInt(thumbCursor.getString(1)))
                                    .setHeight(Integer.parseInt(thumbCursor.getString(2)))
                                    .setOrientation(orientation);
                            result.add(photoBuilder.build());
                        }
                    } finally {
                        if (thumbCursor != null) {
                            thumbCursor.close();
                        }
                    }
                } while (imageCursor.moveToNext());
            }
        } finally {
            if (imageCursor != null) {
                imageCursor.close();
            }
        }

        return result;
    }
}
