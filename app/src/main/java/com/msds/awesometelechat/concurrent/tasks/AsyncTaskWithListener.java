package com.msds.awesometelechat.concurrent.tasks;

import android.os.AsyncTask;

/**
 * @author Michael Spitsin
 * @since 2015-05-15
 */
public abstract class AsyncTaskWithListener<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private OnTaskFinishedListener<Result> mResultListener;

    public AsyncTaskWithListener() {
        super();
    }

    public AsyncTaskWithListener(OnTaskFinishedListener<Result> resultListener) {
        super();
        this.mResultListener = resultListener;
    }

    @Override
    protected final void onPostExecute(Result result) {
        if (mResultListener != null) {
            mResultListener.onTaskFinished(this.getClass(), result);
        }
        onPostExecuteInternal(result);
    }

    protected void onPostExecuteInternal(Result result) {
    }

    public void setOnTaskFinishedListener(OnTaskFinishedListener<Result> resultListener) {
        this.mResultListener = resultListener;
    }

    public interface OnTaskFinishedListener<Result> {
        void onTaskFinished(Class<? extends AsyncTaskWithListener> completedTask, Result result);
    }
}
