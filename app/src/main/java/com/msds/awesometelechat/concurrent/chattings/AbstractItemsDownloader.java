package com.msds.awesometelechat.concurrent.chattings;

import android.os.Handler;
import android.util.Log;

import com.msds.awesometelechat.AwesomeApplication;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-24
 */
//todo add descr
public abstract class AbstractItemsDownloader implements ItemsDownloader {

    private final Client mClient = TG.getClientInstance();
    private final Handler uiHandler = AwesomeApplication.getUiHandler();

    private boolean isReady;
    private boolean isItemsDownloading;
    private boolean isItemsDownloadedCompletely;
    private boolean isKnownAboutDownloading;

    @Override
    public final void download(TdApi.Chat chat, int offset, final int limit) {
        if (!isItemsDownloading) {
            isItemsDownloading = true;
            isKnownAboutDownloading = false;

            TdApi.GetChatHistory historyFunc = new TdApi.GetChatHistory(chat.id, chat.topMessage.id, offset, limit);
            mClient.send(historyFunc, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    prepareLoading();

                    if (object instanceof TdApi.Messages) {
                        int actualLoadedCount = doInBackground((TdApi.Messages) object, limit);

                        if (actualLoadedCount == 0) {
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    isItemsDownloadedCompletely = true;
                                }
                            });
                        }
//                        Log.d("FUCK", "Actual downloaded count = " + actualLoadedCount + ", but limit = " + limit);
                    }

                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (isReady) {
                                performCompletionOfDownloadingPart();
                            }
                            isItemsDownloading = false;
                        }
                    });
                }
            });
        }
    }

    public void setReadyState(boolean isReady) {
        this.isReady = isReady;

        if (isReady && !isItemsDownloading && !isKnownAboutDownloading) {
            performCompletionOfDownloadingPart();
        }
    }

    public boolean isItemsDownloadedCompletely() {
        return isItemsDownloadedCompletely;
    }

    public boolean canDownload() {
        return !isItemsDownloadedCompletely && !isItemsDownloading;
    }

    public void setItemsDownloadedCompletely() {
        this.isItemsDownloadedCompletely = true;
    }

    //todo add descr
    protected abstract void prepareLoading();

    //todo add descr
    protected abstract int doInBackground(TdApi.Messages messages, int limit);

    //todo add descr
    protected abstract void onPartIsDownloaded();

    private void performCompletionOfDownloadingPart() {
        onPartIsDownloaded();
        isKnownAboutDownloading = true;
    }
}
