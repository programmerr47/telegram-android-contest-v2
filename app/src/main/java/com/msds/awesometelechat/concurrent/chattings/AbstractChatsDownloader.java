package com.msds.awesometelechat.concurrent.chattings;

import android.os.Handler;

import com.msds.awesometelechat.AwesomeApplication;
import com.msds.awesometelechat.adapter.item.chat.chatlist.ChatListItem;
import com.msds.awesometelechat.collections.RecyclerItems;
import com.msds.awesometelechat.libhelpers.UIResultHandler;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-07-07
 */
//todo add descr
public abstract class AbstractChatsDownloader implements ChatsDownloader {

    private final Client mClient = TG.getClientInstance();
    private final Handler uiHandler = AwesomeApplication.getUiHandler();

    private boolean isReady = true;
    private boolean isChatsDownloading;
    private boolean isChatsDownloadedCompletely;
    private boolean isKnownAboutDownloading;

    @Override
    public void download(int offset, final int limit) {
        if (!isChatsDownloading) {
            isChatsDownloading = true;
            isKnownAboutDownloading = false;

            mClient.send(new TdApi.GetChats(offset, limit), new UIResultHandler(new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    prepareLoading();

                    if (object instanceof TdApi.Chats) {
                        int actualLoadedCount = doInBackground((TdApi.Chats) object, limit);

                        if (actualLoadedCount < limit) {
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    isChatsDownloadedCompletely = true;
                                }
                            });
                        }
                    }

                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (isReady) {
                                performCompletionOfDownloadingPart();
                            }

                            isChatsDownloading = false;
                        }
                    });
                }
            }));
        }
    }

    public void setReadyState(boolean isReady) {
        this.isReady = isReady;

        if (isReady && !isChatsDownloading && !isKnownAboutDownloading) {
            performCompletionOfDownloadingPart();
        }
    }

    public boolean isChatsDownloadedCompletely() {
        return isChatsDownloadedCompletely;
    }

    public boolean canDownload() {
        return !isChatsDownloadedCompletely && !isChatsDownloading;
    }

    public void setChatsDownloadedCompletely() {
        this.isChatsDownloadedCompletely = true;
    }

    //todo add descr
    protected abstract void prepareLoading();

    //todo add descr
    protected abstract int doInBackground(TdApi.Chats chats, int limit);

    //todo add descr
    protected abstract void onPartIsDownloaded();

    private void performCompletionOfDownloadingPart() {
        onPartIsDownloaded();
        isKnownAboutDownloading = true;
    }
}
