package com.msds.awesometelechat.libhelpers.updating;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Michael Spitsin
 * @since 2015-05-21
 */
enum Updaters {
    INSTANCE;

    private Map<Class<? extends TdApi.TLObject>, Updater> mUpdaterMap = new ConcurrentHashMap<>();

    public <T extends TdApi.TLObject> void putNewUpdater(Class<T> type, Updater<T> newUpdater) {
        if (type == null) {
            throw new NullPointerException("Type is null");
        }

        mUpdaterMap.put(type, newUpdater);
    }

    @SuppressWarnings("unchecked cast")
    public <T extends TdApi.TLObject> Updater<T> getUpdater(Class<T> type) {
        return mUpdaterMap.get(type);
    }
}
