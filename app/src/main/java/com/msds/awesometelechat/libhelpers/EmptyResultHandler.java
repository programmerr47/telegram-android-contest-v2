package com.msds.awesometelechat.libhelpers;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

public enum EmptyResultHandler implements Client.ResultHandler {
    INSTANCE;

    @Override
    public void onResult(final TdApi.TLObject object) {
    }
}
