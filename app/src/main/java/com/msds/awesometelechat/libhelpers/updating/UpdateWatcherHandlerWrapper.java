package com.msds.awesometelechat.libhelpers.updating;

import android.os.Handler;
import android.support.annotation.NonNull;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-21
 */
public class UpdateWatcherHandlerWrapper<T extends TdApi.TLObject> implements UpdateWatcher<T> {

    private UpdateWatcher<T> mUpdateWatcher;
    private Handler mHandler;

    public UpdateWatcherHandlerWrapper(@NonNull UpdateWatcher<T> updateWatcher, @NonNull Handler handler) {
        this.mUpdateWatcher = updateWatcher;
        this.mHandler = handler;
    }

    @Override
    public void onUpdate(final T object) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateWatcher.onUpdate(object);
            }
        });
    }
}
