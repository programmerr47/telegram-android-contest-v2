package com.msds.awesometelechat.libhelpers.updating;

import android.support.annotation.NonNull;
import android.util.Pair;

import com.msds.awesometelechat.AwesomeApplication;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Michael Spitsin
 * @since 2015-05-21
 */
class UIUpdater<T extends TdApi.TLObject> implements Updater<T> {

    private enum Op {
        ADD, REMOVE
    }

    private Collection<UpdateWatcher<T>> mWatchers;
    private UpdateWatcherCycler<T, UpdateWatcher<T>> mCycler;

    private Set<Pair<Op, UpdateWatcher<T>>> mQueue = new LinkedHashSet<>();
    private boolean isNotifying;

    public UIUpdater() {
        this(new HashSet<UpdateWatcher<T>>());
    }

    public UIUpdater(@NonNull Collection<UpdateWatcher<T>> watchers) {
        this.mWatchers = watchers;
        this.mCycler = new UpdateWatcherCycler<>(mWatchers);
    }

    @Override
    public void notifyWatchers(final T object) {
        AwesomeApplication.getUiHandler().post(new Runnable() {
            @Override
            public void run() {
                isNotifying = true;
                mCycler.updateElements(object);
                isNotifying = false;
                clearQueueAfterNotifying();
            }
        });
    }

    @Override
    public void addWatcher(UpdateWatcher<T> watcher) {
        if (isNotifying) {
            mQueue.add(new Pair<>(Op.ADD, watcher));
        } else {
            mWatchers.add(watcher);
        }
    }

    @Override
    public void deleteWatcher(UpdateWatcher<T> watcher) {
        if (isNotifying) {
            if (watcher == mCycler.getCurrentCyclingElement()) {
                mCycler.removeCurrentCyclingElement();
            } else {
                mQueue.add(new Pair<>(Op.REMOVE, watcher));
            }
        } else {
            mWatchers.remove(watcher);
        }
    }

    private void clearQueueAfterNotifying() {
        if (mQueue.size() != 0) {
            for(Pair<Op, UpdateWatcher<T>> queueElem : mQueue) {
                handleOperation(queueElem.first, queueElem.second);
            }
            mQueue.clear();
        }
    }

    private void handleOperation(Op op, UpdateWatcher<T> updateWatcher) {
        switch (op) {
            case ADD:
                mWatchers.add(updateWatcher);
                return;
            case REMOVE:
                mWatchers.remove(updateWatcher);
                return;
            default:
                throw new IllegalArgumentException("Unknown operation: " + op.name());
        }
    }

    private static class UpdateWatcherCycler<T extends TdApi.TLObject, E extends UpdateWatcher<T>> {
        private Collection<E> mCollection;
        private Iterator<E> mCollectionIterator;
        private E mCurrentCyclingElement;

        private UpdateWatcherCycler(Collection<E> collection) {
            this.mCollection = collection;
        }

        private void updateElements(T object) {
            for(mCollectionIterator = mCollection.iterator(); mCollectionIterator.hasNext();) {
                mCurrentCyclingElement = mCollectionIterator.next();
                mCurrentCyclingElement.onUpdate(object);
            }
        }

        private E getCurrentCyclingElement() {
            return mCurrentCyclingElement;
        }

        private void removeCurrentCyclingElement() {
            mCollectionIterator.remove();
        }
    }
}
