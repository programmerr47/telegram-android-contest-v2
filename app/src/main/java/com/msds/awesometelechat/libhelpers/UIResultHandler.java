package com.msds.awesometelechat.libhelpers;

import android.support.annotation.NonNull;

import com.msds.awesometelechat.AwesomeApplication;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-22
 */
public class UIResultHandler implements Client.ResultHandler {

    private Client.ResultHandler mResultHandler;

    public UIResultHandler(@NonNull Client.ResultHandler resultHandler) {
        this.mResultHandler = resultHandler;
    }

    @Override
    public void onResult(final TdApi.TLObject object) {
        AwesomeApplication.getUiHandler().post(new Runnable() {
            @Override
            public void run() {
                mResultHandler.onResult(object);
            }
        });
    }
}
