package com.msds.awesometelechat.libhelpers.updating;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-21
 */
public enum UpdaterCreator {
    UI {
        @Override
        public <T extends TdApi.TLObject> Updater<T> createNewUpdater() {
            return new UIUpdater<>();
        }
    },
    CONCURRENT {
        @Override
        public <T extends TdApi.TLObject> Updater<T> createNewUpdater() {
            throw new UnsupportedOperationException("Concurrent Updater is not implemented yet");
        }
    };

    public abstract <T extends TdApi.TLObject> Updater<T> createNewUpdater();
}
