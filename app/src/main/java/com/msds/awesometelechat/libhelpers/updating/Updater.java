package com.msds.awesometelechat.libhelpers.updating;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-21
 */
interface Updater<T extends TdApi.TLObject> {
    void notifyWatchers(T object);
    void addWatcher(UpdateWatcher<T> watcher);
    void deleteWatcher(UpdateWatcher<T> watcher);
}
