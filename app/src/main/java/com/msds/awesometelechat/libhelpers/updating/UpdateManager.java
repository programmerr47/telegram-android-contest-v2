package com.msds.awesometelechat.libhelpers.updating;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 2015-05-21
 */
public class UpdateManager {

    private Updaters mUpdaters = Updaters.INSTANCE;
    private UpdaterCreator mUpdateCreator;

    public UpdateManager(UpdaterCreator updaterType) {
        this.mUpdateCreator = updaterType;
    }

    public <T extends TdApi.TLObject> void addWatcher(Class<T> type, UpdateWatcher<T> watcher) {
        Updater<T> updater = mUpdaters.getUpdater(type);

        if (updater == null) {
            updater = mUpdateCreator.createNewUpdater();
            mUpdaters.putNewUpdater(type, updater);
        }

        updater.addWatcher(watcher);
    }

    public <T extends TdApi.TLObject> void deleteWatcher(Class<T> type, UpdateWatcher<T> watcher) {
        Updater<T> updater = mUpdaters.getUpdater(type);

        if (updater != null) {
            updater.deleteWatcher(watcher);
        }
    }

    @SuppressWarnings("unchecked")
    public void update(TdApi.TLObject object) {
        Updater updater = mUpdaters.getUpdater(object.getClass());

        if (updater != null) {
            updater.notifyWatchers(object);
        }
    }

    public <T extends TdApi.TLObject> void addUpdater(Class<T> type) {
        addUpdater(type, mUpdateCreator);
    }

    public <T extends TdApi.TLObject> void addUpdater(Class<T> type, UpdaterCreator creator) {
        addUpdater(type, creator, false);
    }

    public <T extends TdApi.TLObject> void addUpdater(Class<T> type, UpdaterCreator creator, boolean force) {
        Updater<T> updater = mUpdaters.getUpdater(type);

        if (updater == null || force) {
            updater = creator.createNewUpdater();
            mUpdaters.putNewUpdater(type, updater);
        }
    }
}
