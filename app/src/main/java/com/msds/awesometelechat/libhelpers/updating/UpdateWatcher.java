package com.msds.awesometelechat.libhelpers.updating;

import org.drinkless.td.libcore.telegram.TdApi;

/**
 * @author Michael Spitsin
 * @since 0215-05-21
 */
public interface UpdateWatcher<T extends TdApi.TLObject> {
    void onUpdate(T object);
}
